/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ortala;

import ortala.appsource.AppUtility;
import ortala.appsource.AppProfile;
import ortala.appsource.setConnection;
import ortala.controller.LoginController;
import ortala.controller.MainFrameController;
import ortala.dbtrans.ApotekerData;
import ortala.dbtrans.PermissionData;
import ortala.view.LoginDialog;
import ortala.view.MainFrame;
import com.alee.laf.progressbar.WebProgressBar;
import com.alee.laf.rootpane.WebFrame;
import com.alee.managers.style.SupportedComponent;
import java.awt.Color;
import java.awt.Font;
import java.util.List;
import java.util.concurrent.ExecutionException;
import javax.swing.SwingWorker;
import javax.swing.UIManager;
import javax.swing.plaf.metal.MetalLookAndFeel;

/**
 *
 * @author LontongSayur
 */
public class Main {
    
    
    static {

        Font globalFont = new Font("Tahoma", Font.PLAIN, 12);

        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
            UIManager.put("OptionPane.messageFont", globalFont);
            UIManager.put("OptionPane.buttonFont", globalFont);
            UIManager.put("ToolTip.font", globalFont);
            //UIManager.put("TableUI", new MetalLookAndFeel());
       

            
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        } catch (InstantiationException ex) {
            ex.printStackTrace();
        } catch (IllegalAccessException ex) {
            ex.printStackTrace();
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            ex.printStackTrace();
        }
    }
    
    public static void main(String args[]) {
        Main main = new Main();
        
        WebFrame frame = new WebFrame("Management Software");
        WebProgressBar progress = new WebProgressBar();
        progress.setProgressBottomColor(Color.CYAN);
        progress.setProgressTopColor(Color.WHITE);
        
        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(frame.getContentPane());
        frame.getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(progress, javax.swing.GroupLayout.DEFAULT_SIZE, 502, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(progress, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
        SwingWorker<Boolean, Void> worker = new SwingWorker<Boolean, Void>() {
            @Override
            protected Boolean doInBackground() throws Exception {
                
                try{
                    AppProfile.DB_CONNECTION = new setConnection().connectTo("jdbc:mysql://localhost:3306/ortala", "root", "");
                    AppProfile.ACCESS_APOTEKER = new ApotekerData();
                    AppProfile.ACCESS_PERMISSION = new PermissionData();
                    LoginDialog dialog = new LoginDialog(new javax.swing.JFrame(), true);
                    dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                        @Override
                        public void windowClosing(java.awt.event.WindowEvent e) {
                            System.exit(0);
                        }
                    });
                    AppProfile.LOGIN_CONTROLLER = new LoginController(dialog);
                    AppProfile.MAIN_CONTROLLER  = new MainFrameController(new MainFrame());
                }catch(NullPointerException e){
                    AppUtility.callErrorMessage(e, "Database Connection Error");
                    System.exit(1);
                }
                progress.setIndeterminate(true);
                progress.setValue(100);
                return true;
            }

            @Override
            protected void process(List<Void> chunks) {
                progress.setValue(this.getProgress());
            }
            
            @Override
            protected void done() {
                try {
                    if(get()){
                        progress.setIndeterminate(false);
                        frame.setVisible(false);
                        //AppProfile.LOGIN_CONTROLLER.callLoginDialog();
                        AppProfile.MAIN_CONTROLLER.getMainFrame().setVisible(true);
                        
                    }
                } catch (InterruptedException ex) {
                    ex.printStackTrace();
                } catch (ExecutionException ex) {
                    ex.printStackTrace();
                }
            }
        };
        worker.execute();
    }
    
}
