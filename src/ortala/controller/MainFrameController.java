/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ortala.controller;

import ortala.appsource.AppProfile;
import ortala.appsource.AppUtility;
import ortala.component.TransparantButton;
import ortala.controller.jabatan.JabatanController;
import ortala.appsource.ControllerTemplate;
import ortala.view.MainFrame;
import com.alee.extended.layout.VerticalFlowLayout;
import com.alee.extended.menu.WebDynamicMenuItem;
import com.alee.extended.window.ComponentMoveAdapter;
import com.alee.extended.window.PopOverDirection;
import com.alee.extended.window.PopOverListener;
import com.alee.extended.window.WebPopOver;
import com.alee.extended.window.WebPopOverStyle;
import com.alee.laf.button.WebButton;
import com.alee.laf.rootpane.WebFrame;
import com.alee.managers.popup.PopupStyle;
import com.alee.managers.popup.WebButtonPopup;
import com.alee.managers.popup.WebPopup;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JInternalFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import ortala.controller.evajab.EvaluasiJabatanController;
import ortala.controller.kompetensi.KompetensiController;
import ortala.controller.master.MasterInstansiController;
import ortala.controller.master.MasterJabatanController;
import ortala.controller.report.FormPengisianController;

/**
 *
 * @author thread009
 */
public class MainFrameController {
    
    private MainFrame mainFrame;
    
    //ALL-DIALOG
    private SettingController settingMenu;
    
    //INVENTORY (MENU)
    private com.alee.extended.menu.WebDynamicMenuItem formDataJabatan
            = new com.alee.extended.menu.WebDynamicMenuItem(new ImageIcon(getClass().getResource("/ortala/image/inventory/add product.png")));
    private com.alee.extended.menu.WebDynamicMenuItem formIdentKompManajerial
            = new com.alee.extended.menu.WebDynamicMenuItem(new ImageIcon(getClass().getResource("/ortala/image/inventory/AddProductCategory.png")));
    private com.alee.extended.menu.WebDynamicMenuItem formDaftarSmentaraKompManajerial
            = new com.alee.extended.menu.WebDynamicMenuItem(new ImageIcon(getClass().getResource("/ortala/image/inventory/CurrentStock.png")));
    private com.alee.extended.menu.WebDynamicMenuItem formKompetensiTambahan
            = new com.alee.extended.menu.WebDynamicMenuItem(new ImageIcon(getClass().getResource("/ortala/image/inventory/add product.png")));
    private com.alee.extended.menu.WebDynamicMenuItem formKategoriKompetensi
            = new com.alee.extended.menu.WebDynamicMenuItem(new ImageIcon(getClass().getResource("/ortala/image/inventory/AddProductCategory.png")));
    private com.alee.extended.menu.WebDynamicMenuItem standarKompetensiManajerial
            = new com.alee.extended.menu.WebDynamicMenuItem(new ImageIcon(getClass().getResource("/ortala/image/inventory/CurrentStock.png")));
    
    //SETTINGS MENU
    private WebPopup popOver = new WebPopup();

    
    public MainFrameController(MainFrame main){
        this.mainFrame  = main;
        mainFrameAction();
        mainFrameFactory();
        settingsProperties();
        //mainFrameMenu(new DashboardController());
    }
    
    public MainFrame getMainFrame() {
        return mainFrame;
    }
    
    private void mainFrameAction(){
        
        //INVENTORY
        this.formDaftarSmentaraKompManajerial.setAction(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                mainFrameMenu(new FormPengisianController());
            }
        });
        
        this.formDataJabatan.setAction(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                
            }
        });
        
        this.formIdentKompManajerial.setAction(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                
            }
        });
        
        this.formKategoriKompetensi.setAction(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                
            }
        });
        
        this.formKompetensiTambahan.setAction(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                
            }
        });
        
        this.standarKompetensiManajerial.setAction(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                
            }
        });
        
      
        this.mainFrame.getBtnSetting().addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (popOver.isShowing()){
                    popOver.hidePopup ();
                } else{
                    popOver.showAsPopupMenu(mainFrame.getBtnSetting());
                }
            }
            
        });
        
        this.mainFrame.getBtnJabatan().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                mainFrameMenu(new JabatanController());
            }
        });
        
        this.mainFrame.getBtnKompetensi().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                mainFrameMenu(new KompetensiController());
            }
        });
        
        this.mainFrame.getBtnEvajab().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                mainFrameMenu(new EvaluasiJabatanController());
            }
        });
        
        this.mainFrame.getBtnMasterInstansi().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                mainFrameMenu(new MasterInstansiController());
            }
        });
        
        this.mainFrame.getBtnMasterJabatan().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                mainFrameMenu(new MasterJabatanController());
            }
        });
    }
    
    private void mainFrameFactory(){
        List<WebDynamicMenuItem> allLaporan = new ArrayList<WebDynamicMenuItem>();
        
        allLaporan.add(formDaftarSmentaraKompManajerial);
        allLaporan.add(formDataJabatan);
        allLaporan.add(formIdentKompManajerial);
        allLaporan.add(formKategoriKompetensi);
        allLaporan.add(formKompetensiTambahan);
        allLaporan.add(standarKompetensiManajerial);
        
        ((ortala.component.TransparantButton) mainFrame.getBtnLaporan()).setMenuItems(allLaporan);
    }
    
    private void mainFrameMenu(ControllerTemplate controller){
        this.mainFrame.getMainPane().addTab(controller.getControllerName(), controller.getControllerView());
    }
    
    private void settingsProperties(){
        
        popOver.setMargin(20);
        
        TransparantButton menuSettings   = new TransparantButton("/ortala/image/new/setting-in.png", "/ortala/image/new/setting-out.png");
        menuSettings.setText("Settings");
        menuSettings.setFont(new Font("Tahoma", Font.BOLD, 12));
        menuSettings.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                popOver.hidePopup();
                settingMenu = new SettingController();
                settingMenu.getDialog().setVisible(true);
//                if(AppProfile.DATA_APOTEKER.getUserLevel()==1){
//                }else{
//                    JOptionPane.showMessageDialog(AppProfile.MAIN_CONTROLLER.getMainFrame(), "Only Admin Can Access Application Settings...!!!");
//                }
            }
        });
        
        TransparantButton menuLogout   = new TransparantButton("/ortala/image/new/setting-in.png", "/ortala/image/new/setting-out.png");
        menuLogout.setText("Logout");
        menuLogout.setFont(new Font("Tahoma", Font.BOLD, 12));
        menuLogout.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                popOver.hidePopup();
                mainFrame.dispose();
                AppProfile.LOGIN_CONTROLLER.getDialog().getTextPassword().setText("");
                AppProfile.LOGIN_CONTROLLER.getDialog().getTextUser().setText("");
                AppProfile.LOGIN_CONTROLLER.getDialog().setVisible(true);
                mainFrame.getMainPane().removeAll();
            }
        });
        
        JPanel settingPanel = new JPanel();
        settingPanel.setOpaque(false);
        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(settingPanel);
        settingPanel.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(menuSettings, javax.swing.GroupLayout.DEFAULT_SIZE, 130, Short.MAX_VALUE)
                    .addComponent(menuLogout, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(menuSettings)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(menuLogout)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        popOver.add(settingPanel);
        popOver.setPopupStyle(PopupStyle.lightSmall);
        popOver.packPopup();
        
        

    }
}