/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ortala.controller.master;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import javax.swing.Action;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import ortala.appsource.ControllerTemplate;
import ortala.dbtrans.ortala.MasterInstansiData;
import ortala.dbtrans.ortala.MasterJabatanData;
import ortala.model.master.MasterInstansi;
import ortala.model.master.MasterJabatan;
import ortala.view.master.MasterJabatanView;

/**
 *
 * @author SALAM29
 */
public class MasterJabatanController extends ControllerTemplate {
    
    MasterJabatanView view;
    MasterJabatanData data;
    MasterInstansiData datains;
    
    public MasterJabatanController() {
        setControllerName("Master Jabatan");
        setControllerData(new MasterJabatanData());
        setControllerView(new MasterJabatanView());
        this.view = (MasterJabatanView) getControllerView();
        this.data = (MasterJabatanData) getControllerData();
        datains = new MasterInstansiData();
        initPanel();
    }
    
    void initPanel() {
        enableFieldJabatan(false);
        enableFieldUraian(false);
        setBtnAction();
        refreshTableJabatan();
    }
    
    void enableFieldJabatan(boolean status) {
        view.getBtnCancelJabatan().setEnabled(status);
        view.getBtnSaveJabatan().setEnabled(status);
        view.getTableJabatan().setEnabled(status);
        view.getCbInstansi().setEnabled(status);
        view.getTextKodeJabatan().setEnabled(status);
        view.getTextNamaJabatan().setEnabled(status);
        view.getRbTipeNon().setEnabled(status);
        view.getRbTipeStruktural().setEnabled(status);
        view.getAreaPeranJabatan().setEnabled(status);
    }
    
    void enableFieldUraian(boolean status) {
        view.getBtnDeleteUraian().setEnabled(status);
        view.getBtnSaveUraian().setEnabled(status);
        view.getTableUraian().setEnabled(status);
        
    }
    
    void setBtnAction() {
        view.getBtnAddJabatan().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                enableFieldJabatan(true);
                view.getBtnAddJabatan().setEnabled(false);
                fillCbInstansi();
            }
        });
        
        view.getBtnSaveJabatan().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                saveJabatan();
            }
        });
        
        view.getBtnCancelJabatan().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                refresh();
                enableFieldJabatan(false);
                view.getBtnAddJabatan().setEnabled(true);
            }
        });
        
        view.getBtnAddUraian().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                enableFieldUraian(true);
                view.getBtnAddUraian().setEnabled(false);
            }
        });
        
        view.getBtnDeleteUraian().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                
            }
        });
        
        view.getBtnSaveUraian().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                
            }
        });
    }
    
    private void fillCbInstansi() {
        view.getCbInstansi().removeAllItems();
        try {
            view.getCbInstansi().addItem("-- Pilih Instansi --");
            for (MasterInstansi instansi : this.datains.getMasterInstansi()) {
                view.getCbInstansi().addItem(instansi.getKodeInstansi() + " - " + instansi.getNamaInstansi());
            }
        } catch (SQLException e) {
            
        }
    }
    
    private int getTipeJabatan() {
        if (view.getRbTipeNon().isSelected()) {
            return 0;
        } else if (view.getRbTipeStruktural().isSelected()) {
            return 1;
        } else {
            return 2;
        }
    }
    
    private boolean validation() {
        if (view.getCbInstansi().getSelectedItem().toString().equals("")) {
            JOptionPane.showMessageDialog(view, "Kode Instansi");
            view.getCbInstansi().requestFocus();
            return false;
        } else if (view.getTextKodeJabatan().getText().equals("")) {
            JOptionPane.showMessageDialog(view, "Kode Jabatan");
            view.getTextKodeJabatan().requestFocus();
            return false;
        } else if (view.getTextNamaJabatan().getText().equals("")) {
            JOptionPane.showMessageDialog(view, "Nama Jabatan");
            view.getTextNamaJabatan().requestFocus();
            return false;
        } else if (getTipeJabatan() == 2) {
            JOptionPane.showMessageDialog(view, "Tipe Jabatan");
            return false;
        } else if (view.getAreaPeranJabatan().getText().equals("")) {
            JOptionPane.showMessageDialog(view, "Peran Jabatan");
            view.getAreaPeranJabatan().requestFocus();
            return false;
        } else {
            return true;
        }
    }
    
    private void saveJabatan() {
        if (validation() == true) {
            try {
                boolean jabatanJab = this.data.insertJabatan(createMasterJabatan());
                if (jabatanJab == true) {
                    JOptionPane.showMessageDialog(view, "Berhasiln Menyimpan Data");
                    refreshTableJabatan();
                }
            } catch (Exception e) {
            }
        }
    }
    
    private MasterJabatan createMasterJabatan() {
        MasterJabatan jabatan = null;
        String kodeinstansi = view.getCbInstansi().getSelectedItem().toString().split(" - ")[0];;
        String kodejabatan = view.getTextKodeJabatan().getText();
        String namajabatan = view.getTextNamaJabatan().getText();
        int tipejabatan = getTipeJabatan();
        String peranjabatan = view.getAreaPeranJabatan().getText();
        jabatan = new MasterJabatan(0, kodeinstansi, kodejabatan, namajabatan, tipejabatan, peranjabatan);
        return jabatan;
    }
    
    private void refreshTableJabatan() {
        DefaultTableModel tmb = (DefaultTableModel) view.getTableJabatan().getModel();
        tmb.setNumRows(0);
        int row = 0;
        try {
            for (MasterJabatan data : this.data.getAllJabatan()) {
                tmb.addRow(new Object[]{
                    ++row,
                    data.getKodeInstansi(),
                    data.getKodeJabatan(),
                    data.getNamaJabatan()
                });
            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(controllerView, "Failed to Select Database: " + e);
        }
    }
    
    private void refresh() {
        view.getCbInstansi().setSelectedIndex(0);
        view.getTextKodeJabatan().setText("");
        view.getTextNamaJabatan().setText("");
        view.getAreaPeranJabatan().setText("");
        view.getBtnGroupTipeJabatan().clearSelection();
    }
    
}
