/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ortala.controller.master;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.Action;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import ortala.appsource.ControllerTemplate;
import ortala.dbtrans.ortala.MasterInstansiData;
import ortala.model.master.MasterInstansi;
import ortala.view.master.MasterInstansiView;

/**
 *
 * @author SALAM29
 */
public class MasterInstansiController extends ControllerTemplate {

    MasterInstansiView view;
    MasterInstansiData data;

    public MasterInstansiController() {
        setControllerName("Master Instansi");
        setControllerData(new MasterInstansiData());
        setControllerView(new MasterInstansiView());
        this.view = (MasterInstansiView) getControllerView();
        this.data = (MasterInstansiData) getControllerData();
        initPanel();
    }

    private void initPanel() {
        enableField(false);
        setBtnAction();
        refreshTableInstansi();
    }

    void enableField(boolean status) {
        view.getBtnCancelInstansi().setEnabled(status);
        view.getBtnSaveInstansi().setEnabled(status);
        view.getTextKodeInstansi().setEnabled(status);
        view.getTextNamaInstansi().setEnabled(status);

    }

    void setBtnAction() {
        view.getBtnAddInstansi().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                enableField(true);
                view.getBtnAddInstansi().setEnabled(false);
            }
        });

        view.getBtnCancelInstansi().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                view.getTextKodeInstansi().setText("");
                view.getTextNamaInstansi().setText("");
                enableField(false);
                view.getBtnAddInstansi().setEnabled(true);
            }
        });

        view.getBtnSaveInstansi().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                saveInstansi();
            }
        });
    }

    

    private boolean validation() {
        if (view.getTextKodeInstansi().getText().equals("")) {
            JOptionPane.showMessageDialog(view, "Kode Instansi");
            view.getTextKodeInstansi().requestFocus();
            return false;
        } else if (view.getTextNamaInstansi().getText().equals("")) {
            JOptionPane.showMessageDialog(view, "Nama Instansi");
            view.getTextNamaInstansi().requestFocus();
            return false;
        } else {
            
            return true;
        }
    }

    private void saveInstansi() {
        if (validation()==true) {
            try {
                boolean instansiIns = this.data.insertInstansi(createMasterInstansi());

                if (instansiIns) {
                    JOptionPane.showMessageDialog(view, "Berhasil Menyimpan Data");
                    refreshTableInstansi();
                } else{
                     JOptionPane.showMessageDialog(view, "Tidak Berhasil Menyimpan Data");
                }
            } catch (Exception e) {
                JOptionPane.showMessageDialog(view, e);
            }
        }
    }

    public MasterInstansi createMasterInstansi() {
        MasterInstansi data = null;
        String kode = view.getTextKodeInstansi().getText();
        String nama = view.getTextNamaInstansi().getText();

        data = new MasterInstansi(0, kode, nama);
        return data;
    }

    public void refreshTableInstansi() {
        DefaultTableModel tmb = (DefaultTableModel) view.getTableInstansi().getModel();
        tmb.setNumRows(0);
        int row = 0;
        try {
            for (MasterInstansi data : this.data.getAllInstansi()) {
                tmb.addRow(new Object[]{++row, data.getKodeInstansi(), data.getNamaInstansi()});
            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(controllerView, "Failed to Select Database: " + e);
        }
    }
}
