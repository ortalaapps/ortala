    /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ortala.controller;

import ortala.appsource.AppProfile;
import ortala.appsource.AppUtility;
import ortala.dbtrans.ApotekerData;
import ortala.dbtrans.PermissionData;
import ortala.model.ApotekerModel;
import ortala.model.PermissionModel;
import ortala.view.LoginDialog;
import ortala.view.MainFrame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.List;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

/**
 *
 * @author LontongSayur
 */
public class LoginController {
    
    private LoginDialog dialog;
    private ApotekerData accessApoteker;
    private PermissionData accessPermission;
    
    
    public LoginController(LoginDialog dialog){
        this.dialog = dialog;
        this.accessApoteker = AppProfile.ACCESS_APOTEKER;
        this.accessPermission = AppProfile.ACCESS_PERMISSION;
        setAction();
    }
    
    private void setAction(){
        
        this.dialog.getBtnLogin().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String namaApoteker = dialog.getTextUser().getText();
                String password = dialog.getTextPassword().getText();
                
                if(namaApoteker.equals(""))
                    JOptionPane.showMessageDialog(dialog, "Harap Isi Nama Apoteker");
                else if(password.equals(""))
                    JOptionPane.showMessageDialog(dialog, "Harap Isi Nama Apoteker");
                else{
                    try{
                        ApotekerModel apoteker = accessApoteker.getApoteker(namaApoteker, password);
                        if(apoteker == null){
                            JOptionPane.showMessageDialog(dialog, "User Atau Password Salah !!!","",JOptionPane.ERROR_MESSAGE);
                        }else{
                            AppProfile.DATA_APOTEKER = apoteker;
                            AppProfile.DATA_PERMISSION = accessPermission.getPermission(apoteker.getApotekerId());
                            dialog.setVisible(false);
                            AppProfile.MAIN_CONTROLLER.getMainFrame().setVisible(true);
                            AppProfile.MAIN_CONTROLLER.getMainFrame().getTextUsername().setText(apoteker.getNamaApoteker());
                            
                            
                            
                        }
                    }catch(SQLException ex){
                        JOptionPane.showMessageDialog(dialog, "SQL Error: "+ex,"",JOptionPane.ERROR_MESSAGE);
                        ex.printStackTrace();
                    }catch(NullPointerException ef){
                        JOptionPane.showMessageDialog(dialog, "Application Error: "+ef,"",JOptionPane.ERROR_MESSAGE);
                        ef.printStackTrace();
                    }
                }
                
            }
        });
    }

    public LoginDialog getDialog() {
        return dialog;
    }
    
    public void callLoginDialog(){
        try{
            List<ApotekerModel> data = accessApoteker.getApotekers();
            if(data.size() == 0){
                AppProfile.MAIN_CONTROLLER.getMainFrame().setVisible(true);
            }else{
                dialog.setVisible(true);
            }
        }catch(SQLException e){
            
        }
    }
    
    
    
    
    
}
