/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ortala.controller.evajab;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ExecutionException;
import javax.swing.JOptionPane;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import ortala.appsource.AppProfile;
import ortala.appsource.AppUtility;
import ortala.appsource.ControllerTemplate;
import ortala.appsource.TextAreaCellRenderer;
import ortala.appsource.TextAreaEditor;
import ortala.component.UIHelper;
import ortala.dbtrans.ortala.EvajabData;
import ortala.model.master.MasterFaktor;
import ortala.model.master.MasterFaktorDetail;
import ortala.model.master.MasterInstansi;
import ortala.model.master.MasterJabatan;
import ortala.model.master.MasterJabatanUraian;
import ortala.view.evajab.EvaluasiJabatanView;
import javax.swing.SwingWorker;
import ortala.model.evajab.Evajab;
import ortala.model.evajab.EvajabDetail;
import ortala.model.evajab.EvajabHasilKerja;
import ortala.model.evajab.EvajabTanggungJawab;

/**
 *
 * @author DELL
 */
public class EvaluasiJabatanController extends ControllerTemplate {

    EvaluasiJabatanView view;
    EvajabData data;
    List<HashMap<String, Object>> tempFaktor;

    public EvaluasiJabatanController() {
        setControllerName("Evaluasi Jabatan");
        setControllerData(new EvajabData());
        setControllerView(new EvaluasiJabatanView());

        this.view = (EvaluasiJabatanView) getControllerView();
        this.data = (EvajabData) getControllerData();

        this.tempFaktor = new ArrayList<>();
        initPanel();
    }

    private void initPanel() {
        enableField(false);
        setCbAction();
        setBtnAction();
        setTableAction();
    }

    private void setBtnAction() {
        view.getBtnAddJabatan().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                tambahBaru();
            }
        });

        view.getBtnAddHasilKerja().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                DefaultTableModel dtm = (DefaultTableModel) view.getTableHasilKerja().getModel();
                dtm.addRow(new Object[]{"", ""});
            }
        });

        view.getBtnAddTanggungJawab().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                DefaultTableModel dtm = (DefaultTableModel) view.getTableTanggungJawab().getModel();
                dtm.addRow(new Object[]{"", ""});
            }
        });

        view.getBtnRemoveHasilKerja().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                DefaultTableModel dtm = (DefaultTableModel) view.getTableHasilKerja().getModel();
                int row = view.getTableHasilKerja().getSelectedRow();
                if (row >= 0) {
                    dtm.removeRow(row);
                } else {
                    JOptionPane.showMessageDialog(view, "Data Belum Dipilih");
                }
            }
        });

        view.getBtnRemoveTanggungJawab().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                DefaultTableModel dtm = (DefaultTableModel) view.getTableTanggungJawab().getModel();
                int row = view.getTableTanggungJawab().getSelectedRow();
                if (row >= 0) {
                    dtm.removeRow(row);
                } else {
                    JOptionPane.showMessageDialog(view, "Data Belum Dipilih");
                }
            }
        });

        view.getBtnAcceptFaktor().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                pilihLevelFaktor();
            }
        });

    }

    private void setCbAction() {
        view.getCbPilihInstansi().addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent ie) {
                if (view.getCbPilihInstansi().getSelectedItem().equals("-- Pilih Instansi --")) {
                    rmItemListenerIfAdded();
                    view.getCbPilihJabatan().removeAllItems();
                    view.getTextPeranJabatan().setText("");
                    DefaultTableModel dtm = new DefaultTableModel(new Object[]{"No.", "Deskripsi"}, 0);
                    view.getTableUraianTugas().setModel(dtm);
                    DefaultTableModel dtm1 = new DefaultTableModel(new Object[]{"No.", "Kode Faktor", "Deskripsi"}, 0);
                    view.getTableFaktor().setModel(dtm1);
                    DefaultTableModel dtm2 = new DefaultTableModel(new Object[]{"No.", "Level", "Deskripsi", "Nilai"}, 0);
                    view.getTableFaktorDetail().setModel(dtm2);

                } else {
                    rmItemListenerIfAdded();
                    view.getCbPilihJabatan().removeAllItems();
                    view.getTextPeranJabatan().setText("");
                    DefaultTableModel dtm = new DefaultTableModel(new Object[]{"No.", "Deskripsi"}, 0);
                    view.getTableUraianTugas().setModel(dtm);
                    DefaultTableModel dtm1 = new DefaultTableModel(new Object[]{"No.", "Kode Faktor", "Deskripsi"}, 0);
                    view.getTableFaktor().setModel(dtm1);
                    DefaultTableModel dtm2 = new DefaultTableModel(new Object[]{"No.", "Level", "Deskripsi", "Nilai"}, 0);
                    view.getTableFaktorDetail().setModel(dtm2);
                    activeCbJabatan();
                }
            }
        });
    }

    private void setTableAction() {
        view.getTableFaktor().getSelectionModel().addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                activeFaktorDetail();
            }
        });
    }

    private void tambahBaru() {
        enableField(true);
        view.getBtnAddJabatan().setEnabled(false);
        String kode = "";
        try {
            do {
                kode = "EVAJAB-" + AppUtility.randomNumericString(5);
            } while (data.isNomorEvajabExist(kode));
        } catch (SQLException e) {
        }
        view.getTextKodeEvajab().setText(kode);
        fillCbInstansi();
    }

    private void fillCbInstansi() {
        view.getCbPilihInstansi().removeAllItems();
        try {
            view.getCbPilihInstansi().addItem("-- Pilih Instansi --");
            for (MasterInstansi instansi : this.data.getMasterInstansi()) {
                view.getCbPilihInstansi().addItem(instansi.getKodeInstansi() + " - " + instansi.getNamaInstansi());
            }
        } catch (SQLException e) {

        }
    }

    private void fillCbJabatan(String kodeInstansi) {
        view.getCbPilihJabatan().removeAllItems();
        try {
            view.getCbPilihJabatan().addItem("-- Pilih Jabatan --");
            for (MasterJabatan jabatan : this.data.getMasterJabatan(kodeInstansi)) {
                view.getCbPilihJabatan().addItem(jabatan.getKodeJabatan() + " - " + jabatan.getNamaJabatan());
            }
        } catch (SQLException e) {

        }
    }

    private void activeCbJabatan() {
        view.getCbPilihJabatan().removeAllItems();
        String kodeInstansi = view.getCbPilihInstansi().getSelectedItem().toString().split(" - ")[0];
        fillCbJabatan(kodeInstansi);

        view.getCbPilihJabatan().addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent ae) {
                if (view.getCbPilihJabatan().getSelectedIndex() == 0) {
                    view.getTextPeranJabatan().setText("");
                    DefaultTableModel dtm = new DefaultTableModel(new Object[]{"No.", "Deskripsi"}, 0);
                    view.getTableUraianTugas().setModel(dtm);
                    DefaultTableModel dtm1 = new DefaultTableModel(new Object[]{"No.", "Kode Faktor", "Deskripsi"}, 0);
                    view.getTableFaktor().setModel(dtm1);
                    DefaultTableModel dtm2 = new DefaultTableModel(new Object[]{"No.", "Level", "Deskripsi", "Nilai"}, 0);
                    view.getTableFaktorDetail().setModel(dtm2);
                } else {
                    view.getTextPeranJabatan().setText("");
                    DefaultTableModel dtm = new DefaultTableModel(new Object[]{"No.", "Deskripsi"}, 0);
                    view.getTableUraianTugas().setModel(dtm);
                    DefaultTableModel dtm1 = new DefaultTableModel(new Object[]{"No.", "Kode Faktor", "Deskripsi"}, 0);
                    view.getTableFaktor().setModel(dtm1);
                    DefaultTableModel dtm2 = new DefaultTableModel(new Object[]{"No.", "Level", "Deskripsi", "Nilai"}, 0);
                    view.getTableFaktorDetail().setModel(dtm2);
                    activePeranJabatan();
                }
            }
        });
    }

    private void activePeranJabatan() {
        view.getTextPeranJabatan().setText("");
        try {
            String kodeJabatan = view.getCbPilihJabatan().getSelectedItem().toString().split(" - ")[0];
            String peran = this.data.getPeranJabatan(kodeJabatan);
            if (peran.equals("")) {
                JOptionPane.showMessageDialog(view, "Data Belum ada");
            } else {
                view.getTextPeranJabatan().append(peran);
                activeUraianTugasJabatan();
                activeFaktor();
            }
        } catch (SQLException e) {
        }
    }

    private void activeUraianTugasJabatan() {
        String kodeJabatan = view.getCbPilihJabatan().getSelectedItem().toString().split(" - ")[0];
        DefaultTableModel dtm = new DefaultTableModel(new Object[]{"No.", "Deskripsi"}, 0);
        int row = 0;
        try {
            for (MasterJabatanUraian uraian : this.data.getUraianJabatan(kodeJabatan)) {
                if (uraian != null) {
                    dtm.addRow(new Object[]{++row, uraian.getDetailUraian()});
                } else {
                    JOptionPane.showMessageDialog(view, "Data Belum Ada");
                }
            }
        } catch (Exception e) {
        }
        view.getTableUraianTugas().setModel(dtm);
        view.getTableUraianTugas().getTableHeader().setFont(AppProfile.FONT_PROPERTIES);
        view.getTableUraianTugas().getRowHeight(80);
        view.getTableUraianTugas().getColumn("Deskripsi").setCellRenderer(new TextAreaCellRenderer("Deskripsi"));
        view.getTableUraianTugas().getColumn("Deskripsi").setCellEditor(new TextAreaEditor("Deskripsi"));

        int[] width1 = {25, 412};
        for (int i = 0; i < view.getTableUraianTugas().getColumnCount(); i++) {
            view.getTableUraianTugas().getColumnModel().getColumn(i).setPreferredWidth(width1[i]);
        }
    }

    private void activeFaktor() {
        String kodeJabatan = view.getCbPilihJabatan().getSelectedItem().toString().split(" - ")[0];
        DefaultTableModel dtm = new DefaultTableModel(new Object[]{"No.", "Kode Faktor", "Deskripsi"}, 0);
        int row = 0;
        try {
            Integer tipe = this.data.getStruturalJabatan(kodeJabatan);
            for (MasterFaktor faktor : this.data.getFaktorJabatan(tipe)) {
                if (faktor != null) {
                    dtm.addRow(new Object[]{++row, faktor.getKodeFaktor(), faktor.getFaktorJabatan()});
                } else {
                    JOptionPane.showMessageDialog(view, "Data Belum Ada");
                }
            }
        } catch (SQLException e) {
        }
        view.getTableFaktor().setModel(dtm);
        view.getTableFaktor().getTableHeader().setFont(AppProfile.FONT_PROPERTIES);
        view.getTableFaktor().getRowHeight(80);
        view.getTableFaktor().getColumn("Kode Faktor").setCellRenderer(AppProfile.insetsCellRendererCenter);
        view.getTableFaktor().getColumn("Deskripsi").setCellRenderer(new TextAreaCellRenderer("Deskripsi"));
        view.getTableFaktor().getColumn("Deskripsi").setCellEditor(new TextAreaEditor("Deskripsi"));

        int[] width1 = {25, 50, 412};
        for (int i = 0; i < view.getTableFaktor().getColumnCount(); i++) {
            view.getTableFaktor().getColumnModel().getColumn(i).setPreferredWidth(width1[i]);
        }
    }

    private void activeFaktorDetail() {
        int selectedRow = view.getTableFaktor().getSelectedRow();
        String kodeFaktor = "";
        if (selectedRow >= 0) {
            kodeFaktor = view.getTableFaktor().getValueAt(selectedRow, 1).toString();
        }
        DefaultTableModel dtm = new DefaultTableModel(new Object[]{"No.", "Level", "Deskripsi", "Nilai"}, 0);
        int row = 0;
        try {
            for (MasterFaktorDetail faktor : this.data.getFaktorDetailJabatan(kodeFaktor)) {
                if (faktor != null) {
                    dtm.addRow(new Object[]{++row, faktor.getLevel(), faktor.getKriteria(), faktor.getNilai()});
                } else {
                    JOptionPane.showMessageDialog(view, "Data Belum Ada");
                }
            }
        } catch (SQLException e) {
        }

        view.getTableFaktorDetail().setModel(dtm);
        view.getTableFaktorDetail().getTableHeader().setFont(AppProfile.FONT_PROPERTIES);
        view.getTableFaktorDetail().getRowHeight(80);
        view.getTableFaktorDetail().getColumn("Level").setCellRenderer(AppProfile.insetsCellRendererCenter);
        view.getTableFaktorDetail().getColumn("Nilai").setCellRenderer(AppProfile.insetsCellRendererCenter);
        view.getTableFaktorDetail().getColumn("Deskripsi").setCellRenderer(new TextAreaCellRenderer("Deskripsi"));
        view.getTableFaktorDetail().getColumn("Deskripsi").setCellEditor(new TextAreaEditor("Deskripsi"));

        int[] width1 = {25, 50, 390, 50};
        for (int i = 0; i < view.getTableFaktor().getColumnCount(); i++) {
            view.getTableFaktorDetail().getColumnModel().getColumn(i).setPreferredWidth(width1[i]);
        }
    }

    private void pilihLevelFaktor() {
        int selectedRow = view.getTableFaktor().getSelectedRow();
        String kodeFaktor = "";

        int selectedRow1 = view.getTableFaktorDetail().getSelectedRow();
        int level = 0, nilai = 0;
        if (selectedRow1 >= 0) {
            kodeFaktor = view.getTableFaktor().getValueAt(selectedRow, 1).toString();
            level = Integer.valueOf(view.getTableFaktorDetail().getValueAt(selectedRow1, 1).toString());
            nilai = Integer.valueOf(view.getTableFaktorDetail().getValueAt(selectedRow1, 3).toString());
        } else {
            JOptionPane.showMessageDialog(view, "Pilih Level terlebih dahulu");
        }

        HashMap<String, Object> row = new HashMap<>();
        row.put("KODE_FAKTOR", kodeFaktor);
        row.put("LEVEL", level);
        row.put("NILAI", nilai);

        if (tempFaktor.isEmpty()) {
            if (kodeFaktor.equals("") && level == 0 && nilai == 0) {
                return;
            } else {
                tempFaktor.add(row);
                JOptionPane.showMessageDialog(view, "Level Faktor Telah Dipilih!");
                view.getTableFaktorDetail().clearSelection();
                view.getTableFaktor().clearSelection();
            }
        } else {
            String kode = "";
            boolean coba = false;
            for (int i = 0; i < tempFaktor.size(); i++) {
                System.out.println("coba mencocokkan data");
                kode = tempFaktor.get(i).get("KODE_FAKTOR").toString();
                if (kode.equals(kodeFaktor)) {
                    coba = false;
                    JOptionPane.showMessageDialog(view, "Date Level untuk Faktor " + kode + " telah ada");
                    break;
                } else {
                    coba = true;
                }
            }

            if (coba) {
                if (kodeFaktor.equals("") && level == 0 && nilai == 0) {
                    return;
                } else {
                    tempFaktor.add(row);
                    JOptionPane.showMessageDialog(view, "Level Faktor Telah Dipilih!");
                    view.getTableFaktorDetail().clearSelection();
                    view.getTableFaktor().clearSelection();
                }
            }
        }
    }

    private void simpanDataEvajab() {
        UIHelper.callLoadingDialog();
        SwingWorker<Boolean, Void> worker = new SwingWorker<Boolean, Void>() {
            @Override
            protected Boolean doInBackground() throws Exception {
                
                return true;
            }

            @Override
            protected void done() {
                try {
                    if (get()) {
                        UIHelper.killLoadingDialog();
                    }
                } catch (InterruptedException ex) {
                    ex.printStackTrace();
                } catch (ExecutionException ex) {
                    ex.printStackTrace();
                }
            }
        };
        worker.execute();
    }

    /* HELPER */
    private void rmItemListenerIfAdded() {
        ItemListener[] aListenerr = view.getCbPilihJabatan().getItemListeners();
        if (aListenerr.length > 0) {
            view.getCbPilihJabatan().removeItemListener(aListenerr[0]);
        }
    }

    private void enableField(boolean status) {
        view.getBtnSaveJabatan().setEnabled(status);
        view.getBtnAddTanggungJawab().setEnabled(status);
        view.getBtnAddHasilKerja().setEnabled(status);
        view.getBtnRemoveHasilKerja().setEnabled(status);
        view.getBtnRemoveTanggungJawab().setEnabled(status);
        view.getCbPilihInstansi().setEnabled(status);
        view.getCbPilihJabatan().setEnabled(status);
        view.getTextPeranJabatan().setEnabled(status);
    }

    private Evajab createEvajab() {
        String kodeEvajab = view.getTextKodeEvajab().getText();
        String kodeInstansi = view.getCbPilihInstansi().getSelectedItem().toString().split(" - ")[0];
        String kodeJabatan = view.getCbPilihJabatan().getSelectedItem().toString().split(" - ")[0];
        Date tanggal = new Date();
        return new Evajab(kodeEvajab, kodeInstansi, kodeJabatan, tanggal);
    }

    private List<EvajabDetail> createEvajabDetail() {
        List<EvajabDetail> data = new ArrayList<>();
        for (HashMap<String, Object> hashMap : tempFaktor) {
            data.add(new EvajabDetail(
                    view.getTextKodeEvajab().getText(),
                    hashMap.get("KODE_FAKTOR").toString(),
                    (Integer) hashMap.get("LEVEL"),
                    (Integer) hashMap.get("NILAI")
            ));
        }
        if (data.size() > 0) {
            return data;
        } else {
            return null;
        }
    }

    private List<EvajabHasilKerja> createEvajabHasilKerja() {
        List<EvajabHasilKerja> data = new ArrayList<>();
        DefaultTableModel dtm = (DefaultTableModel) view.getTableHasilKerja().getModel();
        for (int i = 0; i < dtm.getRowCount(); i++) {
            data.add(new EvajabHasilKerja(
                    view.getTextKodeEvajab().getText(),
                    view.getCbPilihJabatan().getSelectedItem().toString().split(" - ")[0],
                    dtm.getValueAt(i, 2).toString()));
        }
        if (data.size() > 0) {
            return data;
        } else {
            return null;
        }
    }

    private List<EvajabTanggungJawab> createEvajabTanggungJawab() {
        List<EvajabTanggungJawab> data = new ArrayList<>();
        DefaultTableModel dtm = (DefaultTableModel) view.getTableHasilKerja().getModel();
        for (int i = 0; i < dtm.getRowCount(); i++) {
            data.add(new EvajabTanggungJawab(
                    view.getTextKodeEvajab().getText(),
                    view.getCbPilihJabatan().getSelectedItem().toString().split(" - ")[0],
                    dtm.getValueAt(i, 2).toString()));
        }
        if (data.size() > 0) {
            return data;
        } else {
            return null;
        }
    }

}
