/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ortala.controller.kompetensi;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.SQLException;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import ortala.appsource.AppProfile;
import ortala.appsource.AppUtility;
import ortala.appsource.ControllerTemplate;
import ortala.appsource.TextAreaCellRenderer;
import ortala.appsource.TextAreaEditor;
import ortala.dbtrans.ortala.KompetensiData;
import ortala.model.master.MasterKompetensi;
import ortala.model.master.MasterKompetensiDef;
import ortala.model.master.MasterKompetensiLevel;
import ortala.view.kompetensi.DialogAddKompetensi;
import ortala.view.kompetensi.KompetensiView;

/**
 *
 * @author thread009
 */
public class KompetensiController extends ControllerTemplate {

    private KompetensiView view;
    private KompetensiData data;
    private DialogAddKompetensi dialogAddKompetensi;
    private List<MasterKompetensiDef> tempDefinisi;

    public KompetensiController() {
        setControllerName("Kompetensi");
        setControllerData(new KompetensiData());
        setControllerView(new KompetensiView());
        this.view = ((KompetensiView) getControllerView());
        this.data = ((KompetensiData) getControllerData());
        setAction();
        refreshTable(0);
    }

    private void setAction() {
        // MAIN FUNCTION
        view.getBtnAddKompetensi().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dialogAddKompetensi();
            }
        });

        view.getBtnRemoveKompetensi().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

            }
        });

        view.getTableKompetensi().addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (view.getTableKompetensi().getSelectedRow() < 0) {
                    return;
                }
                if (e.getClickCount() == 2) {
                    editKompetensi();
                }

            }

        });

        //FUNGSI DEFINISI
        view.getTableDefinisi().getSelectionModel().addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                editDefinisi();
            }
        });

        view.getBtnAddDefinisi().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                addDefinisi();
            }
        });

        view.getBtnRemoveDefinisi().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

            }
        });

        view.getBtnAcceptDefinisi().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                acceptDefinisi();
            }
        });

        view.getBtnDiscardDefinisi().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                refreshTable(1);
            }
        });

        // FUNGSI DESKRIPSI
        view.getTableDeskripsi().getSelectionModel().addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                int row = view.getTableDeskripsi().getSelectedRow();
                if (view.getTableDeskripsi().isRowSelected(row)) {
                    view.getBtnRemoveDeskripsi().setEnabled(true);
                } else {
                    view.getBtnRemoveDeskripsi().setEnabled(false);
                }
            }
        });

        view.getBtnAddDeskripsi().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                addDeskripsi();
            }
        });

        view.getBtnRemoveDeskripsi().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                
            }
        });

        view.getBtnAcceptDeskripsi().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                acceptDeskripsi();
            }
        });

        view.getBtnDiscardDeskripsi().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

            }
        });
    }

    //FUNGSI DEFINISI
    private void editDefinisi() {
        int row = view.getTableDefinisi().getSelectedRow();
        if (view.getTableDefinisi().isRowSelected(row)) {
            view.getBtnRemoveDefinisi().setEnabled(true);
            view.getBtnAddDeskripsi().setEnabled(true);
            refreshTable(2);
        } else {
            view.getBtnRemoveDefinisi().setEnabled(false);
            view.getBtnAddDeskripsi().setEnabled(false);
            refreshTable(2);
        }
    }

    private void addDefinisi() {

        boolean isEmpty = false;
        for (int i = 0; i < view.getTableDefinisi().getRowCount(); i++) {
            boolean cell = view.getTableDefinisi().getValueAt(i, 0).toString().equals("");
            boolean valu = view.getTableDefinisi().getValueAt(i, 1).toString().equals("");
            if (cell && valu) {
                isEmpty = true;
            }

        }

        if (isEmpty) {
            JOptionPane.showMessageDialog(view, "Fill Table First !!!");
            refreshTable(1);
            view.getBtnAcceptDefinisi().setEnabled(false);
            view.getBtnDiscardDefinisi().setEnabled(false);
            return;
        }
        DefaultTableModel tb = (DefaultTableModel) view.getTableDefinisi().getModel();
        tb.addRow(new Object[]{"", ""});
        view.getBtnAcceptDefinisi().setEnabled(true);
        view.getBtnDiscardDefinisi().setEnabled(true);

    }

    private void acceptDefinisi() {

        try {
            String kodeKompetensi = getKodeKompetensiFrMainFrame();
            String kodeDef = "DEF-" + AppUtility.randomNumericString(5);
            String definisi = "";
            for (int i = 0; i < view.getTableDefinisi().getRowCount(); i++) {
                boolean cell = view.getTableDefinisi().getValueAt(i, 0).toString().equals("");
                boolean valu = !view.getTableDefinisi().getValueAt(i, 1).toString().equals("");
                if (cell && valu) {
                    definisi = view.getTableDefinisi().getValueAt(i, 1).toString();
                } else {
                    definisi = ((TextAreaEditor) view.getTableDefinisi().getCellEditor(i, 1)).getJTextArea().getText();
                }
            }
            if (!definisi.equals("")) {
                boolean stat = this.data.insertDefinisi(new MasterKompetensiDef(0, kodeKompetensi, kodeDef, definisi));
                if (!stat) {
                    JOptionPane.showMessageDialog(view, "Failed to Insert Database");
                    return;
                }
                refreshTable(1);
                view.getBtnAcceptDefinisi().setEnabled(false);
                view.getBtnDiscardDefinisi().setEnabled(false);
            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(controllerView, "Failed to Insert Database: " + e);
        }
    }

    // FUNGSI DESKRIPSI
    private void addDeskripsi() {
        boolean isEmpty = false;
        for (int i = 0; i < view.getTableDeskripsi().getRowCount(); i++) {
            boolean cell = view.getTableDeskripsi().getValueAt(i, 0).toString().equals("");
            boolean valu = view.getTableDeskripsi().getValueAt(i, 1).toString().equals("");
            if (cell && valu) {
                isEmpty = true;
            }

        }

        if (isEmpty) {
            JOptionPane.showMessageDialog(view, "Fill Table First !!!");
            refreshTable(1);
            view.getBtnAcceptDeskripsi().setEnabled(false);
            view.getBtnDiscardDeskripsi().setEnabled(false);
            return;
        }
        DefaultTableModel tb = (DefaultTableModel) view.getTableDeskripsi().getModel();
        tb.addRow(new Object[]{"", ""});
        view.getBtnAcceptDeskripsi().setEnabled(true);
        view.getBtnDiscardDeskripsi().setEnabled(true);
    }

    private void acceptDeskripsi() {
        try {
            String kodeKompetensi = getKodeKompetensiFrMainFrame();
            String kodeDef = this.tempDefinisi.get(view.getTableDefinisi().getSelectedRow()).getKodeDef();
            String definisi = "";
            for (int i = 0; i < view.getTableDeskripsi().getRowCount(); i++) {
                boolean cell = view.getTableDeskripsi().getValueAt(i, 0).toString().equals("");
                boolean valu = !view.getTableDeskripsi().getValueAt(i, 1).toString().equals("");
                if (cell && valu) {
                    definisi = view.getTableDeskripsi().getValueAt(i, 1).toString();
                } else {
                    definisi = ((TextAreaEditor) view.getTableDeskripsi().getCellEditor(i, 1)).getJTextArea().getText();
                }
            }
            if (!definisi.equals("")) {
                boolean stat = this.data.insertLevel(new MasterKompetensiLevel(
                        0,
                        kodeKompetensi,
                        kodeDef,
                        this.data.getLevelCount(kodeDef) + 1,
                        definisi
                ));
                if (!stat) {
                    JOptionPane.showMessageDialog(view, "Failed to Insert Database");
                    return;
                }
                refreshTable(2);
                view.getBtnAcceptDeskripsi().setEnabled(false);
                view.getBtnDiscardDeskripsi().setEnabled(false);
            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(controllerView, "Failed to Insert Database: " + e);
        }
    }

    // MAIN FUNCTION
    private void editKompetensi() {
        int selectedRow = view.getTableKompetensi().getSelectedRow();
        String kompetensi = view.getTableKompetensi().getValueAt(selectedRow, 1).toString();
        String kode = view.getTableKompetensi().getValueAt(selectedRow, 2).toString();
        view.getTextKompetensi().setText(kompetensi + " (" + kode + ")");
        view.getBtnAddDefinisi().setEnabled(true);
        refreshTable(1);
    }

    private void dialogAddKompetensi() {
        this.dialogAddKompetensi = new DialogAddKompetensi(AppProfile.MAIN_CONTROLLER.getMainFrame(), true);
        this.dialogAddKompetensi.getTextKodeKompetensi().setText("KO-" + AppUtility.randomNumericString(3));
        this.dialogAddKompetensi.getBtnTambah().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                addKompetensi();
            }
        });
        this.dialogAddKompetensi.setVisible(true);
    }

    private void addKompetensi() {

        try {
            String kompetensi = dialogAddKompetensi.getValDefinisiKompetensi().getText();
            String kodeKompetensi = dialogAddKompetensi.getTextKodeKompetensi().getText();
            if (!kompetensi.equals("") && !kodeKompetensi.equals("")) {
                boolean stat = this.data.insertKompetensi(new MasterKompetensi(0, kodeKompetensi, kompetensi));
                if (!stat) {
                    JOptionPane.showMessageDialog(view, "Failed to Insert Database");
                    return;
                }
                refreshTable(0);
                dialogAddKompetensi.dispose();
            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(controllerView, "Failed to Insert Database: " + e);
        }

    }

    private String getKodeKompetensiFrMainFrame() {
        String[] extKode = view.getTextKompetensi().getText().split("\\(");
        String[] kodeItself = extKode[1].split("\\)");
        return kodeItself[0];
    }

    private void refreshTable(int source) {
        switch (source) {
            case 0:
                DefaultTableModel tmb = (DefaultTableModel) view.getTableKompetensi().getModel();
                tmb.setNumRows(0);
                int row = 0;
                try {
                    for (MasterKompetensi data : this.data.getAllKompetensi()) {
                        tmb.addRow(new Object[]{++row, data.getNamaKompetensi(), data.getKodeKompetensi()});
                    }
                } catch (SQLException e) {
                    JOptionPane.showMessageDialog(controllerView, "Failed to Select Database: " + e);
                }

                break;
            case 1:
                String kode = getKodeKompetensiFrMainFrame();
                System.out.println(kode);
                DefaultTableModel tmbD = new DefaultTableModel(new Object[]{"No", "Definisi Kompetensi"}, 0);
                int rowD = 0;
                try {
                    this.tempDefinisi = this.data.getDefinisiByKodeKompetensi(kode);
                    for (MasterKompetensiDef data : this.tempDefinisi) {
                        tmbD.addRow(new Object[]{++rowD, data.getDefinisiKompetensi()});
                    }
                } catch (SQLException ex) {
                }
                view.getTableDefinisi().setModel(tmbD);
                view.getTableDefinisi().getTableHeader().setFont(AppProfile.FONT_PROPERTIES);

                view.getTableDefinisi().setRowHeight(80);

                view.getTableDefinisi().getColumn("Definisi Kompetensi").setCellRenderer(new TextAreaCellRenderer("Definisi Kompetensi"));
                view.getTableDefinisi().getColumn("Definisi Kompetensi").setCellEditor(new TextAreaEditor("Definisi Kompetensi"));

                int[] width2 = {40, 412};
                for (int i = 0; i < view.getTableDefinisi().getColumnCount(); i++) {
                    view.getTableDefinisi().getColumnModel().getColumn(i).setPreferredWidth(width2[i]);
                }

                break;
            case 2:
                int r = view.getTableDefinisi().getSelectedRow();
                String kodeDef;
                if (r < 0) {
                    kodeDef = "";
                } else {
                    kodeDef = this.tempDefinisi.get(r).getKodeDef();
                }
                DefaultTableModel tbDes = new DefaultTableModel(new Object[]{"Level", "Deskripsi"}, 0);
                tbDes.setNumRows(0);
                int rDes = 0;
                try {
                    for (MasterKompetensiLevel lv : this.data.getLevelByKodeDefinisi(kodeDef)) {
                        tbDes.addRow(new Object[]{++rDes, lv.getDeskripsi()});
                    }
                } catch (SQLException ef) {}
                
                view.getTableDeskripsi().setModel(tbDes);
                view.getTableDeskripsi().getTableHeader().setFont(AppProfile.FONT_PROPERTIES);

                view.getTableDeskripsi().setRowHeight(80);

                view.getTableDeskripsi().getColumn("Deskripsi").setCellRenderer(new TextAreaCellRenderer("Deskripsi"));
                view.getTableDeskripsi().getColumn("Deskripsi").setCellEditor(new TextAreaEditor("Deskripsi"));

                int[] width3 = {40, 412};
                for (int i = 0; i < view.getTableDeskripsi().getColumnCount(); i++) {
                    view.getTableDeskripsi().getColumnModel().getColumn(i).setPreferredWidth(width3[i]);
                }
                break;
        }

    }

}
