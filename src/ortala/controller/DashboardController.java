/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ortala.controller;

import ortala.appsource.ControllerTemplate;
import ortala.dbtrans.ortala.JabatanData;
import ortala.view.Dashboard;
import ortala.view.jabatan.JabatanView;

/**
 *
 * @author thread009
 */
public class DashboardController extends ControllerTemplate{
    
    private Dashboard view;

    public DashboardController() {
        setControllerName("Home");
        //setControllerData(new JabatanData());
        setControllerView(new Dashboard());
        this.view = ((Dashboard) getControllerView());

    }
    
}
