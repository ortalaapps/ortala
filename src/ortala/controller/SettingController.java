/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ortala.controller;

import ortala.appsource.AppUtility;
import ortala.appsource.AppProfile;
import ortala.dbtrans.ApotekerData;
import ortala.dbtrans.PermissionData;
import ortala.model.ApotekerModel;
import ortala.model.PermissionModel;
import ortala.view.Setting;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author thread009
 */
public class SettingController {
    
    private Setting dialog;
    private PermissionData accessPermission;
    private ApotekerData accessApoteker;
    private List<ApotekerModel> apotekerModel;
    
    public SettingController(){
        this.accessPermission = AppProfile.ACCESS_PERMISSION;
        this.accessApoteker   = AppProfile.ACCESS_APOTEKER;
        this.dialog = new Setting(null, true);
        settingFactory();
    }
    
    private void settingFactory(){
        dialog.setTitle("Application Settings");
        //USER FACTORY
        userSettingActions();
        resetTablePermission();
        resetUserText();
        enableUserText(false);
        enableUserPermission(false);
        setUserData();
    }
    
    //USER SETTING
    private String idApoteker;
    private void setUserData(){
        DefaultTableModel tmb = (DefaultTableModel) dialog.getUserTable().getModel();
        tmb.setNumRows(0);
        int row = 0;
        try{ 
            this.apotekerModel = accessApoteker.getApotekers();
            for(ApotekerModel d : this.apotekerModel){
                tmb.addRow(new Object[]{++row, d.getNamaApoteker()});
            }
        }catch(SQLException e){
            AppUtility.callErrorMessage(e, "Database Connection Error");
        }
    } //GET USER DATA
    
    private void saveUser(){
        if(userDataValidation()){
            
            if(userPermissionValidation()){
                try{
                    accessApoteker.connection.setAutoCommit(false);
                    accessPermission.connection.setAutoCommit(false);
                    if(accessApoteker.crudApoteker(this.createUserData()) && accessPermission.crudPermission(this.createUserPermission())){
                        accessApoteker.connection.commit();
                        accessPermission.connection.commit();
                        accessApoteker.connection.setAutoCommit(false);
                        accessPermission.connection.setAutoCommit(false);
                        
                        JOptionPane.showMessageDialog(dialog, "User Adding Successfull !!!");
                        
                        enableUserPermission(false);
                        enableUserText(false);
                        resetTablePermission();
                        resetUserText();
                        dialog.getBtnAdd().setEnabled(true);
                        dialog.getBtnSave().setEnabled(false);
                        setUserData();
                    }
                    
                    
                }catch(SQLException e){
                    AppUtility.callErrorMessage(e, "Connection  Error");
                }
            }else{
                JOptionPane.showConfirmDialog(dialog, "Choose User Permission");
                dialog.getPermissionPO().requestFocus();
                return;
            }
        }
    } //SAVE USER DATA
    
    private boolean userDataValidation(){
        if(dialog.getTextAddress().getText().equals("")){
            JOptionPane.showMessageDialog(dialog, "You Have to Input Your Address", "User Input", JOptionPane.ERROR_MESSAGE);
            dialog.getTextAddress().requestFocus();
            return false;
        }else if(dialog.getTextContact().getText().equals("")){
            JOptionPane.showMessageDialog(dialog, "You Have to Input Your Contact", "User Input", JOptionPane.ERROR_MESSAGE);
            dialog.getTextContact().requestFocus();
            return false;
        }else if(dialog.getTextEmail().getText().equals("")){
            JOptionPane.showMessageDialog(dialog, "You Have to Input Your e-mail", "User Input", JOptionPane.ERROR_MESSAGE);
            dialog.getTextEmail().requestFocus();
            return false;
        }else if(dialog.getTextUsername().getText().equals("")){
            JOptionPane.showMessageDialog(dialog, "You Have to Input Your Name", "User Input", JOptionPane.ERROR_MESSAGE);
            dialog.getTextUsername().requestFocus();
            return false;
        }else if(dialog.getTextPassword().getText().equals("")){
            JOptionPane.showMessageDialog(dialog, "You Have to Input Your Password", "User Input", JOptionPane.ERROR_MESSAGE);
            dialog.getTextPassword().requestFocus();
            return false;
        }else if(!dialog.getTextPassword().getText().equals(dialog.getTextRePassword().getText())){
            JOptionPane.showMessageDialog(dialog, "Password Doesn't Match","User Input", JOptionPane.ERROR_MESSAGE);
            dialog.getTextRePassword().requestFocus();
            return false;
        }else{
            return true;
        }
    }
    
    private boolean userPermissionValidation(){
        int userPermission = 0;
        for(int y=0;y<dialog.getPermissionPane().getComponentCount();y++){
            JTable gettingTable = ((JTable)dialog.getPermissionPane().getComponent(y));
            DefaultTableModel tmb = (DefaultTableModel) gettingTable.getModel();
            for(int i=0;i<tmb.getRowCount();i++){
                if( (boolean) tmb.getValueAt(i, 1)==true) userPermission++;
                if( (boolean) tmb.getValueAt(i, 2)==true) userPermission++;
            }
        }
        return userPermission==0 ? false:true;
    }
    
    private ApotekerModel createUserData(){
        this.idApoteker = AppUtility.randomNumericString(10);
        return new ApotekerModel(0, this.idApoteker, 
                dialog.getTextUsername().getText(), 
                dialog.getTextContact().getText(), 
                dialog.getTextEmail().getText(),
                dialog.getTextAddress().getText(),
                dialog.getMyUsername().getText(),
                dialog.getTextPassword().getText(), 
                dialog.isAdministrator()==true ? 1:0);
    }
    
    private List<PermissionModel> createUserPermission(){
        List<PermissionModel> data = new ArrayList<PermissionModel>();
        int permissionIndex = 0;
        String[] permission = {
            AppProfile.ORDERING_PRODUCT,
            AppProfile.VENDORS,
            AppProfile.PAY_ORDER,
            AppProfile.RECEIVE_STOCK,
            AppProfile.SALE_PRODUCT,
            AppProfile.CUSTOMERS,
            AppProfile.RECEIVE_PAYMENT,
            AppProfile.PRODCUTS,
            AppProfile.CATEGORIES,
            AppProfile.CURRENT_STOCK,
            AppProfile.PURCHASING_REPORT,
            AppProfile.INVENTORY_REPORT,
            AppProfile.SALES_REPORT,
        };
        for(int y=0;y<dialog.getPermissionPane().getComponentCount();y++){
            JTable gettingTable = ((JTable)dialog.getPermissionPane().getComponent(y));
            DefaultTableModel tmb = (DefaultTableModel) gettingTable.getModel();
            for(int i=0;i<tmb.getRowCount();i++){
                PermissionModel pers = new PermissionModel();
                
                pers.setApotekerId(this.idApoteker);
                pers.setPermissionId(permission[permissionIndex]);
                
                if((boolean)tmb.getValueAt(i, 1)) pers.setView(true);
                else pers.setView(false);
                if((boolean)tmb.getValueAt(i, 2)) pers.setEdit(true);
                else pers.setEdit(false);
                data.add(pers);
                permissionIndex++;
            }
        }
        return data;
    }
    
    
    
    private void userSettingActions(){
        dialog.getBtnAdd().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                enableUserPermission(true);
                enableUserText(true);
                resetUserText();
                resetTablePermission();
                dialog.getBtnAdd().setEnabled(false);
                dialog.getBtnSave().setEnabled(true);
            }
        });
        
        dialog.getBtnSave().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                saveUser();
            }
        });
        
        dialog.getIsAdministrator().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                isAdministrator();
            }
        });
        
        dialog.getTextPassword().addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                String key = dialog.getTextPassword().getText();
                if(key.equals(""))
                    dialog.getTextRePassword().setEnabled(false);
                else{
                    if(AppUtility.haveSpecialChars(key)){
                        dialog.getTextPassword().setText("");
                        dialog.getTextRePassword().setEnabled(false);
                        AppUtility.createPopOver((JTextField)e.getSource(), "You Have Illegal Characters ");
                    }else{
                        dialog.getTextRePassword().setEnabled(true);
                    }
                }
            }
            
        });
        
        dialog.getTextUsername().addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                String key = dialog.getTextUsername().getText();
                if(key.equals(""))
                    dialog.getMyUsername().setText("myUsername");
                else{
                    if(AppUtility.haveSpecialChars(key)){
                        dialog.getTextUsername().setText("");
                        dialog.getMyUsername().setText("myUsername");
                        AppUtility.createPopOver((JTextField)e.getSource(), "You Have Illegal Characters ");
                    }else{
                        String[] myUsername = key.split(" ");
                        dialog.getMyUsername().setText(myUsername[0]);
                    }
                }
                    
            }
        });
        
        dialog.getTextContact().addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent e) {
                if(Character.isAlphabetic(e.getKeyChar())){
                    e.consume();
                }
            }
        });
        
        dialog.getTextEmail().addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                String key = dialog.getTextEmail().getText();
                if(AppUtility.haveSpecialChars(key)){
                    dialog.getTextEmail().setText("");
                    AppUtility.createPopOver((JTextField)e.getSource(), "You Have Illegal Characters ");
                }
            }
            
        });
        
        dialog.getTextAddress().addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                String key = dialog.getTextAddress().getText();
                if(AppUtility.haveSpecialChars(key)){
                    dialog.getTextAddress().setText("");
                    AppUtility.createPopOver((JTextField)e.getSource(), "You Have Illegal Characters ");
                }
            }
            
        });
    }
    
    private void resetUserText(){
        dialog.setAdministrator(false);
        dialog.getTextAddress().setText("");
        dialog.getTextContact().setText("");
        dialog.getTextEmail().setText("");
        dialog.getTextUsername().setText("");
        dialog.getTextPassword().setText("");
        dialog.getTextRePassword().setText("");
    }
    
    private void resetTablePermission(){
        for(int y=0;y<dialog.getPermissionPane().getComponentCount();y++){
            JTable gettingTable = ((JTable)dialog.getPermissionPane().getComponent(y));
            DefaultTableModel tmb = (DefaultTableModel) gettingTable.getModel();
            gettingTable.clearSelection();
            for(int i=0;i<tmb.getRowCount();i++){
                tmb.setValueAt(false, i, 1);
                tmb.setValueAt(false, i, 2);
            }
        }
        
    }
    
    private void enableUserText(boolean booleanValue){
        dialog.getTextAddress().setEnabled(booleanValue);
        dialog.getTextContact().setEnabled(booleanValue);
        dialog.getTextEmail().setEnabled(booleanValue);
        dialog.getTextUsername().setEnabled(booleanValue);
        dialog.getTextPassword().setEnabled(booleanValue);
        dialog.getTextRePassword().setEnabled(false);
        dialog.getIsAdministrator().setEnabled(booleanValue);
    }
    
    private void enableUserPermission(boolean booleanValue){
        for(int y=0;y<dialog.getPermissionPane().getComponentCount();y++){
            ((JTable)dialog.getPermissionPane().getComponent(y)).setEnabled(booleanValue);
        }
    }
    
    private void isAdministrator(){
        
        for(int y=0;y<dialog.getPermissionPane().getComponentCount();y++){
            JTable gettingTable = ((JTable)dialog.getPermissionPane().getComponent(y));
            DefaultTableModel tmb = (DefaultTableModel) gettingTable.getModel();
            for(int i=0;i<tmb.getRowCount();i++){
                tmb.setValueAt(dialog.isAdministrator(), i, 1);
                tmb.setValueAt(dialog.isAdministrator(), i, 2);
            }
            gettingTable.setEnabled(!dialog.isAdministrator());
            
        }
    }
    
    public Setting getDialog(){
        return dialog;
    }
    
    
    
}
