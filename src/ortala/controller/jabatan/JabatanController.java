/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ortala.controller.jabatan;

import ortala.appsource.ControllerTemplate;
import ortala.dbtrans.ortala.JabatanData;
import ortala.view.jabatan.JabatanView;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import ortala.appsource.AppUtility;
import ortala.appsource.TextAreaEditor;
import ortala.appsource.TextAreaNumberingEditor;
import ortala.dbtrans.ortala.KompetensiData;
import ortala.model.jabatan.Jabatan;
import ortala.model.jabatan.UnitKerja;
import ortala.model.master.MasterKompetensi;
import ortala.model.master.MasterKompetensiDef;
import ortala.model.master.MasterKompetensiLevel;
import ortala.model.tugas.Tugas;
import ortala.model.tugas.TugasDetail;
import ortala.view.jabatan.TemporaryTugas;

/**
 *
 * @author thread009
 */
public class JabatanController extends ControllerTemplate {

    JabatanView view;
    JabatanData data;
    KompetensiData dataKompetensi;
    List<TemporaryTugas> tempKompetensi;

    private enum KompetensiTracking {
        UPDATE_KOMPETENSI, UPDATE_DEFINISI, UPDATE_DESKRIPSI, TRACKING,
    }

    public JabatanController() {
        setControllerName("Jabatan");
        setControllerData(new JabatanData());
        setControllerView(new JabatanView());
        this.view = ((JabatanView) getControllerView());
        this.data = (JabatanData) getControllerData();
        this.dataKompetensi = new KompetensiData();
        this.tempKompetensi = new ArrayList<>();

        fillValueKompetensi();
        fillValueJabatan();

        setActionTable();
        setActionMain();
        setActionKompetensi();

    }

    private void setActionTable() {

        /*
         * Aksi untuk tabel data dan tombol yang berhubungan
         */
        view.getBtnAddTugas().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                DefaultTableModel tmb = (DefaultTableModel) view.getTableTugas().getModel();
                int row = tmb.getRowCount();
                tmb.addRow(new Object[]{
                    ++row, "", ""
                });
                TemporaryTugas tugas = new TemporaryTugas();
                tugas.setKodeTugas("TGS-" + AppUtility.randomNumericString(5));
                tempKompetensi.add(tugas);
            }
        });

        view.getBtnRemoveTugas().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int ro = view.getTableTugas().getSelectedRow();
                if (view.getTableTugas().isRowSelected(ro)) {
                    DefaultTableModel tmb = (DefaultTableModel) view.getTableTugas().getModel();

                    tmb.removeRow(ro);
                    tempKompetensi.remove(ro);

                    Object[][] row = new Object[tmb.getRowCount()][tmb.getColumnCount()];
                    for (int i = 0; i < tmb.getRowCount(); i++) {
                        for (int iCol = 0; iCol < tmb.getColumnCount(); iCol++) {
                            row[i][iCol] = tmb.getValueAt(i, iCol);
                        }
                    }
                    tmb.setNumRows(0);
                    for (int i = 0; i < row.length; i++) {
                        int r = i + 1;
                        tmb.addRow(new Object[]{
                            r,
                            row[i][1],
                            row[i][2]
                        });
                    }
                }
            }
        });

        view.getTableTugas().getSelectionModel().addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                int row = view.getTableTugas().getSelectedRow();
                if (view.getTableTugas().isRowSelected(row)) {
                    trackingKompetensi(KompetensiTracking.TRACKING);
                    view.getTugasEditor().setVisible(true);
                    view.getTugasEditor().setBorder(javax.swing.BorderFactory.createTitledBorder(
                            null,
                            "Atur Kompetensi untuk Tugas - " + (row + 1),
                            javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION,
                            javax.swing.border.TitledBorder.DEFAULT_POSITION,
                            new java.awt.Font("Tahoma", 1, 10)));
                    view.getBtnRemoveTugas().setEnabled(true);
                } else {
                    view.getTugasEditor().setVisible(false);
                    view.getBtnRemoveTugas().setEnabled(false);
                }
            }
        });

        view.getTableTugas().addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (SwingUtilities.isRightMouseButton(e)) {
                    view.getTableTugas().clearSelection();
                }
            }

        });

    }

    private void setActionMain() {
        /*
         * Aksi untuk penambanhan data
         */
        view.getBtnAddJabatan().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                setAllForm(true);
                view.getTextKodeJabatan().setText("JB-" + AppUtility.randomNumericString(5));

            }
        });

        view.getBtnSaveJabatan().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                saveJabatan();
            }
        });
    }

    private void setActionKompetensi() {
        view.getValKompetensi().addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                if (view.getValKompetensi().getSelectedIndex() == 0) {
                    rmItemListenerIfAdded(0);
                    rmItemListenerIfAdded(1);
                    view.getValKompetensiP().removeAllItems();
                    view.getTextKompetensiP().setText("");
                    view.getValKompetensiL().removeAllItems();
                    view.getTextKompetensiL().setText("");
                } else {
                    trackingKompetensi(KompetensiTracking.UPDATE_KOMPETENSI);
                    rmItemListenerIfAdded(0);
                    rmItemListenerIfAdded(1);
                    view.getValKompetensiP().removeAllItems();
                    view.getTextKompetensiP().setText("");
                    view.getValKompetensiL().removeAllItems();
                    view.getTextKompetensiL().setText("");
                    activatedValDefinisi();
                }
            }
        });

    }

    private void activatedValDefinisi() {
        rmItemListenerIfAdded(0);
        String kode = view.getValKompetensi().getSelectedItem().toString().split("/")[0];
        fillValueKompetensiP(kode);
        view.getValKompetensiP().addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                if (view.getValKompetensiP().getSelectedIndex() == 0) {
                    rmItemListenerIfAdded(1);
                    view.getValKompetensiL().removeAllItems();
                    view.getTextKompetensiP().setText("");
                    view.getTextKompetensiL().setText("");
                } else {
                    trackingKompetensi(KompetensiTracking.UPDATE_DEFINISI);
                    rmItemListenerIfAdded(1);
                    view.getValKompetensiL().removeAllItems();
                    view.getTextKompetensiP().setText("");
                    view.getTextKompetensiL().setText("");
                    activatedValDeskripsi();
                }
            }
        });
    }

    private void activatedValDeskripsi() {
        rmItemListenerIfAdded(1);
        String kodeDefinisi = view.getValKompetensiP().getSelectedItem().toString();
        try {
            MasterKompetensiDef def = dataKompetensi.getDefinisiByKodeDefinisi(kodeDefinisi);
            view.getTextKompetensiP().setText(def.getDefinisiKompetensi());
            fillValueKompetensiL(kodeDefinisi);
        } catch (SQLException ex) {
        }

        view.getValKompetensiL().addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                if (view.getValKompetensiL().getSelectedIndex() == 0) {
                    view.getTextKompetensiL().setText("");
                } else {
                    try {
                        trackingKompetensi(KompetensiTracking.UPDATE_DESKRIPSI);
                        String def = view.getValKompetensiP().getSelectedItem().toString();
                        String lv = view.getValKompetensiL().getSelectedItem().toString();
                        MasterKompetensiLevel l = dataKompetensi.getLevelByLevel(def, Integer.valueOf(lv));
                        view.getTextKompetensiL().setText(
                                l.getDeskripsi());
                    } catch (SQLException ex) {
                        Logger.getLogger(JabatanController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        });
    }

    private void rmItemListenerIfAdded(int source) {
        switch (source) {
            /*
             * Hapus ItemListener Definisi
             */
            case 0:
                ItemListener[] aListener = view.getValKompetensiP().getItemListeners();
                if (aListener.length > 0) {
                    view.getValKompetensiP().removeItemListener(aListener[0]);
                }
                break;
            /*
             * Hapus ItemListener Definisi
             */
            case 1:
                ItemListener[] aListenerr = view.getValKompetensiL().getItemListeners();
                if (aListenerr.length > 0) {
                    view.getValKompetensiL().removeItemListener(aListenerr[0]);
                }
                break;
        }
    }
    
    private void fillValueJabatan(){
        try{
            DefaultTableModel tmb  = (DefaultTableModel) view.getTableJabatan().getModel();
            tmb.setNumRows(0);
            int row = 0;
            for(Jabatan data : this.data.getAllData()){
                tmb.addRow(new Object[]{++row, data.getKodeJabatan(), data.getNamaJabatan()});
            }
        }catch(SQLException e){
            
        }
    }

    private void fillValueKompetensi() {
        view.getValKompetensi().removeAllItems();
        try {
            view.getValKompetensi().addItem("--Pilih Kompetensi--");
            for (MasterKompetensi data : this.dataKompetensi.getAllKompetensi()) {
                view.getValKompetensi().addItem(data.getKodeKompetensi() + "/" + data.getNamaKompetensi());
            }
        } catch (SQLException e) {

        }
    }

    private void fillValueKompetensiP(String kode) {
        view.getValKompetensiP().removeAllItems();
        try {
            view.getValKompetensiP().addItem("--Pilih Kompetensi--");
            for (MasterKompetensiDef data : this.dataKompetensi.getDefinisiByKodeKompetensi(kode)) {
                view.getValKompetensiP().addItem(data.getKodeDef());
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
    }

    private void fillValueKompetensiL(String kode) {
        view.getValKompetensiL().removeAllItems();
        try {
            view.getValKompetensiL().addItem("--Pilih Kompetensi--");
            for (MasterKompetensiLevel data : this.dataKompetensi.getLevelByKodeDefinisi(kode)) {
                view.getValKompetensiL().addItem(String.valueOf(data.getLevel()));
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
    }

    private void trackingKompetensi(KompetensiTracking source) {
        int row = view.getTableTugas().getSelectedRow();
        switch (source) {
            case TRACKING:
                String kodeKompetensi = tempKompetensi.get(row).getKodeKompetensi();
                String kodeDef = tempKompetensi.get(row).getKodeDef();
                Integer level = tempKompetensi.get(row).getLevel();
                System.out.println(kodeKompetensi);
                System.out.println(kodeDef);
                System.out.println(level);
                /*
                 *Select latest kompetensi tracking
                 */
                if (kodeKompetensi != null) {
                    for (int a = 0; a < view.getValKompetensi().getItemCount(); a++) {
                        String tmpKodeKompetensi = view.getValKompetensi().
                                getItemAt(a).toString().split("/")[0];
                        if (kodeKompetensi.equals(tmpKodeKompetensi)) {
                            view.getValKompetensi().setSelectedIndex(a);
                            break;
                        }
                    }
                } else {
                    view.getValKompetensi().setSelectedIndex(0);
                }

                /*
                 *Select latest definisi tracking
                 */
                if (kodeDef != null) {
                    for (int a = 0; a < view.getValKompetensiP().getItemCount(); a++) {
                        String tmpKodeDefinisi = view.getValKompetensiP().getItemAt(a);
                        if (kodeDef.equals(tmpKodeDefinisi)) {
                            view.getValKompetensiP().setSelectedIndex(a);
                            break;
                        }
                    }
                }

                /*
                 *Select latest definisi tracking
                 */
                if (level != null) {
                    for (int a = 1; a < view.getValKompetensiL().getItemCount(); a++) {
                        int tmpLevel = Integer.valueOf(view.getValKompetensiL().getItemAt(a));
                        if (level == tmpLevel) {
                            view.getValKompetensiL().setSelectedIndex(a);
                            break;
                        }
                    }
                }

                break;
            case UPDATE_KOMPETENSI:
                if (view.getValKompetensi().getSelectedIndex() == 0) {
                    return;
                }
                String upKodeKompetensi = view.getValKompetensi().getSelectedItem().toString().split("/")[0];
                tempKompetensi.get(row).setKodeKompetensi(upKodeKompetensi);

                break;
            case UPDATE_DEFINISI:
                if (view.getValKompetensiP().getSelectedIndex() == 0) {
                    return;
                }
                String upKodeDefinisi = view.getValKompetensiP().getSelectedItem().toString();
                tempKompetensi.get(row).setKodeDef(upKodeDefinisi);
                break;
            case UPDATE_DESKRIPSI:
                if (view.getValKompetensiL().getSelectedIndex() == 0) {
                    return;
                }
                Integer upLevel = Integer.valueOf(view.getValKompetensiL().getSelectedItem().toString());
                tempKompetensi.get(row).setLevel(upLevel);
                break;
        }

    }

    private void saveJabatan() {
        if (validation(0)) {
            try {
                boolean jabatanSucc = this.data.insertJabatan(createJabatan());
                boolean unitKerjaSucc = this.data.insertUnitKerja(createUnitKerja());
                boolean tugasSucc = this.data.insertTugas(this.createUraianTugas(), this.createUraianTugasDetail());

                if (jabatanSucc && unitKerjaSucc && tugasSucc) {
                    JOptionPane.showMessageDialog(view, "Berhasil Menyimpan Data");
                } else {
                    JOptionPane.showMessageDialog(view, "Gagal Menyimpan Data");
                }
            } catch (SQLException e) {
                JOptionPane.showMessageDialog(view, e);
            }
        }
    }

    private void setAllForm(boolean values) {
        view.getBtnAddJabatan().setEnabled(!values);
        view.getBtnSaveJabatan().setEnabled(values);
        view.getValNamaJabatan().setEnabled(values);
        view.getValJenjangJabatan().setEnabled(values);
        view.getValIkhtisarJabatan().setEnabled(values);
        view.getBtnAddTugas().setEnabled(values);
        view.getValEselon1().setEnabled(values);
        view.getValEselon2().setEnabled(values);
        view.getValEselon3().setEnabled(values);
        view.getValEselon4().setEnabled(values);
    }

    private boolean validation(int source) {
        boolean isValid = false;
        switch (source) {
            case 0:
                if (view.getValNamaJabatan().getText().equals("")) {
                    JOptionPane.showMessageDialog(view, "Nama Jabatan");
                    view.getValNamaJabatan().requestFocus();
                    return false;
                } else if (view.getValJenjangJabatan().getText().equals("")) {
                    JOptionPane.showMessageDialog(view, "Jenjang Jabatan");
                    view.getValJenjangJabatan().requestFocus();
                    return false;
                } else if (view.getValIkhtisarJabatan().getText().equals("")) {
                    JOptionPane.showMessageDialog(view, "Ikhtisar");
                    view.getValIkhtisarJabatan().requestFocus();
                    return false;
                } else if (view.getValEselon1().getText().equals("")) {
                    JOptionPane.showMessageDialog(view, "Eselon I");
                    view.getValEselon1().requestFocus();
                    return false;
                } else if (view.getValEselon2().getText().equals("")) {
                    JOptionPane.showMessageDialog(view, "Eselon II");
                    view.getValEselon2().requestFocus();
                    return false;
                } else if (view.getValEselon3().getText().equals("")) {
                    JOptionPane.showMessageDialog(view, "Eselon III");
                    view.getValEselon3().requestFocus();
                    return false;
                } else if (view.getValEselon4().getText().equals("")) {
                    JOptionPane.showMessageDialog(view, "Eselon IV");
                    view.getValEselon4().requestFocus();
                    return false;
                } else {
                    return true;
                }
            case 1:
                if (view.getTableTugas().getRowCount() == 0) {
                    return false;
                }
                for (int i = 0; i < view.getTableTugas().getRowCount(); i++) {
                    String desc = ((TextAreaEditor) view.getTableTugas().getCellEditor(i, 1)).getJTextArea().getText();
                    String uraian = ((TextAreaNumberingEditor) view.getTableTugas().getCellEditor(i, 2)).getCellEditorValue().toString();
                    if (desc.equals("") || uraian.equals("")) {
                        return false;
                    }

                }
                for (TemporaryTugas tgs : tempKompetensi) {
                    if (tgs.getKodeDef() == null || tgs.getKodeKompetensi() == null || tgs.getLevel() == null) {
                        return false;
                    }
                }
        }
        return isValid;
    }

    private Jabatan createJabatan() {
        Jabatan data = null;
        String kodeJabatan = view.getTextKodeJabatan().getText();
        String namaJabatan = view.getValNamaJabatan().getText();
        String ikhtisar = view.getValIkhtisarJabatan().getText();

        data = new Jabatan(0, ikhtisar, kodeJabatan, namaJabatan);
        return data;
    }

    private UnitKerja createUnitKerja() {

        return new UnitKerja(0,
                view.getTextKodeJabatan().getText(),
                view.getValEselon1().getText(),
                view.getValEselon2().getText(),
                view.getValEselon3().getText(),
                view.getValEselon4().getText());

    }

    private List<Tugas> createUraianTugas() {
        List<Tugas> data = new ArrayList<Tugas>();
        for (int i = 0; i < view.getTableTugas().getRowCount(); i++) {
            data.add(new Tugas(
                    0,
                    view.getTextKodeJabatan().getText(),
                    this.tempKompetensi.get(i).getKodeTugas(),
                    view.getTableTugas().getValueAt(i, 1).toString(),
                    "Kata Kunci--untuk nanti",
                    this.tempKompetensi.get(i).getKodeKompetensi(),
                    this.tempKompetensi.get(i).getKodeDef(),
                    this.tempKompetensi.get(i).getLevel()
            ));
        }
        return data;
    }

    private List<TugasDetail> createUraianTugasDetail() {
        List<TugasDetail> data = new ArrayList<TugasDetail>();
        for (int i = 0; i < view.getTableTugas().getRowCount(); i++) {
            String[] splitPar = view.getTableTugas().getValueAt(i, 2).toString().split("\\n");
            int pnt = 0;

            for (String str : splitPar) {
                data.add(new TugasDetail(
                        0,
                        this.tempKompetensi.get(i).getKodeTugas(),
                        ++pnt,
                        str,
                        "Kata Kunci--untuk nanti"
                ));
            }

        }
        return data;
    }

}
