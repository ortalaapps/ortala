/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ortala.dbtrans.ortala;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import ortala.appsource.AppProfile;
import ortala.appsource.DataTemplate;
import ortala.model.master.MasterJabatan;

/**
 *
 * @author SALAM29
 */
public class MasterJabatanData extends DataTemplate {

    public MasterJabatanData() {
        setConnection(AppProfile.DB_CONNECTION);
    }
    
    //INSERT DATA JABATAN
    public boolean insertJabatan(MasterJabatan jabatan) throws SQLException{
        
        String sql = "INSERT INTO master_jabatan(kode_instansi, kode_jabatan, nama_jabatan, tipe_jabatan, peran_jabatan) VALUES(?, ?, ?, ?, ?)";
        PreparedStatement st = getConnection().prepareStatement(sql);
        st.setString(1, jabatan.getKodeInstansi());
        st.setString(2, jabatan.getKodeJabatan());
        st.setString(3, jabatan.getNamaJabatan());
        st.setInt(4, jabatan.getTipeJabatan());
        st.setString(5, jabatan.getPeranJabatan());
         
        return st.executeUpdate()==1 ? true:false;
    }
    
   //SELECT DATA
    public List<MasterJabatan> getAllJabatan() throws SQLException{
        List<MasterJabatan> data = new ArrayList<MasterJabatan>();
        String sql = "SELECT *FROM master_Jabatan";
        PreparedStatement st = getConnection().prepareStatement(sql);
        ResultSet rs = st.executeQuery();
        while(rs.next()){
            data.add(new MasterJabatan(
                    rs.getInt(1),
                    rs.getString(2),
                    rs.getString(3),
                    rs.getString(4),
                    rs.getInt(5),
                    rs.getString(6)                   
            ));
        }
        return data;
    }
    
}
