/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ortala.dbtrans.ortala;

import ortala.appsource.AppProfile;
import ortala.appsource.DataTemplate;
import ortala.model.jabatan.Jabatan;
import ortala.model.jabatan.UnitKerja;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import ortala.model.tugas.Tugas;
import ortala.model.tugas.TugasDetail;

/**
 *
 * @author thread009
 */
public class JabatanData extends DataTemplate {

    public JabatanData() {
        setConnection(AppProfile.DB_CONNECTION);
    }

    public boolean insertJabatan(Jabatan data) throws SQLException {

        PreparedStatement st = null;
        String query = "INSERT INTO tb_jabatan(kode_jabatan, nama_jabatan, ikhtisar) VALUES(?, ?, ?)";
        st = getConnection().prepareStatement(query);
        st.setString(1, data.getKodeJabatan());
        st.setString(2, data.getNamaJabatan());
        st.setString(3, data.getIkhtisar());

        return st.executeUpdate() == 1 ? true : false;
    }

    public boolean insertUnitKerja(UnitKerja data) throws SQLException {
        PreparedStatement st = null;
        String query = "INSERT INTO tb_unit_kerja(kode_jabatan, eselon_1, eselon_2, eselon_3, eselon_4) VALUES(?, ?, ?, ?, ?)";
        st = getConnection().prepareStatement(query);
        st.setString(1, data.getKodeJabatan());
        st.setString(2, data.getEselon1());
        st.setString(3, data.getEselon2());
        st.setString(4, data.getEselon3());
        st.setString(5, data.getEselon4());

        return st.executeUpdate() == 1 ? true : false;
    }

    public boolean insertTugas(List<Tugas> tugasAll, List<TugasDetail> tugasDetail) throws SQLException {
        PreparedStatement st = null;
        PreparedStatement st2 = null;

        String query = "INSERT INTO tb_uraian_tugas"
                + "(kode_jabatan, kode_tugas, uraian_tugas, kata_kunci, kode_kompetensi, kode_def, level) "
                + "VALUES(?, ?, ?, ?, ?, ?, ?)";
        String query2 = "INSERT INTO tb_uraian_tugas_detail(kode_tugas, point, detail, kata_kunci) "
                + "VALUES(?, ?, ?, ?)";

        st = getConnection().prepareStatement(query);
        for (Tugas tugas : tugasAll) {
            st.setString(1, tugas.getKodeJabatan());
            st.setString(2, tugas.getKodeTugas());
            st.setString(3, tugas.getUraianTugas());
            st.setString(4, tugas.getKataKunci());
            st.setString(5, tugas.getKodeKompetensi());
            st.setString(6, tugas.getKodeDef());
            st.setInt(7, tugas.getLevel());
            st.addBatch();
        }

        st2 = getConnection().prepareStatement(query2);
        for (TugasDetail dt : tugasDetail) {
            st2.setString(1, dt.getKodeTugas());
            st2.setInt(2, dt.getPoint());
            st2.setString(3, dt.getDetail());
            st2.setString(4, dt.getKataKunci());
            st2.addBatch();
        }

        return st.executeBatch().length > 0 && st2.executeBatch().length > 0 ? true : false;
    }

    public List<Jabatan> getAllData() throws SQLException {
        List<Jabatan> data = new ArrayList<Jabatan>();
        String sql = "SELECT *FROM tb_jabatan";
        PreparedStatement st = getConnection().prepareStatement(sql);
        ResultSet res = st.executeQuery();
        while (res.next()) {
            data.add(new Jabatan(res.getDouble(1), res.getString(2), res.getString(3), res.getString(4)));
        }
        return data;
    }

}
