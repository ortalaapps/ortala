/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ortala.dbtrans.ortala;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import ortala.appsource.AppProfile;
import ortala.appsource.DataTemplate;
import ortala.model.evajab.Evajab;
import ortala.model.evajab.EvajabDetail;
import ortala.model.evajab.EvajabHasilKerja;
import ortala.model.evajab.EvajabTanggungJawab;
import ortala.model.master.MasterFaktor;
import ortala.model.master.MasterFaktorDetail;
import ortala.model.master.MasterInstansi;
import ortala.model.master.MasterJabatan;
import ortala.model.master.MasterJabatanUraian;

/**
 *
 * @author DELL
 */
public class EvajabData extends DataTemplate {
    
    public EvajabData() {
        setConnection(AppProfile.DB_CONNECTION);
    }
    
    public List<MasterInstansi> getMasterInstansi() throws SQLException{
        List<MasterInstansi> instansi = new ArrayList<>();
        PreparedStatement ps = getConnection().prepareStatement("SELECT * FROM master_instansi ORDER BY id ASC");
        ResultSet rs = ps.executeQuery();
        while (rs.next()) {            
            instansi.add(new MasterInstansi(rs.getInt(1), rs.getString(2), rs.getString(3)));
        }
        return instansi;
    }

    public List<MasterJabatan> getMasterJabatan(String kodeInstansi) throws SQLException{
        List<MasterJabatan> jabatan = new ArrayList<>();
        PreparedStatement ps = getConnection().prepareStatement("SELECT * FROM master_jabatan WHERE kode_instansi = ? ORDER BY id ASC");
        ps.setString(1, kodeInstansi);
        ResultSet rs = ps.executeQuery();
        while (rs.next()) {            
            jabatan.add(new MasterJabatan(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getInt(5), rs.getString(6)));
        }
        return jabatan;
    }
    
    public List<MasterJabatanUraian> getUraianJabatan (String kodeJabatan) throws SQLException {
        List<MasterJabatanUraian> uraian = new ArrayList<>();
        PreparedStatement ps = getConnection().prepareStatement("SELECT * FROM master_jabatan_uraian WHERE kode_jabatan = ? ORDER BY id ASC");
        ps.setString(1, kodeJabatan);
        ResultSet rs = ps.executeQuery();
        while (rs.next()) {            
            uraian.add(new MasterJabatanUraian(rs.getInt(1), rs.getString(2), rs.getString(3)));
        }
        return uraian;
    }
    
    public List<MasterFaktor> getFaktorJabatan(Integer tipe) throws SQLException{
        List<MasterFaktor> faktor = new ArrayList<>();
        PreparedStatement ps = getConnection().prepareStatement("SELECT * FROM master_faktor WHERE tipe_jabatan = ? ORDER BY id ASC");
        ps.setInt(1, tipe);
        ResultSet rs = ps.executeQuery();
        while (rs.next()) {            
            faktor.add(new MasterFaktor(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getInt(4)));
        }
        return faktor;
    }
    
    public List<MasterFaktorDetail> getFaktorDetailJabatan(String kodeFaktor) throws SQLException{
        List<MasterFaktorDetail> detail = new ArrayList<>();
        PreparedStatement ps = getConnection().prepareStatement("SELECT * FROM master_faktor_detail WHERE kode_faktor = ? ORDER BY id ASC");
        ps.setString(1, kodeFaktor);
        ResultSet rs = ps.executeQuery();
        while (rs.next()) {            
            detail.add(new MasterFaktorDetail(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getInt(4), rs.getInt(5)));
        }
        return detail;
    }

    public boolean insertDataEvajab(Evajab data) throws SQLException{
        PreparedStatement ps = getConnection().prepareStatement("INSERT INTO tb_evajab (kode_evajab, kode_instansi, kode_jabatan, tanggal) VALUES (?, ?, ?, ?)");
        ps.setString(1, data.getKodeEvajab());
        ps.setString(2, data.getKodeInstansi());
        ps.setString(3, data.getKodeJabatan());
        ps.setDate(4, (Date) data.getTanggal());
        return ps.executeUpdate() == 1 ? true : false;
    }
    
    public boolean insertDataEvajabDetail(List<EvajabDetail> data) throws SQLException {
        PreparedStatement ps = getConnection().prepareStatement("INSERT INTO tb_evajab_detail (kode_evajab, kode_faktor, level_faktor, nilai_faktor) VALUES (?, ?, ?, ?)");
        for (EvajabDetail a : data) {
            ps.setString(1, a.getKodeEvajab());
            ps.setString(2, a.getKodeFaktor());
            ps.setInt(3, a.getLevelFaktor());
            ps.setInt(4, a.getNilaiFaktor());
            ps.addBatch();
        }
        return ps.executeBatch().length >= 1 ? true : false;
    }
    
    public boolean insertEvajabHasilKerja(List<EvajabHasilKerja> data) throws SQLException {
        PreparedStatement ps = getConnection().prepareStatement("INSERT INTO tb_evajab_hasil_kerja (kode_evajab, kode_jabatan, deskripsi) VALUES (?, ?, ?)");
        for (EvajabHasilKerja a : data) {
            ps.setString(1, a.getKodeEvajab());
            ps.setString(2, a.getKodeJabatan());
            ps.setString(3, a.getDeskripsi());
            ps.addBatch();
        }
        return ps.executeBatch().length >= 1 ? true : false;
    }
    
    public boolean insertEvajabTanggungJawab(List<EvajabTanggungJawab> data) throws SQLException {
        PreparedStatement ps = getConnection().prepareStatement("INSERT INTO tb_evajab_tanggung_jawab (kode_evajab, kode_jabatan, deskripsi) VALUES (?, ?, ?)");
        for (EvajabTanggungJawab a : data) {
            ps.setString(1, a.getKodeEvajab());
            ps.setString(2, a.getKodeJabatan());
            ps.setString(3, a.getDeskripsi());
            ps.addBatch();
        }
        return ps.executeBatch().length >= 1 ? true : false;
    }
    
    /* HELPER */
    public boolean isNomorEvajabExist(String id) throws SQLException{
        PreparedStatement ps = null;
        ps = connection.prepareStatement("SELECT COUNT(*) FROM tb_evajab WHERE kode_evajab = ?");
        ps.setString(1, id);
        ResultSet rs = ps.executeQuery();
        if (rs.next()) if (rs.getInt(1) > 0) return true;
        rs.close();
        return false;
    }
    
    public Integer getStruturalJabatan(String kodeJabatan) throws SQLException{
        PreparedStatement ps = getConnection().prepareStatement("SELECT tipe_jabatan FROM master_jabatan WHERE kode_jabatan = ?");
        ps.setString(1, kodeJabatan);
        ResultSet rs = ps.executeQuery();
        while (rs.next()) {            
            return rs.getInt(1);
        }
        return null;
    }
    
    public String getPeranJabatan(String kodeJabatan) throws SQLException{
        PreparedStatement ps = getConnection().prepareStatement("SELECT peran_jabatan FROM master_jabatan WHERE kode_jabatan = ?");
        ps.setString(1, kodeJabatan);
        ResultSet rs = ps.executeQuery();
        while (rs.next()) {            
            return rs.getString(1);
        }
        return null;
    }
}
