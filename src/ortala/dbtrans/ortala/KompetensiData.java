/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ortala.dbtrans.ortala;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import ortala.appsource.AppProfile;
import ortala.appsource.DataTemplate;
import ortala.model.master.MasterKompetensi;
import ortala.model.master.MasterKompetensiDef;
import ortala.model.master.MasterKompetensiLevel;

/**
 *
 * @author thread009
 */
public class KompetensiData extends DataTemplate {
    
    public  KompetensiData(){
        setConnection(AppProfile.DB_CONNECTION);
    }
    
    public boolean insertKompetensi(MasterKompetensi masterKompetensi) throws SQLException{
        
        String sql = "INSERT INTO master_kompetensi(kode_kompetensi, nama_kompetensi) VALUES(?, ?)";
        PreparedStatement st = getConnection().prepareStatement(sql);
        st.setString(1, masterKompetensi.getKodeKompetensi());
        st.setString(2, masterKompetensi.getNamaKompetensi());
        
        return st.executeUpdate()==1 ? true:false;
    }
    
    public boolean insertDefinisi(MasterKompetensiDef masterKompetensi) throws SQLException{
        
        String sql = "INSERT INTO master_kompetensi_def"
                + "(kode_kompetensi, kode_def, definisi_kompetensi) "
                + "VALUES(?, ?, ?)";
        PreparedStatement st = getConnection().prepareStatement(sql);
        st.setString(1, masterKompetensi.getKodeKompetensi());
        st.setString(2, masterKompetensi.getKodeDef());
        st.setString(3, masterKompetensi.getDefinisiKompetensi());
        
        return st.executeUpdate()==1 ? true:false;
    }
    
    public boolean insertLevel(MasterKompetensiLevel masterKompetensi) throws SQLException{
        
        String sql = "INSERT INTO master_kompetensi_level"
                + "(kode_kompetensi, kode_def, level, deskripsi) "
                + "VALUES(?, ?, ?, ?)";
        PreparedStatement st = getConnection().prepareStatement(sql);
        st.setString(1, masterKompetensi.getKodeKompetensi());
        st.setString(2, masterKompetensi.getKodeDef());
        st.setInt(3, masterKompetensi.getLevel());
        st.setString(4, masterKompetensi.getDeskripsi());
        
        return st.executeUpdate()==1 ? true:false;
    }
    
    //SELECT DATA
    public List<MasterKompetensi> getAllKompetensi() throws SQLException{
        List<MasterKompetensi> data = new ArrayList<MasterKompetensi>();
        String sql = "SELECT *FROM master_kompetensi";
        PreparedStatement st = getConnection().prepareStatement(sql);
        ResultSet rs = st.executeQuery();
        while(rs.next()){
            data.add(new MasterKompetensi(
                    rs.getInt(1),
                    rs.getString(2),
                    rs.getString(3)
            ));
        }
        return data;
    }
    
    public MasterKompetensi getKompetensiByKode(String kode) throws SQLException{
        MasterKompetensi data = null;
        String sql = "SELECT *FROM master_kompetensi WHERE kode_kompetensi = ?";
        PreparedStatement st = getConnection().prepareStatement(sql);
        st.setString(1, kode);
        ResultSet rs = st.executeQuery();
        if(rs.next()){
            data = new MasterKompetensi(
                    rs.getInt(1),
                    rs.getString(2),
                    rs.getString(3)
            );
        }
        return data;
    }
    
    public List<MasterKompetensiDef> getAllDefinisi() throws SQLException{
        List<MasterKompetensiDef> data = new ArrayList<MasterKompetensiDef>();
        String sql = "SELECT *FROM master_kompetensi_def";
        PreparedStatement st = getConnection().prepareStatement(sql);
        ResultSet rs = st.executeQuery();
        while(rs.next()){
            data.add(new MasterKompetensiDef(
                    rs.getInt(1),
                    rs.getString(2),
                    rs.getString(3),
                    rs.getString(4)
            ));
        }
        return data;
    }
    
    public List<MasterKompetensiDef> getDefinisiByKodeKompetensi(String kode) throws SQLException{
        List<MasterKompetensiDef> data = new ArrayList<MasterKompetensiDef>();
        String sql = "SELECT *FROM master_kompetensi_def WHERE kode_kompetensi = ?";
        PreparedStatement st = getConnection().prepareStatement(sql);
        st.setString(1, kode);
        ResultSet rs = st.executeQuery();
        while(rs.next()){
            data.add(new MasterKompetensiDef(
                    rs.getInt(1),
                    rs.getString(2),
                    rs.getString(3),
                    rs.getString(4)
            ));
        }
        return data;
    }
    
    public MasterKompetensiDef getDefinisiByKodeDefinisi(String kode) throws SQLException{
        MasterKompetensiDef data = null;
        String sql = "SELECT *FROM master_kompetensi_def WHERE kode_def = ?";
        PreparedStatement st = getConnection().prepareStatement(sql);
        st.setString(1, kode);
        ResultSet rs = st.executeQuery();
        if(rs.next()){
            data = new MasterKompetensiDef(
                    rs.getInt(1),
                    rs.getString(2),
                    rs.getString(3),
                    rs.getString(4)
            );
        }
        return data;
    }
    
    public List<MasterKompetensiLevel> getAllKompetensiLevel() throws SQLException{
        List<MasterKompetensiLevel> data = new ArrayList<MasterKompetensiLevel>();
        String sql = "SELECT *FROM master_kompetensi";
        PreparedStatement st = getConnection().prepareStatement(sql);
        ResultSet rs = st.executeQuery();
        while(rs.next()){
            data.add(new MasterKompetensiLevel(
                    rs.getInt(1),
                    rs.getString(2),
                    rs.getString(3),
                    rs.getInt(4),
                    rs.getString(5)
            ));
        }
        return data;
    }
    
    public List<MasterKompetensiLevel> getLevelByKodeKompetensi(String kode) throws SQLException{
        List<MasterKompetensiLevel> data = new ArrayList<MasterKompetensiLevel>();
        String sql = "SELECT *FROM master_kompetensi_level WHERE kode_kompetensi = ?";
        PreparedStatement st = getConnection().prepareStatement(sql);
        st.setString(1, kode);
        ResultSet rs = st.executeQuery();
        while(rs.next()){
            data.add(new MasterKompetensiLevel(
                    rs.getInt(1),
                    rs.getString(2),
                    rs.getString(3),
                    rs.getInt(4),
                    rs.getString(5)
            ));
        }
        return data;
    }
    
    public List<MasterKompetensiLevel> getLevelByKodeDefinisi(String kode) throws SQLException{
        List<MasterKompetensiLevel> data = new ArrayList<MasterKompetensiLevel>();
        String sql = "SELECT *FROM master_kompetensi_level WHERE kode_def = ?";
        PreparedStatement st = getConnection().prepareStatement(sql);
        st.setString(1, kode);
        ResultSet rs = st.executeQuery();
        while(rs.next()){
            data.add(new MasterKompetensiLevel(
                    rs.getInt(1),
                    rs.getString(2),
                    rs.getString(3),
                    rs.getInt(4),
                    rs.getString(5)
            ));
        }
        return data;
    }
    
    public MasterKompetensiLevel getLevelByLevel(String kodeDefinisi, int level) throws SQLException{
        MasterKompetensiLevel data = null;
        String sql = "SELECT *FROM master_kompetensi_level WHERE kode_def = ? AND level = ?";
        PreparedStatement st = getConnection().prepareStatement(sql);
        st.setString(1, kodeDefinisi);
        st.setInt(2, level);
        ResultSet rs = st.executeQuery();
        if(rs.next()){
            data = new MasterKompetensiLevel(
                    rs.getInt(1),
                    rs.getString(2),
                    rs.getString(3),
                    rs.getInt(4),
                    rs.getString(5)
            );
        }
        return data;
    }
    
    public int getLevelCount(String kodeDefinisi) throws SQLException{
        MasterKompetensiLevel data = null;
        String sql = "SELECT COUNT(*) FROM master_kompetensi_level WHERE kode_def = ?";
        PreparedStatement st = getConnection().prepareStatement(sql);
        st.setString(1, kodeDefinisi);
        ResultSet rs = st.executeQuery();
        if(rs.next()){
            return rs.getInt(1);
        }
        return 0;
    }
    

}
