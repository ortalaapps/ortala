/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ortala.dbtrans.ortala;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import ortala.appsource.AppProfile;
import ortala.appsource.DataTemplate;
import ortala.model.master.MasterInstansi;

/**
 *
 * @author SALAM29
 */
public class MasterInstansiData extends DataTemplate {

    public MasterInstansiData() {
        setConnection(AppProfile.DB_CONNECTION);
    }
    
    //INSERT DATA INSTANSI
    public boolean insertInstansi(MasterInstansi masterinstansi) throws SQLException{
        
        String sql = "INSERT INTO master_instansi(kode_instansi, nama_instansi) VALUES(?, ?)";
        PreparedStatement st = getConnection().prepareStatement(sql);
        st.setString(1, masterinstansi.getKodeInstansi());
        st.setString(2, masterinstansi.getNamaInstansi());
        
        return st.executeUpdate()==1 ? true:false;
    }
    
    //SELECT DATA ISNTANSI
    //SELECT DATA
    public List<MasterInstansi> getAllInstansi() throws SQLException{
        List<MasterInstansi> data = new ArrayList<MasterInstansi>();
        String sql = "SELECT *FROM master_instansi";
        PreparedStatement st = getConnection().prepareStatement(sql);
        ResultSet rs = st.executeQuery();
        while(rs.next()){
            data.add(new MasterInstansi(
                    rs.getInt(1),
                    rs.getString(2),
                    rs.getString(3)
            ));
        }
        return data;
    }
    
    //SELECT MASTER INSTANSI
    public List<MasterInstansi> getMasterInstansi() throws SQLException{
        List<MasterInstansi> instansi = new ArrayList<>();
        PreparedStatement ps = getConnection().prepareStatement("SELECT * FROM master_instansi ORDER BY id ASC");
        ResultSet rs = ps.executeQuery();
        while (rs.next()) {            
            instansi.add(new MasterInstansi(rs.getInt(1), rs.getString(2), rs.getString(3)));
        }
        return instansi;
    }
}
