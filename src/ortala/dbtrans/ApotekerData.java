/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ortala.dbtrans;

import ortala.appsource.AppProfile;
import ortala.model.ApotekerModel;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author LontongSayur
 */
public class ApotekerData {
    
    public Connection connection;
    
    public ApotekerData(){
        this.connection = AppProfile.DB_CONNECTION;
    }
    
    public ortala.model.ApotekerModel getApoteker(String nama, String password) throws SQLException{
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SELECT * FROM master_apoteker WHERE username= ? AND password = ?";
        st = connection.prepareStatement(query);
        st.setString(1, nama);
        st.setString(2, password);
        rs = st.executeQuery();
        if(rs.next()){
            return new ortala.model.ApotekerModel(
                    rs.getInt(1), 
                    rs.getString(2), 
                    rs.getString(3), 
                    rs.getString(4),
                    rs.getString(5),
                    rs.getString(6),
                    rs.getString(7),
                    rs.getString(8),
                    rs.getInt(9));
        }else{
            return null;
        }
    }
    
    public List<ortala.model.ApotekerModel> getApotekers() throws SQLException{
        PreparedStatement st = null;
        ResultSet rs = null;
        List<ortala.model.ApotekerModel> data = new ArrayList<ortala.model.ApotekerModel>();
        String query = "SELECT * FROM master_apoteker";
        st = connection.prepareStatement(query);
        rs = st.executeQuery();
        while(rs.next()){
            data.add(new ortala.model.ApotekerModel(
                    rs.getInt(1), 
                    rs.getString(2), 
                    rs.getString(3), 
                    rs.getString(4),
                    rs.getString(5),
                    rs.getString(6),
                    rs.getString(7),
                    rs.getString(8),
                    rs.getInt(9)));
        }
        
        return data;
    }
    
    //CRUD
    public boolean crudApoteker(ApotekerModel model) throws SQLException{
        PreparedStatement statement = null;
        String query = ""
                + "INSERT INTO master_apoteker"
                + "(apoteker_id, nama_apoteker, no_contact, e_mail, alamat, username, password, user_level)"
                + "VALUES"
                + "(?, ?, ?, ?, ?, ?, ?, ?)";
        statement = connection.prepareStatement(query);
        statement.setString(1, model.getApotekerId());
        statement.setString(2, model.getNamaApoteker());
        statement.setString(3, model.getNoContact());
        statement.setString(4, model.geteMail());
        statement.setString(5, model.getAlamat());
        statement.setString(6, model.getUsername());
        statement.setString(7, model.getPassword());
        statement.setInt(8, model.getUserLevel());
        
        return statement.executeUpdate()==1 ? true:false;
    }
    
    
    
}
