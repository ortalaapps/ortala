/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ortala.dbtrans;

import ortala.appsource.AppProfile;
import ortala.model.ApotekerModel;
import ortala.model.PermissionModel;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author thread009
 */
public class PermissionData {
    public Connection connection;
    
    public PermissionData(){
        this.connection = AppProfile.DB_CONNECTION;
    }
    
    public List<ortala.model.PermissionModel> getPermission(String idApoteker) throws SQLException{
        PreparedStatement st = null;
        ResultSet rs = null;
        List<ortala.model.PermissionModel> data = new ArrayList<ortala.model.PermissionModel>();
        String query = "SELECT * FROM master_apoteker_permission WHERE apoteker_id = ?";
        st = connection.prepareStatement(query);
        st.setString(1, idApoteker);
        rs = st.executeQuery();
        while(rs.next()){
            ortala.model.PermissionModel permission = new ortala.model.PermissionModel();
            permission.setApotekerId(rs.getString(1));
            permission.setPermissionId(rs.getString(2));
            if(rs.getInt(3) == 0) permission.setView(false);
            else permission.setView(true);
            if(rs.getInt(4) == 0) permission.setEdit(false);
            else permission.setEdit(true);
            data.add(permission);
        }
        
        return data;
    }
    
    //CRUD
    public boolean crudPermission(List<PermissionModel> data) throws SQLException{
        PreparedStatement statement = null;
        String query = ""
                + "INSERT INTO master_apoteker_permission"
                + "(apoteker_id, permission_id, view, edit)"
                + "VALUES"
                + "(?, ?, ?, ?)";
        statement = connection.prepareStatement(query);
        for(PermissionModel model : data){
            statement.setString(1, model.getApotekerId());
            statement.setString(2, model.getPermissionId());
            if(model.isView()) statement.setInt(3, 1);
            else statement.setInt(3, 0);
            if(model.isEdit()) statement.setInt(4, 1);
            else statement.setInt(4, 0);
            statement.addBatch();
        }
        
        return statement.executeBatch().length > 0 ? true:false;
    }
    
}
