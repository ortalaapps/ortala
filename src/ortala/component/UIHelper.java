package ortala.component;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author raditya
 */
public class UIHelper {
    
    private static JDialog loadingDialog = new JDialog();
    private static ImageIcon im = new ImageIcon(System.getProperties().getProperty("user.dir") + File.separatorChar + "src" + File.separatorChar + "img" + File.separatorChar + "loading.gif");
    
    public static Color defaultFontColor = new Color(76, 76, 76);
    public static Color fieldUnfocusColor = new Color(203, 203, 203);
    
    public static Font defaultFont = new Font("Tahoma", Font.PLAIN, 12);
    public static Font defaultFontBold = new Font("Tahoma", Font.BOLD, 12);
    public static Font fieldUnfocusFont = new Font("Tahoma", Font.ITALIC, 12);
    
    public static void callLoadingDialog() {
        
        loadingDialog.setResizable(false);
        loadingDialog.setSize(300, 100);
        
        JPanel panel = new JPanel(new GridLayout(2, 1, 0, 0));
        JLabel lText = new JLabel("Loading...");
        lText.setFont(new Font("Tahoma", Font.PLAIN, 12));
        lText.setHorizontalAlignment(JLabel.CENTER);
        JLabel lIcon = new JLabel(im);
        
        panel.add(lText);
        panel.add(lIcon);
        
        loadingDialog.add(panel, BorderLayout.CENTER);
        loadingDialog.setLocationRelativeTo(null);
        loadingDialog.setVisible(true);
        loadingDialog.isAlwaysOnTop();
        
    }
    
    public static void killLoadingDialog() {
        loadingDialog.setVisible(false);
    }
    
    public static void setPlaceholder(final JTextField textField, final String text) {
        textField.setText(text);
        textField.setForeground(fieldUnfocusColor);
        textField.setFont(fieldUnfocusFont);
        textField.addFocusListener(new FocusAdapter() {
            @Override
            public void focusGained(FocusEvent e) {
                if(textField.getText().trim().equals(text)) {
                    textField.setFont(defaultFont);
                    textField.setForeground(defaultFontColor);
                    textField.setText("");
                }
            }

            @Override
            public void focusLost(FocusEvent e) {
                if(textField.getText().trim().equals("")) {
                    textField.setFont(fieldUnfocusFont);
                    textField.setForeground(fieldUnfocusColor);
                    textField.setText(text);
                }
            }
        });
    }
    
    public static Color blueHoverOnColor = new Color(0, 15, 251);
    public static Color redHoverOnColor = new Color(250, 38, 30);
    public static void addHoverBlue(final JLabel label) {
        label.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseEntered(MouseEvent e) {
                label.setForeground(defaultFontColor);
            }

            @Override
            public void mouseExited(MouseEvent e) {
                label.setForeground(blueHoverOnColor);
            }
        });
    }
    
    public static void addHoverRed(final JLabel label) {
        label.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseEntered(MouseEvent e) {
                label.setForeground(defaultFontColor);
            }

            @Override
            public void mouseExited(MouseEvent e) {
                label.setForeground(redHoverOnColor);
            }
        });
    }
    
}
