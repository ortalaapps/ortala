/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ortala.component;


import java.awt.Component;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.plaf.TabbedPaneUI;

/**
 *
 * @author thread009
 */
public class CloseTabbedPane extends JTabbedPane{

    @Override
    public void addTab(String title, Component component) {
        int indexOf = indexOfTab(title);
        
        if(indexOf == -1){
            super.addTab(title, component); //To change body of generated methods, choose Tools | Templates.
            JPanel tabPanel             = new JPanel();
            TransparantButton tabButton = new TransparantButton("/ortala/image/new/delete.png", "/ortala/image/new/cancel.png");
            JLabel tabTitle             = new JLabel(title);
            GridBagConstraints tabGrid  = new GridBagConstraints();

            tabPanel.setOpaque(false);

            tabButton.addActionListener(this.createActionListener(title));
            
            tabTitle.setFont(new Font("Tahoma", Font.BOLD, 12));

            tabGrid.gridx   = 0;
            tabGrid.gridy   = 0;
            tabGrid.weightx = 1;
            tabPanel.add(tabTitle, tabGrid);

            tabGrid.gridx++;
            tabGrid.weightx = 0;
            tabPanel.add(tabButton, tabGrid);
            super.setTabComponentAt(super.indexOfTab(title), tabPanel);
        }else{
            setSelectedIndex(indexOf);
        }
        
    }
    
    private ActionListener createActionListener(String title){
        
        return new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                removeTabAt(indexOfTab(title));
            }
        };
        
    }

    @Override
    public void setUI(TabbedPaneUI ui) {
        super.setUI(new PlasticTabbedPaneUI()); //To change body of generated methods, choose Tools | Templates.
    }
    
    
}

