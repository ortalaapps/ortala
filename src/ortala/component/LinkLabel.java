/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ortala.component;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.event.FocusListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.font.TextAttribute;
import java.util.Map;
import javax.swing.JLabel;

/**
 *
 * @author thread009
 */
public class LinkLabel extends JLabel implements MouseListener{
    
    private Color focusColor;
    private Color gainedColor;
    private LinkAction classAction;
    
    public LinkLabel(){
        this(Color.BLUE, Color.BLACK);
    }
    
    public LinkLabel(Color focusColor, Color gainedColor){
        this.focusColor = focusColor;
        this.gainedColor = gainedColor;
        this.addMouseListener(this);
    }
    
    private void labelFocused(){
        this.setForeground(focusColor);
        Font original = this.getFont();
        Map attributes = original.getAttributes();
        attributes.put(TextAttribute.UNDERLINE, TextAttribute.UNDERLINE_ON);
        this.setFont(original.deriveFont(attributes));
    }
    
    private void labelGained(){
        this.setForeground(gainedColor);
        this.setFont(new Font(this.getFont().getFontName(), this.getFont().getStyle(), this.getFont().getSize()));
    }
    
    private void labelClicked(){
        labelFocused();
        this.setForeground(Color.RED);
        labelFocused();
    }
    
    public interface LinkAction{
        public void linkLabelAction();
    }
    
    public void addLinkLabelAction(LinkAction e){
        this.classAction = e;
    }
    

    @Override
    public void mouseClicked(MouseEvent e) {
        labelClicked();
        if(this.classAction == null)
            System.out.println("ACTION IS NOT ADDED");
        else
            this.classAction.linkLabelAction();
    }

    @Override
    public void mousePressed(MouseEvent e) {
        labelFocused();
        this.setForeground(Color.RED);
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        labelGained();
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        labelFocused();
        this.setCursor(new Cursor(Cursor.HAND_CURSOR));
    }

    @Override
    public void mouseExited(MouseEvent e) {
        labelGained();
    }
}
