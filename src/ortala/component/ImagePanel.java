/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ortala.component;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.JPanel;

public class ImagePanel extends JPanel {

    private BufferedImage image;

    public ImagePanel(boolean stat) {
        super(stat);
        try {
            this.image = ImageIO.read(getClass().getResource("/ortala/image/48px/profle-icon.png"));
        } catch (IOException ex) {
            System.out.println(ex);
        }
    }

    public ImagePanel(String url) {
        try {
            System.out.println(url);
            this.image = ImageIO.read(getClass().getResource(url));
        } catch (IOException ex) {
            System.out.println(ex);
        }
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.drawImage(image, 0, 0, this); // see javadoc for more info on the parameters            
    }

}
