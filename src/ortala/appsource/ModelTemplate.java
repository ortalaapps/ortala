/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ortala.appsource;

/**
 *
 * @author thread009
 */
public abstract class ModelTemplate {
    
    public abstract Object getValue(int index);
    
    public abstract int getSize();
    
}
