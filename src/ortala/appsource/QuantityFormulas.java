package ortala.appsource;

import java.math.BigDecimal;

public class QuantityFormulas {
	
	private Long biayaPemesanan;
        private BigDecimal jumlahPermintaan;
        private Long hargaPembelianPerUnit;
        private BigDecimal biayaSimpanPerUnit; //return by rumus and by set
        private double presentaseBiayaSimpan;
        
        
        //return by rumus
        private Long EOQValue;

    public QuantityFormulas(Long biayaPemesanan, BigDecimal jumlahPermintaan, Long hargaPembelianPerUnit, double presentaseBiayaSimpan) {
        this.biayaPemesanan = biayaPemesanan;
        this.jumlahPermintaan = jumlahPermintaan;
        this.hargaPembelianPerUnit = hargaPembelianPerUnit;
        this.presentaseBiayaSimpan = presentaseBiayaSimpan;
    }
    
    public QuantityFormulas(){
        this.biayaPemesanan = 0L;
        this.jumlahPermintaan = null;
        this.hargaPembelianPerUnit = 0L;
        this.presentaseBiayaSimpan = 0.0;
    }

    public Long getBiayaPemesanan() {
        return biayaPemesanan;
    }

    public void setBiayaPemesanan(Long biayaPemesanan) {
        this.biayaPemesanan = biayaPemesanan;
    }

    public BigDecimal getJumlahPermintaan() {
        return jumlahPermintaan;
    }

    public void setJumlahPermintaan(BigDecimal jumlahPermintaan) {
        this.jumlahPermintaan = jumlahPermintaan;
    }

    public Long getHargaPembelianPerUnit() {
        return hargaPembelianPerUnit;
    }

    public void setHargaPembelianPerUnit(Long hargaPembelianPerUnit) {
        this.hargaPembelianPerUnit = hargaPembelianPerUnit;
    }

    public double getBiayaSimpanPerUnit() {
        return hargaPembelianPerUnit * presentaseBiayaSimpan;
    }

    public void setBiayaSimpanPerUnit(BigDecimal biayaSimpanPerUnit) {
        this.biayaSimpanPerUnit = biayaSimpanPerUnit;
    }

    public double getPresentaseBiayaSimpan() {
        return presentaseBiayaSimpan;
    }

    public void setPresentaseBiayaSimpan(double presentaseBiayaSimpan) {
        this.presentaseBiayaSimpan = presentaseBiayaSimpan;
    }
    
    
    public long economicOrderQuantity(BigDecimal untiCost, int quantity, int eau, BigDecimal setupCost, BigDecimal holdingCost) {
        double eoq = 0;
        eoq = Math.sqrt((2 * (double) this.biayaPemesanan * this.jumlahPermintaan.doubleValue()) / (this.biayaSimpanPerUnit.doubleValue()));
        this.EOQValue = Math.round(eoq);
        return Math.round(eoq);
    }
    
    public double orderFrequency(){
        return this.jumlahPermintaan.doubleValue() / EOQValue;
    }
    
    public Double ropByWeek(Double leadTime){
        if(leadTime==null)
            return null;
        else if(leadTime.doubleValue() == 0)
            return ((double) (jumlahPermintaan.doubleValue()) / 4);
        else
            return ((double) (jumlahPermintaan.doubleValue() * leadTime.doubleValue()) / 4);
    }
    
    public Double ropByDay(Double leadTime){
        if(leadTime==null)
            return null;
        else if(leadTime.doubleValue() == 0)
            return ((double) (jumlahPermintaan.doubleValue()) / 7);
        else
            return ((double) (jumlahPermintaan.doubleValue() * leadTime.doubleValue()) / 7);
    }
    
    

}