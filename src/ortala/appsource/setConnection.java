package ortala.appsource;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.util.EntityUtils;

public class setConnection {
    
    public static Connection connection;
    private static String Url = "http://localhost/PROJECT/json/HI-DatabaseProjectBackup/";
    
    public Connection connectTo(String url, String user, String password){
        Connection connection = null;
        
        try{
            connection = DriverManager.getConnection(url, user, password);
        }catch(SQLException e){
            AppUtility.callErrorMessage(e, "Database Connection Error");
            System.exit(1);
            
        }
        
        return connection;
    }
    //private final boolean isRegister = false;
    
    public String ServiceGet(String url) {
        String respon = null;
        try {
            HttpClient httpClient = new DefaultHttpClient();
            HttpGet httpGet = new HttpGet(Url+url);
            httpGet.setHeader(new BasicHeader("Prama", "no-cache"));
            httpGet.setHeader(new BasicHeader("Cache-Control", "no-cache"));
            HttpResponse httpResponse = httpClient.execute(httpGet);
            HttpEntity httpEntity = httpResponse.getEntity();
            respon = EntityUtils.toString(httpEntity);
        } catch (ClientProtocolException e) {
        }
        catch(IOException d){
            
        }
        return respon;
    }
    
    public boolean ServicePost(String url, ArrayList<NameValuePair> nameValuePairs){
    	HttpResponse res = null;
        try {
            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost(Url+url);
            httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
            res = httpclient.execute(httppost); 
            HttpEntity entity = res.getEntity();
            entity.getContent();
	}catch(IOException | IllegalStateException e) {
	} 
        return res.containsHeader("OK");
    }
    
    public String ServiceLogin(String url, ArrayList<NameValuePair> nameValuePairs){
    	String respon=null;
        try {
            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost(Url+url);
            httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
            HttpResponse res = httpclient.execute(httppost); 
            HttpEntity entity = res.getEntity();
            respon = EntityUtils.toString(entity);
	}catch(IOException | IllegalStateException e) {
	}
        return respon;
    }
}
