/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ortala.appsource;

import ortala.model.PermissionModel;
import com.alee.extended.panel.GroupPanel;
import com.alee.extended.window.PopOverDirection;
import com.alee.extended.window.WebPopOver;
import com.alee.laf.button.WebButton;
import com.alee.laf.label.WebLabel;
import com.sun.corba.se.impl.protocol.SpecialMethod;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.net.URL;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableCellRenderer;

/**
 *
 * @author thread009
 */
public class AppUtility {
    
    //ERROR DIALOG
    public static void callErrorMessage(Exception e, String typeError){
        String message = "Error("+e+"): \n"+e.getMessage();
        JTextArea textError;
        JScrollPane scroll;
        JPanel panel;
        
        scroll = new javax.swing.JScrollPane();
        textError = new javax.swing.JTextArea();
        panel = new JPanel();

        textError.setColumns(20);
        textError.setRows(5);
        scroll.setViewportView(textError);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(panel);
        panel.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(scroll, javax.swing.GroupLayout.DEFAULT_SIZE, 445, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(scroll, javax.swing.GroupLayout.DEFAULT_SIZE, 155, Short.MAX_VALUE)
        );
        
        textError.setText(message);
        
        JOptionPane.showMessageDialog(null, new JScrollPane(panel), typeError, JOptionPane.ERROR_MESSAGE);
        e.printStackTrace();
    }
    
    private ImageIcon loadIcon(String path){
        return new ImageIcon(getClass().getResource(path));
    }
    
    public static void createPopOver ( final JTextField e, String text ){
        final WebPopOver popOver = new WebPopOver ( e );
        final WebLabel label = new WebLabel ( text );
        
        label.setFont(new Font("Tahoma", Font.ITALIC, 11));
        
        popOver.setMargin ( 10 );
        popOver.setFocusableWindowState ( false );
        popOver.setTransparency(0.06f);
        popOver.add ( label, BorderLayout.CENTER );
        popOver.add ( new WebButton(new AppUtility().loadIcon("/apotekx/image/new/cancel.png"), new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                popOver.dispose();
            }
        }).setUndecorated(true), BorderLayout.EAST );
        
        popOver.show(e, PopOverDirection.down);
    }
    
    //TABLE RENDERER
    /* TABLE RENDERER */
    public static String CENTER_ALIGN = "CENTER";
    public static String RIGHT_ALIGN = "RIGHT";
    public static String LEFT_ALIGN = "LEFT";
    public static String TABLE_HEADER = "HEADER";
    public static String TABLE_COLUMN = "COLUMN";
    public static void setTableRenderer(JTable table, String align, String source) {
        DefaultTableCellRenderer tableCellRenderer = null;
        int alignValue = 0;
        
        if (align.equals("CENTER")) alignValue = JLabel.CENTER;
        else if (align.equals("RIGHT")) alignValue = JLabel.RIGHT;
        else if (align.equals("LEFT")) alignValue = JLabel.LEFT; 
        
        if (source.equals("HEADER")) {
            tableCellRenderer = (DefaultTableCellRenderer) table.getTableHeader().getDefaultRenderer();
            tableCellRenderer.setHorizontalAlignment(alignValue);
            for (int i = 0; i < table.getTableHeader().getColumnModel().getColumnCount(); i++) table.getTableHeader().getColumnModel().getColumn(i).setHeaderRenderer(tableCellRenderer);
        } else if (source.equals("COLUMN")) {
            tableCellRenderer = new DefaultTableCellRenderer();
            tableCellRenderer.setHorizontalAlignment(alignValue);
            for (int i = 0; i < table.getColumnModel().getColumnCount(); i++) table.getTableHeader().getColumnModel().getColumn(i).setCellRenderer(tableCellRenderer);
        }
    }    
    
    public static void setTableRenderer(JTable table, String align, String source, int column) {
        DefaultTableCellRenderer tableCellRenderer = null;
        int alignValue = 0;
        
        if (align.equals("CENTER")) alignValue = JLabel.CENTER;
        else if (align.equals("RIGHT")) alignValue = JLabel.RIGHT;
        else if (align.equals("LEFT")) alignValue = JLabel.LEFT; 
        
        if (source.equals("HEADER")) {
            tableCellRenderer = (DefaultTableCellRenderer) table.getTableHeader().getDefaultRenderer();
            tableCellRenderer.setHorizontalAlignment(alignValue);
            table.getTableHeader().getColumnModel().getColumn(column).setHeaderRenderer(tableCellRenderer);
        } else if (source.equals("COLUMN")) {
            tableCellRenderer = new DefaultTableCellRenderer();
            tableCellRenderer.setHorizontalAlignment(alignValue);
            table.getColumnModel().getColumn(column).setCellRenderer(tableCellRenderer);
        }
    }
    
    public static void setTableRenderer(JTable table, String align, String source, int startColumn, int endColumn) {
        DefaultTableCellRenderer tableCellRenderer = null;
        int alignValue = 0;
        
        if (align.equals("CENTER")) alignValue = JLabel.CENTER;
        else if (align.equals("RIGHT")) alignValue = JLabel.RIGHT;
        else if (align.equals("LEFT")) alignValue = JLabel.LEFT; 
        
        if (source.equals("HEADER")) {
            tableCellRenderer = (DefaultTableCellRenderer) table.getTableHeader().getDefaultRenderer();
            tableCellRenderer.setHorizontalAlignment(alignValue);
            for (int i = startColumn; i <= endColumn; i++) table.getTableHeader().getColumnModel().getColumn(i).setHeaderRenderer(tableCellRenderer);
        } else if (source.equals("COLUMN")) {
            tableCellRenderer = new DefaultTableCellRenderer();
            tableCellRenderer.setHorizontalAlignment(alignValue);
            for (int i = startColumn; i <= endColumn; i++) table.getColumnModel().getColumn(i).setCellRenderer(tableCellRenderer);
        }
    }
    
    //NUMERIC SETTING
    public static String randomNumericString(int length) {
        char[] characterSet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789".toCharArray();
        Random random = new SecureRandom();
        char[] result = new char[length];
        for (int i = 0; i < result.length; i++) {
            int randomCharIndex = random.nextInt(characterSet.length);
            result[i] = characterSet[randomCharIndex];
        }
        return new String(result);
    }
    
    //PERMISSION SETTINGS
    public static PermissionModel getPermission(String permissionKey){
        PermissionModel permission = null;
        for(PermissionModel get : AppProfile.DATA_PERMISSION){
            if(permissionKey.equals(get.getPermissionId())){
                permission = get;
                break;
            }
            else continue;
        }
        
        return permission;
    }
    
    public static boolean haveSpecialChars(String s) {
        if (s == null || s.trim().isEmpty()) {
            System.out.println("Incorrect format of string");
            return false;
        }
        Pattern p = Pattern.compile("[']");
        Matcher m = p.matcher(s);
        // boolean b = m.matches();
        boolean b = m.find();
        if (b == true)
            return true;
        else
            return false;
    }
    
}
