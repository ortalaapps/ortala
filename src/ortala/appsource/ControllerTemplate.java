/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ortala.appsource;

import ortala.appsource.DataTemplate;
import javax.swing.JPanel;

/**
 *
 * @author thread009
 */
public abstract class ControllerTemplate {
    
    protected String controllerName;
    protected JPanel controllerView;
    protected DataTemplate controllerData;

    public String getControllerName() {
        return controllerName;
    }

    public void setControllerName(String controllerName) {
        this.controllerName = controllerName;
    }

    public JPanel getControllerView() {
        return controllerView;
    }

    public void setControllerView(JPanel controllerView) {
        this.controllerView = controllerView;
    }

    public DataTemplate getControllerData() {
        return controllerData;
    }

    public void setControllerData(DataTemplate controllerData) {
        this.controllerData = controllerData;
    }
    
    
    
}
