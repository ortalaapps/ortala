package ortala.appsource.table;

import java.awt.Component;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableCellRenderer;

/**
 *
 * @author raditya
 */
public class InsetsCellRenderer extends DefaultTableCellRenderer{
    
    private final EmptyBorder emptyBorder = new EmptyBorder(5, 5, 5, 5);

    public InsetsCellRenderer() {
    }

    public InsetsCellRenderer(int align) {
        setHorizontalAlignment(align);
    }

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
        setBorder(emptyBorder);
        return this;
    }
    
}
