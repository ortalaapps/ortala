package ortala.appsource.table;

import java.awt.Color;
import java.awt.Dimension;
import javax.swing.DefaultCellEditor;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

public class TableUtility {
    
    private TableColumnAdjuster tca;
    private Color headerColor = new Color(141,253,227);

    public TableUtility() {
        tca = new TableColumnAdjuster();
    }
    
    public void adjustColumn(JTable table) {
        tca.setTable(table);
        tca.adjustColumns();
    }
    
    public void adjustColumn(JTable []table) {
        for (int i = 0; i < table.length; i++) {
            tca.setTable(table[i]);
            tca.adjustColumns();
        }
    }
    
    public void removeHeaderBorder(JTable []table) {
        JLabel headerRenderer = new DefaultTableCellRenderer();
        for (int i = 0; i < table.length; i++) {
            TableColumnModel columnModel = table[i].getColumnModel();
            table[i].getTableHeader().setPreferredSize(new Dimension(new Double(table[i].getPreferredSize().getWidth()).intValue(), 30));
            table[i].getTableHeader().setBackground(headerColor);
            for (int j = 0; j < columnModel.getColumnCount(); j++) {
                headerRenderer.setBackground(headerColor);
                columnModel.getColumn(j).setHeaderRenderer((TableCellRenderer) headerRenderer);
            }   
        }
    }
    
    public void setColumnCombo(JTable table, int column, String[] comboItems) {
        JComboBox combo = new JComboBox(comboItems);
        combo.setEditable(true);
        TableColumn col = table.getColumnModel().getColumn(column);
        col.setCellEditor(new DefaultCellEditor(combo));
    }
    
    public void setColumnCombo(JTable table, int column, JComboBox comboBox) {
        comboBox.setEditable(true);
        TableColumn col = table.getColumnModel().getColumn(column);
        col.setCellEditor(new DefaultCellEditor(comboBox));
    }
    
}
