package ortala.appsource.table;

import java.awt.Color;
import java.awt.Component;
import java.awt.Point;
import java.awt.Rectangle;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JTable;
import javax.swing.plaf.TableUI;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableModel;

/**
 * An extension of {@link JTable} that is able to render spanned cells using a
 * custom UI - {@link SpanTableUI}.
 *
 * @author ytoh
 */
public class SpanTable extends JTable {

    private boolean isSpanModel;
    //private Border outside = new MatteBorder(1, 0, 1, 0, Color.RED);
    //private Border inside = new EmptyBorder(1, 0, 1, 0);
    //private Border highlight = new CompoundBorder(outside, inside);
    Map<Integer, Color> rowColor = new HashMap<Integer, Color>();
    Map<Integer, Integer> rowAlignment = new HashMap<Integer, Integer>();

    public SpanTable() {
        super();
        super.setUI(new SpanTableUI());
    }

    public SpanTable(TableModel model) {
        super(model);
        // the table UI has to be set to <code>SpanTableUI</code>
        super.setUI(new SpanTableUI());
    }

    @Override
    public Rectangle getCellRect(int row, int column, boolean includeSpacing) {
        if (isSpanModel) {
            // if the model is a span model, we have to check if the cell is spanned or not
            // and expand the area of the cell to reflect it
            Rectangle cellRect = super.getCellRect(row, column, includeSpacing);

            for (int i = 1, n = ((SpanModel) getModel()).getRowSpan(row, column); i < n; i++) {
                // expand the area of the visible cell
                cellRect.height += getRowHeight(row + i);
            }

            for (int i = 1, n = ((SpanModel) getModel()).getColumnSpan(row, column); i < n; i++) {
                // expand the area of the visible cell
                cellRect.width += getColumnModel().getColumn(column + i).getWidth();
            }

            return cellRect;
        }

        return super.getCellRect(row, column, includeSpacing);
    }
    
    @Override
    public Component prepareRenderer(TableCellRenderer renderer, int row, int column) {
        Component c = super.prepareRenderer(renderer, row, column);
        if (!isRowSelected(row)) {
            c = super.prepareRenderer(renderer, row, column);
            Color color = rowColor.get(row);
            c.setBackground(color == null ? getBackground() : color);
        }
        return c;
    }

    @Override
    public void setUI(TableUI ui) {
    }

    @Override
    public void setModel(TableModel dataModel) {
        isSpanModel = dataModel instanceof SpanModel;
        super.setModel(dataModel);
    }

    @Override
    public void changeSelection(int rowIndex, int columnIndex, boolean toggle, boolean extend) {
        // only needed for correct repaint behavior
        super.changeSelection(rowIndex, columnIndex, toggle, extend);
        repaint();
    }

    @Override
    public int columnAtPoint(Point point) {
        if (isSpanModel) {
            int row = super.rowAtPoint(point);
            int column = super.columnAtPoint(point);

            // return the column of the hiding cell
            return ((SpanModel) getModel()).getVisibleCell(row, column).getColumn();
        }

        return super.columnAtPoint(point);
    }

    @Override
    public int rowAtPoint(Point point) {
        if (isSpanModel) {
            int row = super.rowAtPoint(point);
            int column = super.columnAtPoint(point);
            // return the row of the hiding cell
            return ((SpanModel) getModel()).getVisibleCell(row, column).getRow();
        }
        return super.rowAtPoint(point);
    }

    public void setRowColor(int row, Color color) {
        rowColor.put(row, color);
    }
    
    public void setRowAlignment(int row, int align) {
        rowAlignment.put(row, align);
    }

    @Override
    public void setGridColor(Color gridColor) {
        super.setGridColor(gridColor); //To change body of generated methods, choose Tools | Templates.
    }

}
