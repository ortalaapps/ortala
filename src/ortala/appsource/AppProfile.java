/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ortala.appsource;

import ortala.controller.LoginController;
import ortala.controller.MainFrameController;
import ortala.dbtrans.ApotekerData;
import ortala.dbtrans.PermissionData;
import ortala.model.ApotekerModel;
import ortala.model.PermissionModel;
import java.awt.Font;
import java.sql.Connection;
import java.util.List;
import javax.swing.JLabel;
import ortala.appsource.table.InsetsCellRenderer;

/**
 *
 * @author LontongSayur
 */
public class AppProfile {
    
    //DB_TRANS
    public static Connection DB_CONNECTION;
    public static ApotekerData ACCESS_APOTEKER;
    public static PermissionData ACCESS_PERMISSION;
    
    //APOTEKER_DATA
    public static ApotekerModel DATA_APOTEKER;
    public static List<PermissionModel> DATA_PERMISSION;
    
    //APPLICATION VARIABLE
    public static MainFrameController MAIN_CONTROLLER;
    public static LoginController LOGIN_CONTROLLER;
    public static final Font FONT_PROPERTIES = new Font("Tahome", Font.BOLD, 12);
    
    //APPLICATION PERMISSION
    public static final String ORDERING_PRODUCT = "ORDER_PRODUCT";
    public static final String VENDORS = "VENDORS";
    public static final String PAY_ORDER = "PAY_ORDER";
    public static final String RECEIVE_STOCK = "RECEIVE_STOCK";
    
    public static final String SALE_PRODUCT = "SALE_PRODUCT";
    public static final String CUSTOMERS = "CUSTOMERS";
    public static final String RECEIVE_PAYMENT = "RECEIVE_PAYMENT";
    
    public static final String PRODCUTS = "PRODUCTS";
    public static final String CATEGORIES = "CATEGORIES";
    public static final String CURRENT_STOCK = "CURRENT_STOCK";
    
    public static final String PURCHASING_REPORT = "PURCHASING_REPORT";
    public static final String INVENTORY_REPORT = "INVENTORY_REPORT";
    public static final String SALES_REPORT = "SALES_REPORT";
    
    public static InsetsCellRenderer insetsCellRendererCenter = new InsetsCellRenderer(JLabel.CENTER);
    
    
}
