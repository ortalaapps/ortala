/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ortala.appsource;

import java.awt.Component;
import java.awt.Font;
import javax.swing.DefaultCellEditor;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JTextPane;

/*
 * A text editor program with basic edit and format functions.
 */
public class TextAreaNumberingEditor extends DefaultCellEditor {

    protected JScrollPane scrollpane;
    protected JTextPane textarea;
    protected TextLineNumber t;

    public TextAreaNumberingEditor(String editor) {
        super(new JCheckBox());
        textarea = new JTextPane();
        scrollpane = new JScrollPane();
        
        
        //textarea.setLineWrap(true);
        //textarea.setWrapStyleWord(true);
        textarea.setFont(new java.awt.Font("Tahoma", Font.PLAIN, 12));
        textarea.setBorder(javax.swing.BorderFactory.createTitledBorder(null,
                editor, javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION,
                javax.swing.border.TitledBorder.DEFAULT_POSITION,
                new java.awt.Font("Tahoma", Font.BOLD, 12)));
        t = new TextLineNumber(textarea);
        
        scrollpane.getViewport().add(textarea);
        scrollpane.setRowHeaderView(t);
        
    }

    public Component getTableCellEditorComponent(JTable table, Object value,
            boolean isSelected, int row, int column) {
        textarea.setText((String) value);

        return scrollpane;
    }

    public Object getCellEditorValue() {
        return textarea.getText();
    }
    
    public TextLineNumber getTextLineNumber(){
        return t;
    }
}
