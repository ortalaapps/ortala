/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ortala.model.tugas;

/**
 *
 * @author thread009
 */
public class Tugas {
    
    private double id;
    private String kodeJabatan;
    private String kodeTugas;
    private String uraianTugas;
    private String kataKunci;
    private String kodeKompetensi;
    private String kodeDef;
    private int level;

    public Tugas(double id, String kodeJabatan, String kodeTugas, String uraianTugas, String kataKunci, String kodeKompetensi, String kodeDef, int level) {
        this.id = id;
        this.kodeJabatan = kodeJabatan;
        this.kodeTugas = kodeTugas;
        this.uraianTugas = uraianTugas;
        this.kataKunci = kataKunci;
        this.kodeKompetensi = kodeKompetensi;
        this.kodeDef = kodeDef;
        this.level = level;
    }

    public double getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getKodeJabatan() {
        return kodeJabatan;
    }

    public void setKodeJabatan(String kodeJabatan) {
        this.kodeJabatan = kodeJabatan;
    }

    public String getKodeTugas() {
        return kodeTugas;
    }

    public void setKodeTugas(String kodeTugas) {
        this.kodeTugas = kodeTugas;
    }

    public String getUraianTugas() {
        return uraianTugas;
    }

    public void setUraianTugas(String uraianTugas) {
        this.uraianTugas = uraianTugas;
    }

    public String getKataKunci() {
        return kataKunci;
    }

    public void setKataKunci(String kataKunci) {
        this.kataKunci = kataKunci;
    }

    public String getKodeKompetensi() {
        return kodeKompetensi;
    }

    public void setKodeKompetensi(String kodeKompetensi) {
        this.kodeKompetensi = kodeKompetensi;
    }

    public String getKodeDef() {
        return kodeDef;
    }

    public void setKodeDef(String kodeDef) {
        this.kodeDef = kodeDef;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }
    
    
    
}
