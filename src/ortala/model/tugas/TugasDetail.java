/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ortala.model.tugas;

/**
 *
 * @author thread009
 */
public class TugasDetail {
    
    private double id;
    private String kodeTugas;
    private int point;
    private String detail;
    private String kataKunci;

    public TugasDetail(double id, String kodeTugas, int point, String detail, String kataKunci) {
        this.id = id;
        this.kodeTugas = kodeTugas;
        this.point = point;
        this.detail = detail;
        this.kataKunci = kataKunci;
    }

    public double getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getKodeTugas() {
        return kodeTugas;
    }

    public void setKodeTugas(String kodeTugas) {
        this.kodeTugas = kodeTugas;
    }

    public int getPoint() {
        return point;
    }

    public void setPoint(int point) {
        this.point = point;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getKataKunci() {
        return kataKunci;
    }

    public void setKataKunci(String kataKunci) {
        this.kataKunci = kataKunci;
    }
    
    
    
}
