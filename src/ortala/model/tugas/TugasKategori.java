/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ortala.model.tugas;

/**
 *
 * @author thread009
 */
public class TugasKategori {
    
    
    private double id;
    private String kodeTugas;
    private boolean mutlak;
    private boolean penting;
    private boolean perlu;

    public TugasKategori(double id, String kodeTugas, boolean mutlak, boolean penting, boolean perlu) {
        this.id = id;
        this.kodeTugas = kodeTugas;
        this.mutlak = mutlak;
        this.penting = penting;
        this.perlu = perlu;
    }

    public double getId() {
        return id;
    }

    public void setId(double id) {
        this.id = id;
    }

    public String getKodeTugas() {
        return kodeTugas;
    }

    public void setKodeTugas(String kodeTugas) {
        this.kodeTugas = kodeTugas;
    }

    public boolean isMutlak() {
        return mutlak;
    }

    public void setMutlak(boolean mutlak) {
        this.mutlak = mutlak;
    }

    public boolean isPenting() {
        return penting;
    }

    public void setPenting(boolean penting) {
        this.penting = penting;
    }

    public boolean isPerlu() {
        return perlu;
    }

    public void setPerlu(boolean perlu) {
        this.perlu = perlu;
    }
    
    
    
}
