package ortala.model.evajab;

/**
 *
 * @author DELL
 */
public class EvajabHasilKerja {
    private Integer id;
    private String kodeEvajab, kodeJabatan, deskripsi;

    public EvajabHasilKerja() {
    }

    public EvajabHasilKerja(Integer id, String kodeEvajab, String kodeJabatan, String deskripsi) {
        this.id = id;
        this.kodeEvajab = kodeEvajab;
        this.kodeJabatan = kodeJabatan;
        this.deskripsi = deskripsi;
    }

    public EvajabHasilKerja(String kodeEvajab, String kodeJabatan, String deskripsi) {
        this.kodeEvajab = kodeEvajab;
        this.kodeJabatan = kodeJabatan;
        this.deskripsi = deskripsi;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getKodeEvajab() {
        return kodeEvajab;
    }

    public void setKodeEvajab(String kodeEvajab) {
        this.kodeEvajab = kodeEvajab;
    }

    public String getKodeJabatan() {
        return kodeJabatan;
    }

    public void setKodeJabatan(String kodeJabatan) {
        this.kodeJabatan = kodeJabatan;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }
    
    
}
