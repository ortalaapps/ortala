package ortala.model.evajab;

/**
 *
 * @author DELL
 */
public class EvajabDetail {
    private Integer id;
    private String kodeEvajab, kodeFaktor;
    private Integer levelFaktor, nilaiFaktor;

    public EvajabDetail() {
    }

    public EvajabDetail(Integer id, String kodeEvajab, String kodeFaktor, Integer levelFaktor, Integer nilaiFaktor) {
        this.id = id;
        this.kodeEvajab = kodeEvajab;
        this.kodeFaktor = kodeFaktor;
        this.levelFaktor = levelFaktor;
        this.nilaiFaktor = nilaiFaktor;
    }

    public EvajabDetail(String kodeEvajab, String kodeFaktor, Integer levelFaktor, Integer nilaiFaktor) {
        this.kodeEvajab = kodeEvajab;
        this.kodeFaktor = kodeFaktor;
        this.levelFaktor = levelFaktor;
        this.nilaiFaktor = nilaiFaktor;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getKodeEvajab() {
        return kodeEvajab;
    }

    public void setKodeEvajab(String kodeEvajab) {
        this.kodeEvajab = kodeEvajab;
    }

    public String getKodeFaktor() {
        return kodeFaktor;
    }

    public void setKodeFaktor(String kodeFaktor) {
        this.kodeFaktor = kodeFaktor;
    }

    public Integer getLevelFaktor() {
        return levelFaktor;
    }

    public void setLevelFaktor(Integer levelFaktor) {
        this.levelFaktor = levelFaktor;
    }

    public Integer getNilaiFaktor() {
        return nilaiFaktor;
    }

    public void setNilaiFaktor(Integer nilaiFaktor) {
        this.nilaiFaktor = nilaiFaktor;
    }
    
}
