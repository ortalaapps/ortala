package ortala.model.evajab;

import java.util.Date;

/**
 *
 * @author DELL
 */
public class Evajab {
    private Integer id;
    private String kodeEvajab, kodeInstansi, kodeJabatan;
    private Date tanggal;

    public Evajab() {
    }

    public Evajab(Integer id, String kodeEvajab, String kodeInstansi, String kodeJabatan, Date tanggal) {
        this.id = id;
        this.kodeEvajab = kodeEvajab;
        this.kodeInstansi = kodeInstansi;
        this.kodeJabatan = kodeJabatan;
        this.tanggal = tanggal;
    }

    public Evajab(String kodeEvajab, String kodeInstansi, String kodeJabatan, Date tanggal) {
        this.kodeEvajab = kodeEvajab;
        this.kodeInstansi = kodeInstansi;
        this.kodeJabatan = kodeJabatan;
        this.tanggal = tanggal;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getKodeEvajab() {
        return kodeEvajab;
    }

    public void setKodeEvajab(String kodeEvajab) {
        this.kodeEvajab = kodeEvajab;
    }

    public String getKodeInstansi() {
        return kodeInstansi;
    }

    public void setKodeInstansi(String kodeInstansi) {
        this.kodeInstansi = kodeInstansi;
    }

    public String getKodeJabatan() {
        return kodeJabatan;
    }

    public void setKodeJabatan(String kodeJabatan) {
        this.kodeJabatan = kodeJabatan;
    }

    public Date getTanggal() {
        return tanggal;
    }

    public void setTanggal(Date tanggal) {
        this.tanggal = tanggal;
    }
    
    
}
