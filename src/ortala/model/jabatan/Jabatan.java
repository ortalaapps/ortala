/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ortala.model.jabatan;

/**
 *
 * @author thread009
 */
public class Jabatan {
    private double id;
    private String ikhtisar;
    private String kodeJabatan;
    private String namaJabatan;

    public Jabatan(double id, String ikhtisar, String kodeJabatan, String namaJabatan) {
        this.id = id;
        this.ikhtisar = ikhtisar;
        this.kodeJabatan = kodeJabatan;
        this.namaJabatan = namaJabatan;
    }

    public double getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getIkhtisar() {
        return ikhtisar;
    }

    public void setIkhtisar(String ikhtisar) {
        this.ikhtisar = ikhtisar;
    }

    public String getKodeJabatan() {
        return kodeJabatan;
    }

    public void setKodeJabatan(String kodeJabatan) {
        this.kodeJabatan = kodeJabatan;
    }

    public String getNamaJabatan() {
        return namaJabatan;
    }

    public void setNamaJabatan(String namaJabatan) {
        this.namaJabatan = namaJabatan;
    }
    
    
}
