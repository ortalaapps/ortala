/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ortala.model.jabatan;

/**
 *
 * @author thread009
 */
public class UnitKerja {
    
    private double id;
    private String kodeJabatan;
    private String eselon1, eselon2, eselon3, eselon4;

    public UnitKerja(double id, String kodeJabatan, String eselon1, String eselon2, String eselon3, String eselon4) {
        this.id = id;
        this.kodeJabatan = kodeJabatan;
        this.eselon1 = eselon1;
        this.eselon2 = eselon2;
        this.eselon3 = eselon3;
        this.eselon4 = eselon4;
    }
    
    

    public double getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getKodeJabatan() {
        return kodeJabatan;
    }

    public void setKodeJabatan(String kodeJabatan) {
        this.kodeJabatan = kodeJabatan;
    }

    public String getEselon1() {
        return eselon1;
    }

    public void setEselon1(String eselon1) {
        this.eselon1 = eselon1;
    }

    public String getEselon2() {
        return eselon2;
    }

    public void setEselon2(String eselon2) {
        this.eselon2 = eselon2;
    }

    public String getEselon3() {
        return eselon3;
    }

    public void setEselon3(String eselon3) {
        this.eselon3 = eselon3;
    }

    public String getEselon4() {
        return eselon4;
    }

    public void setEselon4(String eselon4) {
        this.eselon4 = eselon4;
    }
    
    
    
}
