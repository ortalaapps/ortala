/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ortala.model;

import ortala.appsource.ModelTemplate;

/**
 *
 * @author user
 */
public class ApotekerModel extends ModelTemplate{
    
    private int id;
    private String apotekerId;
    private String namaApoteker;
    private String noContact;
    private String eMail;
    private String alamat;
    private String username;
    private String password;
    private int userLevel;

    public ApotekerModel(int id, String apotekerId, String namaApoteker, String noContact, String eMail, String alamat, String username, String password, int userLevel) {
        this.id = id;
        this.apotekerId = apotekerId;
        this.namaApoteker = namaApoteker;
        this.noContact = noContact;
        this.eMail = eMail;
        this.alamat = alamat;
        this.username = username;
        this.password = password;
        this.userLevel = userLevel;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getApotekerId() {
        return apotekerId;
    }

    public void setApotekerId(String apotekerId) {
        this.apotekerId = apotekerId;
    }

    public String getNamaApoteker() {
        return namaApoteker;
    }

    public void setNamaApoteker(String namaApoteker) {
        this.namaApoteker = namaApoteker;
    }

    public String getNoContact() {
        return noContact;
    }

    public void setNoContact(String noContact) {
        this.noContact = noContact;
    }

    public String geteMail() {
        return eMail;
    }

    public void seteMail(String eMail) {
        this.eMail = eMail;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }    

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getUserLevel() {
        return userLevel;
    }

    public void setUserLevel(int userLevel) {
        this.userLevel = userLevel;
    }

    @Override
    public Object getValue(int index) {
        switch(index){
            case 0:
                return apotekerId;
            case 1:
                return namaApoteker;
            case 2:
                return noContact;
            case 3:
                return eMail;
            case 4:
                return alamat;
            case 5:
                return password;
            case 6:
                return userLevel;
            default:
                return null;
                
        }
    }

    @Override
    public int getSize() {
        return 7;
    }
    
    
    
    
}
