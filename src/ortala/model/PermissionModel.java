/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ortala.model;

/**
 *
 * @author thread009
 */
public class PermissionModel {
    
    private String apotekerId;
    private String permissionId;
    private boolean view;
    private boolean edit;

    public PermissionModel(String apotekerId, String permissionId, boolean view, boolean edit) {
        this.apotekerId = apotekerId;
        this.permissionId = permissionId;
        this.view = view;
        this.edit = edit;
    }
    
    public PermissionModel(){}

    public String getApotekerId() {
        return apotekerId;
    }

    public void setApotekerId(String apotekerId) {
        this.apotekerId = apotekerId;
    }

    public String getPermissionId() {
        return permissionId;
    }

    public void setPermissionId(String permissionId) {
        this.permissionId = permissionId;
    }

    public boolean isView() {
        return view;
    }

    public void setView(boolean view) {
        this.view = view;
    }

    public boolean isEdit() {
        return edit;
    }

    public void setEdit(boolean edit) {
        this.edit = edit;
    }
    
    
    
    
    
}
