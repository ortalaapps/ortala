/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ortala.model.master;

/**
 *
 * @author thread009
 */
public class MasterKompetensiDef {
    
    private int id;
    private String kodeKompetensi;
    private String kodeDef;
    private String definisiKompetensi;

    public MasterKompetensiDef(int id, String kodeKompetensi, String kodeDef, String definisiKompetensi) {
        this.id = id;
        this.kodeKompetensi = kodeKompetensi;
        this.kodeDef = kodeDef;
        this.definisiKompetensi = definisiKompetensi;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getKodeKompetensi() {
        return kodeKompetensi;
    }

    public void setKodeKompetensi(String kodeKompetensi) {
        this.kodeKompetensi = kodeKompetensi;
    }

    public String getKodeDef() {
        return kodeDef;
    }

    public void setKodeDef(String kodeDef) {
        this.kodeDef = kodeDef;
    }

    public String getDefinisiKompetensi() {
        return definisiKompetensi;
    }

    public void setDefinisiKompetensi(String definisiKompetensi) {
        this.definisiKompetensi = definisiKompetensi;
    }

    
    
}
