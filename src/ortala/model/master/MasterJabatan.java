package ortala.model.master;

/**
 *
 * @author DELL
 */
public class MasterJabatan {

    private Integer id, tipeJabatan;
    private String kodeInstansi, kodeJabatan, namaJabatan, peranJabatan;

    public MasterJabatan() {
    }

    public MasterJabatan(Integer id, String kodeInstansi, String kodeJabatan, String namaJabatan, Integer tipeJabatan, String peranJabatan) {
        this.id = id;
        this.kodeInstansi = kodeInstansi;
        this.kodeJabatan = kodeJabatan;
        this.namaJabatan = namaJabatan;
        this.tipeJabatan = tipeJabatan;
        this.peranJabatan = peranJabatan;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getKodeInstansi() {
        return kodeInstansi;
    }

    public void setKodeInstansi(String kodeInstansi) {
        this.kodeInstansi = kodeInstansi;
    }

    public String getKodeJabatan() {
        return kodeJabatan;
    }

    public void setKodeJabatan(String kodeJabatan) {
        this.kodeJabatan = kodeJabatan;
    }

    public String getNamaJabatan() {
        return namaJabatan;
    }

    public void setNamaJabatan(String namaJabatan) {
        this.namaJabatan = namaJabatan;
    }

    public Integer getTipeJabatan() {
        return tipeJabatan;
    }

    public void setTipeJabatan(Integer tipeJabatan) {
        this.tipeJabatan = tipeJabatan;
    }

    public String getPeranJabatan() {
        return peranJabatan;
    }

    public void setPeranJabatan(String peranJabatan) {
        this.peranJabatan = peranJabatan;
    }
    
    
    
}
