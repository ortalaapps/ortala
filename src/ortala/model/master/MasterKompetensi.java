/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ortala.model.master;

/**
 *
 * @author thread009
 */
public class MasterKompetensi {
    
    private int id;
    private String kodeKompetensi;
    private String namaKompetensi;
    
    public MasterKompetensi(int id, String kodeKompetensi, String namaKompetensi){
        this.id = id;
        this.kodeKompetensi = kodeKompetensi;
        this.namaKompetensi = namaKompetensi;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getKodeKompetensi() {
        return kodeKompetensi;
    }

    public void setKodeKompetensi(String kodeKompetensi) {
        this.kodeKompetensi = kodeKompetensi;
    }

    public String getNamaKompetensi() {
        return namaKompetensi;
    }

    public void setNamaKompetensi(String namaKompetensi) {
        this.namaKompetensi = namaKompetensi;
    }
    
    
    
}
