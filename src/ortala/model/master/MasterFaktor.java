package ortala.model.master;

/**
 *
 * @author DELL
 */
public class MasterFaktor {
    private Integer id;
    private String kodeFaktor, faktorJabatan;
    private Integer tipe;

    public MasterFaktor() {
    }

    public MasterFaktor(Integer id, String kodeFaktor, String faktorJabatan, Integer tipe) {
        this.id = id;
        this.kodeFaktor = kodeFaktor;
        this.faktorJabatan = faktorJabatan;
        this.tipe = tipe;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getKodeFaktor() {
        return kodeFaktor;
    }

    public void setKodeFaktor(String kodeFaktor) {
        this.kodeFaktor = kodeFaktor;
    }

    public String getFaktorJabatan() {
        return faktorJabatan;
    }

    public void setFaktorJabatan(String faktorJabatan) {
        this.faktorJabatan = faktorJabatan;
    }

    public Integer getTipe() {
        return tipe;
    }

    public void setTipe(Integer tipe) {
        this.tipe = tipe;
    }
    
    
}
