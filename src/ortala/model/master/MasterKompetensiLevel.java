/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ortala.model.master;

/**
 *
 * @author thread009
 */
public class MasterKompetensiLevel {
    
    private int id;
    private String kodeKompetensi;
    private String kodeDef;
    private int level;
    private String deskripsi;

    public MasterKompetensiLevel(int id, String kodeKompetensi, String kodeDef, int level, String deskripsi) {
        this.id = id;
        this.kodeKompetensi = kodeKompetensi;
        this.kodeDef = kodeDef;
        this.level = level;
        this.deskripsi = deskripsi;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getKodeKompetensi() {
        return kodeKompetensi;
    }

    public void setKodeKompetensi(String kodeKompetensi) {
        this.kodeKompetensi = kodeKompetensi;
    }

    public String getKodeDef() {
        return kodeDef;
    }

    public void setKodeDef(String kodeDef) {
        this.kodeDef = kodeDef;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }
    
    
    
}
