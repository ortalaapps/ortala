package ortala.model.master;

/**
 *
 * @author DELL
 */
public class MasterJabatanUraian {

    private Integer id;
    private String kodeJabatan, detailUraian;

    public MasterJabatanUraian() {
    }

    public MasterJabatanUraian(Integer id, String kodeJabatan, String detailUraian) {
        this.id = id;
        this.kodeJabatan = kodeJabatan;
        this.detailUraian = detailUraian;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getKodeJabatan() {
        return kodeJabatan;
    }

    public void setKodeJabatan(String kodeJabatan) {
        this.kodeJabatan = kodeJabatan;
    }

    public String getDetailUraian() {
        return detailUraian;
    }

    public void setDetailUraian(String detailUraian) {
        this.detailUraian = detailUraian;
    }
    
    
    
}
