package ortala.model.master;

/**
 *
 * @author DELL
 */
public class MasterFaktorDetail {
    private Integer id;
    private String kodeFaktor, kriteria;
    private Integer level, nilai;

    public MasterFaktorDetail() {
    }

    public MasterFaktorDetail(Integer id, String kodeFaktor, String kriteria, Integer level, Integer nilai) {
        this.id = id;
        this.kodeFaktor = kodeFaktor;
        this.kriteria = kriteria;
        this.level = level;
        this.nilai = nilai;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getKodeFaktor() {
        return kodeFaktor;
    }

    public void setKodeFaktor(String kodeFaktor) {
        this.kodeFaktor = kodeFaktor;
    }

    public String getKriteria() {
        return kriteria;
    }

    public void setKriteria(String kriteria) {
        this.kriteria = kriteria;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public Integer getNilai() {
        return nilai;
    }

    public void setNilai(Integer nilai) {
        this.nilai = nilai;
    }
    
    
}
