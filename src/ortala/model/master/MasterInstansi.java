/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ortala.model.master;

/**
 *
 * @author DELL
 */
public class MasterInstansi {
    
    private Integer id;
    private String kodeInstansi, namaInstansi;

    public MasterInstansi() {
    }

    public MasterInstansi(Integer id, String kodeInstansi, String namaInstansi) {
        this.id = id;
        this.kodeInstansi = kodeInstansi;
        this.namaInstansi = namaInstansi;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getKodeInstansi() {
        return kodeInstansi;
    }

    public void setKodeInstansi(String kodeInstansi) {
        this.kodeInstansi = kodeInstansi;
    }

    public String getNamaInstansi() {
        return namaInstansi;
    }

    public void setNamaInstansi(String namaInstansi) {
        this.namaInstansi = namaInstansi;
    }
            
}
