/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ortala.view.kompetensi;

import java.awt.Font;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.JTextArea;
import ortala.appsource.AppProfile;
import ortala.appsource.TextAreaCellRenderer;
import ortala.appsource.TextAreaEditor;

/**
 *
 * @author thread009
 */
public class KompetensiView extends javax.swing.JPanel {

    /**
     * Creates new form Kompetensi
     */
    public KompetensiView() {
        initComponents();
        tableProperties();
    }

    private void tableProperties(){
        tableKompetensi.getTableHeader().setFont(AppProfile.FONT_PROPERTIES);
        tableDefinisi.getTableHeader().setFont(AppProfile.FONT_PROPERTIES);
        tableDeskripsi.getTableHeader().setFont(AppProfile.FONT_PROPERTIES);
        
        tableDefinisi.setRowHeight(80);
        tableDeskripsi.setRowHeight(80);
        
        tableDefinisi.getColumn("Definisi Kompetensi").setCellRenderer(new TextAreaCellRenderer("Definisi Kompetensi"));
        tableDefinisi.getColumn("Definisi Kompetensi").setCellEditor(new TextAreaEditor("Definisi Kompetensi"));
        
        tableDeskripsi.getColumn("Deskripsi").setCellRenderer(new TextAreaCellRenderer("Deskripsi"));
        tableDeskripsi.getColumn("Deskripsi").setCellEditor(new TextAreaEditor("Deskripsi"));
        
        int[] width = {30, 156, 60};
        for (int i = 0; i < tableKompetensi.getColumnCount(); i++) {
            tableKompetensi.getColumnModel().getColumn(i).setPreferredWidth(width[i]);
        }
        
        int[] width2 = {40, 412};
        for (int i = 0; i < tableDefinisi.getColumnCount(); i++) {
            tableDefinisi.getColumnModel().getColumn(i).setPreferredWidth(width2[i]);
        }
        
        int[] width3 = {45, 365};
        for (int i = 0; i < tableDeskripsi.getColumnCount(); i++) {
            tableDeskripsi.getColumnModel().getColumn(i).setPreferredWidth(width3[i]);
        }
        
    }

    public JButton getBtnAddKompetensi() {
        return btnAddKompetensi;
    }

    public JButton getBtnRemoveKompetensi() {
        return btnRemoveKompetensi;
    }

    public JTable getTableDefinisi() {
        return tableDefinisi;
    }

    public JTable getTableDeskripsi() {
        return tableDeskripsi;
    }

    public JTable getTableKompetensi() {
        return tableKompetensi;
    }

    public JButton getBtnAddDefinisi() {
        return btnAddDefinisi;
    }

    public JButton getBtnAddDeskripsi() {
        return btnAddDeskripsi;
    }

    public JButton getBtnRemoveDefinisi() {
        return btnRemoveDefinisi;
    }

    public JButton getBtnRemoveDeskripsi() {
        return btnRemoveDeskripsi;
    }

    public JLabel getTextKompetensi() {
        return textKompetensi;
    }

    public JButton getBtnAcceptDefinisi() {
        return btnAcceptDefinisi;
    }

    public JButton getBtnDiscardDefinisi() {
        return btnDiscardDefinisi;
    }

    public JButton getBtnAcceptDeskripsi() {
        return btnAcceptDeskripsi;
    }

    public JButton getBtnDiscardDeskripsi() {
        return btnDiscardDeskripsi;
    }
    
    
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel2 = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        btnAddKompetensi = new javax.swing.JButton();
        btnRemoveKompetensi = new javax.swing.JButton();
        jScrollPane3 = new javax.swing.JScrollPane();
        tableKompetensi = new javax.swing.JTable();
        jPanel4 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tableDefinisi = new javax.swing.JTable();
        jPanel6 = new javax.swing.JPanel();
        btnAddDefinisi = new javax.swing.JButton();
        btnRemoveDefinisi = new javax.swing.JButton();
        btnDiscardDefinisi = new javax.swing.JButton();
        btnAcceptDefinisi = new javax.swing.JButton();
        jPanel5 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tableDeskripsi = new javax.swing.JTable();
        jPanel7 = new javax.swing.JPanel();
        btnAddDeskripsi = new javax.swing.JButton();
        btnRemoveDeskripsi = new javax.swing.JButton();
        btnDiscardDeskripsi = new javax.swing.JButton();
        btnAcceptDeskripsi = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        textKompetensi = new javax.swing.JLabel();

        jPanel2.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));

        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        btnAddKompetensi.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ortala/image/16px/add.png"))); // NOI18N

        btnRemoveKompetensi.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ortala/image/16px/delete.png"))); // NOI18N

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(btnAddKompetensi, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnRemoveKompetensi, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(170, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(btnAddKompetensi, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(btnRemoveKompetensi, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        tableKompetensi.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        tableKompetensi.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "No", "Defenisi Kompetensi", "Singkat"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tableKompetensi.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jScrollPane3.setViewportView(tableKompetensi);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 634, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        jPanel4.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        tableDefinisi.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        tableDefinisi.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "No", "Definisi Kompetensi"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, true
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tableDefinisi.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jScrollPane1.setViewportView(tableDefinisi);

        jPanel6.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        btnAddDefinisi.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ortala/image/16px/add.png"))); // NOI18N
        btnAddDefinisi.setEnabled(false);

        btnRemoveDefinisi.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ortala/image/16px/delete.png"))); // NOI18N
        btnRemoveDefinisi.setEnabled(false);

        btnDiscardDefinisi.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ortala/image/16px/discard.png"))); // NOI18N
        btnDiscardDefinisi.setEnabled(false);

        btnAcceptDefinisi.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ortala/image/16px/accept1.png"))); // NOI18N
        btnAcceptDefinisi.setEnabled(false);

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addComponent(btnAddDefinisi, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnRemoveDefinisi, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnAcceptDefinisi, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnDiscardDefinisi, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(btnAddDefinisi, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(btnRemoveDefinisi, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(btnDiscardDefinisi, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(btnAcceptDefinisi, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1)
            .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1))
        );

        jPanel5.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        tableDeskripsi.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        tableDeskripsi.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Level", "Deskripsi"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, true
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tableDeskripsi.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jScrollPane2.setViewportView(tableDeskripsi);

        jPanel7.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        btnAddDeskripsi.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ortala/image/16px/add.png"))); // NOI18N
        btnAddDeskripsi.setEnabled(false);

        btnRemoveDeskripsi.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ortala/image/16px/delete.png"))); // NOI18N
        btnRemoveDeskripsi.setEnabled(false);

        btnDiscardDeskripsi.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ortala/image/16px/discard.png"))); // NOI18N
        btnDiscardDeskripsi.setEnabled(false);

        btnAcceptDeskripsi.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ortala/image/16px/accept1.png"))); // NOI18N
        btnAcceptDeskripsi.setEnabled(false);

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addComponent(btnAddDeskripsi, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnRemoveDeskripsi, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnAcceptDeskripsi)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnDiscardDeskripsi))
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(btnAddDeskripsi, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(btnRemoveDeskripsi, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel7Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnDiscardDeskripsi, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(btnAcceptDeskripsi, javax.swing.GroupLayout.Alignment.TRAILING)))
        );

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 411, Short.MAX_VALUE)
            .addComponent(jPanel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2))
        );

        jPanel3.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        textKompetensi.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(textKompetensi, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(textKompetensi, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAcceptDefinisi;
    private javax.swing.JButton btnAcceptDeskripsi;
    private javax.swing.JButton btnAddDefinisi;
    private javax.swing.JButton btnAddDeskripsi;
    private javax.swing.JButton btnAddKompetensi;
    private javax.swing.JButton btnDiscardDefinisi;
    private javax.swing.JButton btnDiscardDeskripsi;
    private javax.swing.JButton btnRemoveDefinisi;
    private javax.swing.JButton btnRemoveDeskripsi;
    private javax.swing.JButton btnRemoveKompetensi;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JTable tableDefinisi;
    private javax.swing.JTable tableDeskripsi;
    private javax.swing.JTable tableKompetensi;
    private javax.swing.JLabel textKompetensi;
    // End of variables declaration//GEN-END:variables
}
