/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ortala.view.jabatan;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;
import ortala.appsource.AppUtility;

/**
 *
 * @author thread009
 */
public class TemporaryTugas {
    
    private String kodeTugas;
    private String kodeKompetensi;
    private String kodeDef;
    private Integer level;

    public String getKodeTugas() {
        return kodeTugas;
    }

    public void setKodeTugas(String kodeTugas) {
        this.kodeTugas = kodeTugas;
    }
    
    public String getKodeKompetensi() {
        return kodeKompetensi;
    }

    public void setKodeKompetensi(String kodeKompetensi) {
        this.kodeKompetensi = kodeKompetensi;
    }

    public String getKodeDef() {
        return kodeDef;
    }

    public void setKodeDef(String kodeDef) {
        this.kodeDef = kodeDef;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }
    
    
}
