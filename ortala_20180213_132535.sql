-- Valentina Studio --
-- MySQL dump --
-- ---------------------------------------------------------


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
-- ---------------------------------------------------------


-- CREATE TABLE "master_kompetensi" ------------------------
-- CREATE TABLE "master_kompetensi" ----------------------------
CREATE TABLE `master_kompetensi` ( 
	`id` BigInt( 255 ) AUTO_INCREMENT NOT NULL,
	`kode_kompetensi` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
	`nama_kompetensi` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
	CONSTRAINT `unique_id` UNIQUE( `id` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 20;
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE TABLE "master_kompetensi_def" --------------------
-- CREATE TABLE "master_kompetensi_def" ------------------------
CREATE TABLE `master_kompetensi_def` ( 
	`id` BigInt( 255 ) AUTO_INCREMENT NOT NULL,
	`kode_kompetensi` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
	`kode_def` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
	`definisi_kompetensi` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
	CONSTRAINT `unique_id` UNIQUE( `id` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 41;
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE TABLE "master_kompetensi_level" ------------------
-- CREATE TABLE "master_kompetensi_level" ----------------------
CREATE TABLE `master_kompetensi_level` ( 
	`id` BigInt( 255 ) AUTO_INCREMENT NOT NULL,
	`kode_kompetensi` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
	`kode_def` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
	`level` TinyInt( 1 ) NOT NULL,
	`deskripsi` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
	CONSTRAINT `unique_id` UNIQUE( `id` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 33;
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE TABLE "tb_jabatan" -------------------------------
-- CREATE TABLE "tb_jabatan" -----------------------------------
CREATE TABLE `tb_jabatan` ( 
	`id` BigInt( 255 ) AUTO_INCREMENT NOT NULL,
	`kode_jabatan` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
	`nama_jabatan` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
	`ikhtisar` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
	CONSTRAINT `unique_id` UNIQUE( `id` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 3;
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE TABLE "tb_unit_kerja" ----------------------------
-- CREATE TABLE "tb_unit_kerja" --------------------------------
CREATE TABLE `tb_unit_kerja` ( 
	`id` BigInt( 255 ) AUTO_INCREMENT NOT NULL,
	`kode_jabatan` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
	`eselon_1` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
	`eselon_2` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
	`eselon_3` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
	`eselon_4` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
	CONSTRAINT `unique_id` UNIQUE( `id` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 3;
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE TABLE "tb_uraian_tugas" --------------------------
-- CREATE TABLE "tb_uraian_tugas" ------------------------------
CREATE TABLE `tb_uraian_tugas` ( 
	`id` BigInt( 255 ) AUTO_INCREMENT NOT NULL,
	`kode_jabatan` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
	`kode_tugas` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
	`uraian_tugas` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
	`kata_kunci` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
	`kode_kompetensi` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
	`kode_def` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
	`level` TinyInt( 1 ) NOT NULL,
	CONSTRAINT `unique_id` UNIQUE( `id` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 3;
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE TABLE "tb_uraian_tugas_detail" -------------------
-- CREATE TABLE "tb_uraian_tugas_detail" -----------------------
CREATE TABLE `tb_uraian_tugas_detail` ( 
	`id` BigInt( 255 ) AUTO_INCREMENT NOT NULL,
	`kode_tugas` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
	`point` TinyInt( 255 ) NOT NULL,
	`detail` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
	`kata_kunci` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
	CONSTRAINT `unique_id` UNIQUE( `id` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 7;
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE TABLE "tb_uraian_tugas_kategori" -----------------
-- CREATE TABLE "tb_uraian_tugas_kategori" ---------------------
CREATE TABLE `tb_uraian_tugas_kategori` ( 
	`id` BigInt( 255 ) AUTO_INCREMENT NOT NULL,
	`kode_tugas` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
	`mutlak` TinyInt( 255 ) NOT NULL,
	`penting` TinyInt( 255 ) NOT NULL,
	`perlu` TinyInt( 255 ) NOT NULL,
	CONSTRAINT `unique_id` UNIQUE( `id` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 1;
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- Dump data of "master_kompetensi" ------------------------
INSERT INTO `master_kompetensi`(`id`,`kode_kompetensi`,`nama_kompetensi`) VALUES ( '17', 'KO-M7X', 'Kemampuan Berpikir' );
INSERT INTO `master_kompetensi`(`id`,`kode_kompetensi`,`nama_kompetensi`) VALUES ( '18', 'KO-59S', 'Mengelola Diri' );
INSERT INTO `master_kompetensi`(`id`,`kode_kompetensi`,`nama_kompetensi`) VALUES ( '19', 'KO-YDB', 'Mengelola Orang lain' );
-- ---------------------------------------------------------


-- Dump data of "master_kompetensi_def" --------------------
INSERT INTO `master_kompetensi_def`(`id`,`kode_kompetensi`,`kode_def`,`definisi_kompetensi`) VALUES ( '34', 'KO-M7X', 'DEF-0P8PJ', 'fleksibilitas Berpikir' );
INSERT INTO `master_kompetensi_def`(`id`,`kode_kompetensi`,`kode_def`,`definisi_kompetensi`) VALUES ( '35', 'KO-M7X', 'DEF-UUCR0', 'Kemampuan memunculkan ide / gagasan dan pemikiran baru dalam rangka meningkatkan efektivitas kerja' );
INSERT INTO `master_kompetensi_def`(`id`,`kode_kompetensi`,`kode_def`,`definisi_kompetensi`) VALUES ( '36', 'KO-M7X', 'DEF-A7B2H', 'Kemampuan menguraikan permasalahan berdasarkan informasi yang relevan dari berbagai sumber secara komprehensif untuk mengidentifikasi penyebab dan dampak terhadap organisasi' );
INSERT INTO `master_kompetensi_def`(`id`,`kode_kompetensi`,`kode_def`,`definisi_kompetensi`) VALUES ( '37', 'KO-59S', 'DEF-7LQP4', 'Kemampuan menyesuaikan diri terhadap perubahan sehingga tetap dapat mempertahankan efektivitas kerja' );
INSERT INTO `master_kompetensi_def`(`id`,`kode_kompetensi`,`kode_def`,`definisi_kompetensi`) VALUES ( '38', 'KO-59S', 'DEF-SM189', 'kemampuan bertindak secara komprehensif dan transparan dalam segala situasi dan kondisi sesuai dengan nilai-nilai, norma atau etika yang berlaku' );
INSERT INTO `master_kompetensi_def`(`id`,`kode_kompetensi`,`kode_def`,`definisi_kompetensi`) VALUES ( '39', 'KO-YDB', 'DEF-6204R', 'Kemampuan menyelesaikan pekerjaan secara bersama-sama dengan menjadi bagian dari suatu kelompok untuk mencapai tujuan organisasi' );
INSERT INTO `master_kompetensi_def`(`id`,`kode_kompetensi`,`kode_def`,`definisi_kompetensi`) VALUES ( '40', 'KO-YDB', 'DEF-NKBWF', 'Kemampuan melakukan upaya untuk  mendorong pengembangan potensi orang lain agar dapat bekerja lebih baik' );
-- ---------------------------------------------------------


-- Dump data of "master_kompetensi_level" ------------------
INSERT INTO `master_kompetensi_level`(`id`,`kode_kompetensi`,`kode_def`,`level`,`deskripsi`) VALUES ( '16', 'KO-M7X', 'DEF-0P8PJ', '1', 'Mengetahui adanya perbedaan sudut pandang' );
INSERT INTO `master_kompetensi_level`(`id`,`kode_kompetensi`,`kode_def`,`level`,`deskripsi`) VALUES ( '17', 'KO-M7X', 'DEF-0P8PJ', '2', 'Mengenali sudut pandang orang lain yang berbeda' );
INSERT INTO `master_kompetensi_level`(`id`,`kode_kompetensi`,`kode_def`,`level`,`deskripsi`) VALUES ( '18', 'KO-M7X', 'DEF-0P8PJ', '3', 'Menyelaraskan sudut pandang pribadinya dengan sudut pandang orang lain' );
INSERT INTO `master_kompetensi_level`(`id`,`kode_kompetensi`,`kode_def`,`level`,`deskripsi`) VALUES ( '19', 'KO-M7X', 'DEF-0P8PJ', '4', 'mengakui kebenaran sudut pandang orang lain' );
INSERT INTO `master_kompetensi_level`(`id`,`kode_kompetensi`,`kode_def`,`level`,`deskripsi`) VALUES ( '20', 'KO-M7X', 'DEF-UUCR0', '1', 'menggunakan gagasan/pemikiran yang sudah ada' );
INSERT INTO `master_kompetensi_level`(`id`,`kode_kompetensi`,`kode_def`,`level`,`deskripsi`) VALUES ( '21', 'KO-M7X', 'DEF-UUCR0', '2', 'mengenali adanya gagasan baru' );
INSERT INTO `master_kompetensi_level`(`id`,`kode_kompetensi`,`kode_def`,`level`,`deskripsi`) VALUES ( '22', 'KO-M7X', 'DEF-UUCR0', '3', 'Mengidentifikasi alternatif ide/gagasan baru yang dapat diterapkan' );
INSERT INTO `master_kompetensi_level`(`id`,`kode_kompetensi`,`kode_def`,`level`,`deskripsi`) VALUES ( '23', 'KO-M7X', 'DEF-UUCR0', '4', 'menentukan alternatif yang mungkin dapat dikendalikan' );
INSERT INTO `master_kompetensi_level`(`id`,`kode_kompetensi`,`kode_def`,`level`,`deskripsi`) VALUES ( '24', 'KO-M7X', 'DEF-UUCR0', '5', 'mengadopsi ide/pemikiran yang cocok diterapkan dalam lingkungan kerja' );
INSERT INTO `master_kompetensi_level`(`id`,`kode_kompetensi`,`kode_def`,`level`,`deskripsi`) VALUES ( '25', 'KO-M7X', 'DEF-A7B2H', '1', 'mengetahui permasalahan dalam pekerjaan' );
INSERT INTO `master_kompetensi_level`(`id`,`kode_kompetensi`,`kode_def`,`level`,`deskripsi`) VALUES ( '26', 'KO-M7X', 'DEF-A7B2H', '2', 'menguraikan faktor-faktor penyebab dan dampak dari permasalahan terkait' );
INSERT INTO `master_kompetensi_level`(`id`,`kode_kompetensi`,`kode_def`,`level`,`deskripsi`) VALUES ( '27', 'KO-59S', 'DEF-7LQP4', '1', 'menganggap perubahan bukan suatu hal yang penting' );
INSERT INTO `master_kompetensi_level`(`id`,`kode_kompetensi`,`kode_def`,`level`,`deskripsi`) VALUES ( '28', 'KO-59S', 'DEF-7LQP4', '2', 'mengikuti perubahan sesuai dengan tuntutan kebijakan organisasi' );
INSERT INTO `master_kompetensi_level`(`id`,`kode_kompetensi`,`kode_def`,`level`,`deskripsi`) VALUES ( '29', 'KO-59S', 'DEF-7LQP4', '3', 'Menyesuaikan diri terhadap perubahan yang  terjadi atas kesadaran dan inisiatif sendiri' );
INSERT INTO `master_kompetensi_level`(`id`,`kode_kompetensi`,`kode_def`,`level`,`deskripsi`) VALUES ( '30', 'KO-59S', 'DEF-SM189', '1', 'Menyadari tentang pentingnya norma dan etika bagi organisasi' );
INSERT INTO `master_kompetensi_level`(`id`,`kode_kompetensi`,`kode_def`,`level`,`deskripsi`) VALUES ( '31', 'KO-59S', 'DEF-SM189', '2', 'Menerapkan norma dan etika organisasi sebatas memenuhi kewajiban' );
INSERT INTO `master_kompetensi_level`(`id`,`kode_kompetensi`,`kode_def`,`level`,`deskripsi`) VALUES ( '32', 'KO-59S', 'DEF-SM189', '3', 'mengingatkan orang lain untuk bertindak sesuai dengan nilai, norma dan etika organisasi dalam segala situasi dan kondisi' );
-- ---------------------------------------------------------


-- Dump data of "tb_jabatan" -------------------------------
INSERT INTO `tb_jabatan`(`id`,`kode_jabatan`,`nama_jabatan`,`ikhtisar`) VALUES ( '2', 'JB-UD6LN', 'Kepala Subbagian Tata Usaha', 'Memimpin, melaksakan pembuatan rancangan penelitian, rancangan petunjuk teknis dan rancangan pedoman struktur kerja SKPD sesuai dengan peraturan dan ketentuan yang berlaku agar kegiatan di Sub Bagian Tata Usaha berjalan dengan lancar' );
-- ---------------------------------------------------------


-- Dump data of "tb_unit_kerja" ----------------------------
INSERT INTO `tb_unit_kerja`(`id`,`kode_jabatan`,`eselon_1`,`eselon_2`,`eselon_3`,`eselon_4`) VALUES ( '2', 'JB-UD6LN', 'X', 'X', 'X', 'X' );
-- ---------------------------------------------------------


-- Dump data of "tb_uraian_tugas" --------------------------
INSERT INTO `tb_uraian_tugas`(`id`,`kode_jabatan`,`kode_tugas`,`uraian_tugas`,`kata_kunci`,`kode_kompetensi`,`kode_def`,`level`) VALUES ( '1', 'JB-UD6LN', 'TGS-LU8P9', 'Merencanakan kegiatan subbagian Tata Usaha sesuai dengan program kerja direktorat pengendalian Kebakaran Hutan dan ketentuan yang berlaku untuk pedoman pelaksanaan tugas', 'Kata Kunci--untuk nanti', 'KO-M7X', 'DEF-UUCR0', '3' );
INSERT INTO `tb_uraian_tugas`(`id`,`kode_jabatan`,`kode_tugas`,`uraian_tugas`,`kata_kunci`,`kode_kompetensi`,`kode_def`,`level`) VALUES ( '2', 'JB-UD6LN', 'TGS-4IO6K', 'Membagi tugas kepada bawahan di lingkungan subbagian tata usaha sesuai dengan beban kerja dan tanggung jawabnya masing-masing untuk kelancaran pelaksanaan tugas', 'Kata Kunci--untuk nanti', 'KO-59S', 'DEF-7LQP4', '2' );
-- ---------------------------------------------------------


-- Dump data of "tb_uraian_tugas_detail" -------------------
INSERT INTO `tb_uraian_tugas_detail`(`id`,`kode_tugas`,`point`,`detail`,`kata_kunci`) VALUES ( '1', 'TGS-LU8P9', '1', 'Menelaah Program direktorat pengendalian Kebakaran Hutan', 'Kata Kunci--untuk nanti' );
INSERT INTO `tb_uraian_tugas_detail`(`id`,`kode_tugas`,`point`,`detail`,`kata_kunci`) VALUES ( '2', 'TGS-LU8P9', '2', 'Menyusun konsep rencana kegiatan dengan pimpinan untuk mendapatkan pengarahan', 'Kata Kunci--untuk nanti' );
INSERT INTO `tb_uraian_tugas_detail`(`id`,`kode_tugas`,`point`,`detail`,`kata_kunci`) VALUES ( '3', 'TGS-LU8P9', '3', 'menetapkan rencana kegiatan subbagian tata usaha', 'Kata Kunci--untuk nanti' );
INSERT INTO `tb_uraian_tugas_detail`(`id`,`kode_tugas`,`point`,`detail`,`kata_kunci`) VALUES ( '4', 'TGS-4IO6K', '1', 'menjabarkan rencana tugas menjadi tugas-tugas kecil yang harus dilaksakan bawahan', 'Kata Kunci--untuk nanti' );
INSERT INTO `tb_uraian_tugas_detail`(`id`,`kode_tugas`,`point`,`detail`,`kata_kunci`) VALUES ( '5', 'TGS-4IO6K', '2', 'memaparkan rencana kegiatan subbagian tata usaha', 'Kata Kunci--untuk nanti' );
INSERT INTO `tb_uraian_tugas_detail`(`id`,`kode_tugas`,`point`,`detail`,`kata_kunci`) VALUES ( '6', 'TGS-4IO6K', '3', 'memberikan petunjuk pelaksaan', 'Kata Kunci--untuk nanti' );
-- ---------------------------------------------------------


-- Dump data of "tb_uraian_tugas_kategori" -----------------
-- ---------------------------------------------------------


/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
-- ---------------------------------------------------------


