-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.30-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             9.4.0.5186
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for ortala
CREATE DATABASE IF NOT EXISTS `ortala` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `ortala`;

-- Dumping structure for table ortala.master_faktor
CREATE TABLE IF NOT EXISTS `master_faktor` (
  `id` bigint(255) NOT NULL AUTO_INCREMENT,
  `kode_faktor` varchar(255) DEFAULT NULL,
  `faktor_jabatan` varchar(255) DEFAULT NULL,
  `tipe_jabatan` tinyint(4) DEFAULT NULL COMMENT '0 : Struktural, 1 : Non Struktural',
  PRIMARY KEY (`id`),
  UNIQUE KEY `kode_faktor` (`kode_faktor`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

-- Dumping data for table ortala.master_faktor: ~16 rows (approximately)
/*!40000 ALTER TABLE `master_faktor` DISABLE KEYS */;
INSERT INTO `master_faktor` (`id`, `kode_faktor`, `faktor_jabatan`, `tipe_jabatan`) VALUES
	(1, 'FS001', 'Ruang Lingkup Pekerjaan dan Dampak', 0),
	(2, 'FS002', 'Pengaturan Organisasi', 0),
	(3, 'FS003', 'Wewenang Kepenyeliaan dan Manajerial', 0),
	(4, 'FS004A', 'Hubungan Personal', 0),
	(5, 'FS004B', 'Hubungan Personal', 0),
	(6, 'FS005', 'Kesulitan Dalam Pengarahan Pekerjaan', 0),
	(7, 'FS006', 'Kondisi Lain', 0),
	(8, 'FF001', 'Pengetahuan yang Dibutuhkan Jabatan', 1),
	(9, 'FF002', 'Pengawasan Penyelia', 1),
	(10, 'FF003', 'Pedoman', 1),
	(11, 'FF004', 'Kompleksitas', 1),
	(12, 'FF005', 'Ruang Lingkup dan Dampak', 1),
	(13, 'FF006', 'Hubungan Personal', 1),
	(14, 'FF007', 'Tujuan Hubungan', 1),
	(15, 'FF008', 'Persyaratan Fisik', 1),
	(16, 'FF009', 'Lingkungan Pekerjaan', 1);
/*!40000 ALTER TABLE `master_faktor` ENABLE KEYS */;

-- Dumping structure for table ortala.master_faktor_detail
CREATE TABLE IF NOT EXISTS `master_faktor_detail` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `kode_faktor` varchar(255) NOT NULL,
  `kriteria` text NOT NULL,
  `level` bigint(20) NOT NULL,
  `nilai` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_master_faktor_detail_master_faktor` (`kode_faktor`),
  CONSTRAINT `FK_master_faktor_detail_master_faktor` FOREIGN KEY (`kode_faktor`) REFERENCES `master_faktor` (`kode_faktor`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=79 DEFAULT CHARSET=latin1;

-- Dumping data for table ortala.master_faktor_detail: ~78 rows (approximately)
/*!40000 ALTER TABLE `master_faktor_detail` DISABLE KEYS */;
INSERT INTO `master_faktor_detail` (`id`, `kode_faktor`, `kriteria`, `level`, `nilai`) VALUES
	(1, 'FS001', 'a. RUANG LINGKUP\r\nPekerjaan yang diarahkan bersifat prosedural, rutin, dan secara tipikal memberikan jasa atau produk kepada orang tertentu atau kepada unit organisasiterkecildidalam organisasi.\r\nllustrasi:\r\nMengarahkan pekerjaan kurir, pekerjaan satpam, pekerjaan klerek, atau pekerjaan penunjang laboratorium yang berada di bawah kelas 7, atau yang setara.\r\nb. DAMPAK\r\nPekerjaan yang diarahkan memudahkan pekerjaan orang lain dalam unit organisasi langsung, memberikan respon kepada permintaan atau kebutuhan spesifik dari pegawai,atau hanya mempengaruhi fungsitertentu yang terlokalisasi.\r\nllustrasi:\r\nMemberikan jasa kepada unit organisasi, kantor lapangan yang berukuran kecil, atau kegiatan setara.\r\n', 1, 175),
	(2, 'FS001', 'a. RUANG LINGKUP\r\nSegmen program atau pekerjaan yang diarahkan bersifat pekerjaan administratif, pekerjaan teknis, pekerjaan klerek yang rumit, atau yang setara.\r\nllustrasi:\r\nMengarahkan jasa anggaran, manajemen, staf, suplai, pemeliharaan, perlindungan, perpustakaan, daftar gaji, atau jasa serupa,yang menunjang suatu rumah sakit atau kantor lapangan instansi yang berukuran sedang dan memiliki kerumitan terbatas.\r\nATAU\r\nMengarahkan kegiatan segmen program yang setara dengan kegiatan di atas tetapi berada pada tingkat organisasi yang lebih tinggi dalam instansi terkait, misalnya, bagian dari sebuah biro.\r\nb. DAMPAK\r\nJasa atau produknya:\r\n1. menunjang dan cukup mempengaruhi kantor lapangan, kantor daerah, atau operasi dan sasaran kantor lapangan, atau segmen program yang setara; atau\r\nllustrasi :\r\nKantor lapangan yang memberil<.an jasa kepada masyarakat atau memberikan sebagian jasa sesual kasus yang dibutuhkan kepada masyarakat dalam populasi kecil.\r\n2. memberikan jasa kepada populasi/pemakai dengan lingkup sedang, lokal atau terbatas pada sebuah kota kecilatau pedesaan.\r\nllustrasi:\r\nUkuran populasi yang dilayani kantor lapangan setara dengan semua warga dalam sebuah kota kecil. Tergantung pada sifat layanan yang diberikan, populasi yang dilayani boleh terkonsentrasi pada satu kota saJa atau tersebar pada suatu wilayah geografis yang lebih luas.\r\n3. Fungsi, kegiatan, atau jasa yang diberikan mencakup geografis terbatas dan menunjang sebagian besar kegiatan kantor lapangan, kantor daerah, atau kegiatan setara dalam segmen program instansi.\r\nllustrasi:\r\nJasa yang diberikan secara langsung berdampak terhadap fungsi dan kegiatan lain di seluruh jajaran organisasi yang didukung dan/atau populasi/pemakai dalam jumlah kecil.', 2, 350),
	(3, 'FS001', 'a. RUANG LINGKUP\r\nSegmen program atau pekerjaan yang diarahkan bersifat  pekerjaan teknis, pekerjaan administratif, atau pekerjaan profesional. Segmen program atau pekerjaan yang diarahkan mencakup suatu daerah ibukota, suatu daerah, atau suatu daerah kecil di beberapa daerah.\r\nllustrasi:\r\nMengarahkan perencanaan, pengawasan, dan jasa lainnya untuk pembangunan fasilitas yang komplek untuk satu atau beberapa instansi di beberapa lokasi. Fasilitas tersebut sangat penting untuk operasi lapanga n yang dilakukan oleh satu atau beberapa instansi untuk beberapa daerah provinsi.\r\nMengarahkan pelayanan administrasi (pegawai, manajemen suplai, anggaran, manajemen fasilitas, atau yang sejenis) yang menunjarig dan mempengaruhi operasi suatu organisasi biro atau organisasi markas komando militer penting, suatu organisasi dengan ukuran yang serupa atau sekelompok organisasi yang secara keseluruhan adalah setara.\r\nb. DAMPAK\r\nKegiatan, fungsi, atau jasa yang diberikan secara langsung mempengaruhi pekerjaan instansi, pekerjaan instansi lain, operasi sektor industri, atau masyarakat luas.\r\nllustrasi:\r\nDalam memberikan jasa secara iangsung kepada masyarakat luas, sebagian  besar program lini instansi tersebut diberikan untuk masyarakat dengan populasi ukuran sedang. Ukuran populasi sedang adalah setara dengan sekelompok warga dan/atau perusahaan di beberapa pedesaan, kota kecil, atau bagian dari kota metropolitan.\r\nPada tingkat lapangan (mencakup organisasi multimisi yang besar, komplek, dan rumit, dan/atau populasi yang sangat besar yang setara dengan beberapa contoh di bawah ini) pekerjaan tersebut secara langsung mempengaruhi atau menunjang fungsi yang sangat penting dari pekerjaan teknis, pekerjaan profesional, dan pekerjaan administratif yang sangat banyak, beragam, dan rumit.\r\nllustrasi:\r\nTergantung pada total populasi yang dilayani dan tingkat kompleksitas dan intensitas pelayanan itu sendiri, populasi yang dilayani  dapat terkonsentrasi pada satu wilayah geografis tertentu, atau meliputi sebagian besar dari populasi berbagai daerah, atau kelompok populasi yang setara.', 3, 550),
	(4, 'FS001', 'a. RUANG LINGKUP\r\nMengarahkan suatu segmen program pekerjaan profesional, pekerjaan sangat teknis, atau pekerjaan administratif yang rumit, yang meliputi:\r\n1. pengembangan aspek penting dalam pengembangan ilmiah, medis, hukum, administratif, peraturan, dan kebijak an, atau program yang sangat teknis yang setara; atau\r\n2. operasi utama beberapa instalasi industri besar yang sangat rumit milik pemerintah.\r\nb. DAMPAK\r\n1. Menimbulkan dampak terhadap kantor pusat instansi, beberapa program di seluruh tingkat biro, atau sebagian besar kantor lapangan instansi; atau\r\n2. memfasilitasi pencapaian misi atau program nasional suatu instansi; atau\r\n3. menimbulkan dampak terhadap sebagian besar populasi bangsa atau satu atau beberapa segmen industri besar; atau\r\n4. mendapat perhatian yang berkesinambungan dari DPR atau dari media massa.\r\nllustrasi:\r\n1. Mengarahkan program atau segmen program yang sangat penting dari:\r\na) pusat penelitian dan pengembangan yang besar dan komplek ruang angkasa, dan bawah laut;\r\nb) departemen produksi perkapalan atau pusat logistik pesawat terbang;\r\nc) pusat medis yang melakukan penelitian dan pengembangan atau beberapa program medis lain  untuk kepentingan nasional.\r\nSegmen program yang diarahkan mempengaruhi beberapa segmen sektor industri besar, atau mendapat perhatian yang berkesinambungan dari DPR atau dari media massa, atau sangat penting untuk program pertahanan, ruang angkasa, atau kesehatan masyarakat.\r\n2. Mengarahkan suatu segmen program yang mempunyai aspek utama dalam pengaturan, pelayanan masyarakat,  atau penerimaan pajak untuk sebagian besar neaara  atau  seiumlah  daerah.  Seamen program yang diarahkan secara langsung mempengaruhi sebagian  besar penduduk atau sektor usaha.\r\n3. Mengarahkan pelayanan administrasi (analisis anggaran, manajemen, atau pegawai) untuk seluruh kator pusat instansi atau sebagian besar kantor lapangan. Segmen program yang diarahkan berguna untuk membentuk atau menyempurnakan struktur, efektifitas, efisiensi, atau produktifitas beberapa bagian penting dari misi utama instansi , program berbagai daerah, seluruh jajaran kantor pusat, atau proyek untuk kepentinqan nasional.\r\n', 4, 775),
	(5, 'FS001', 'RUANG LINGKUP DAN DAMPAK DIGABUNGKAN\r\nMengarahkan suatu program yang ruang lingkup maupun dampak program atau	organisasi yang diarahkan merupakan satu atau beberapa dari yang berikut ini (secara nasional, di seluruh jajaran instansi, di seluruh jajaran industri; atau di seluruh jajaran pemerintah):	.\r\n1. yang merupakan	kepentingan nasional atau kepentingan nasional instansi tersebut;\r\n2. mendapat perhatian yang berkesinambungan dari DPR atau dari media massa; atau\r\n3. yang memiliki dampak yang	luas terhadap masyarakat.\r\nATAU\r\nMengarahkan beberapa segmen program yang kritis, proyek ilmiah yang penting, atau organisasi yang memiliki ruang lingkup dan dampak yang setara.\r\nllustrasi:\r\n1. Mengarahkan kebijakan instansi yang mempengaruhi masyarakat luas, atau satu atau beberapa sektor industri besar. Jabatan ini memimpin organisasi penting yang mempunyai tugas mengembangkan, mengeluarkan, dan melaksanakan kebijakan, peraturan, dan pedoman lain, yang digunakan seluruh jajaran instansi, atau mempengaruhi kegiatan penting sektor industri besar, atau mempengaruhi masyarakat umum.\r\n2. Mengarahkan pengembangan beberapa sub-sistem yang paling kritis dan  rumit dalam program pengembangan sistem persenjataan  atau  ruang  angkasa  yang atau di bawahnya) memiliki dampak langsung yang cukup besar terhadap satu atau beberapa sektor industri penting, terhadap misi nasional instansi, atau terhadap pertahanan nasional.', 5, 900),
	(6, 'FS002', 'Jabatan ini bertanggung jawab kepada suatu jabatan yang berada pada dua atau beberapa tingkat di bawah jabatan struktural tertinggi, misalnya jabatan yang paling rendah dalam mata rantai komando, atau jabatan yang tingkatnya setara atau lebih tinggi dalam mata rantai pengawasan langsung.', 1, 100),
	(7, 'FS002', 'Jabatan ini bertanggung jawab kepada suatu jabatan yang berada satu tingkat di bawah jabatan struktural tertinggi atau jabatan yang setara atau lebih tinggi dalam mata rantai pengawasan langsung.', 2, 250),
	(8, 'FS002', 'Jabatan ini adalah jabatan struktural tertinggi.', 3, 350),
	(9, 'FS003', 'Jabatan pada tingkat ini memenuhi: \r\nKetentuan a:\r\n1. Merencanakan dan menjadwalkan pekerjaan yang berorientasi pada output setiap 3 bulan dan setiap tahun, atau mengarahkan tugas yang jangka waktunya sama.\r\n2. Menyesuaikan kelas pegawai atau prosedur kerja dalam unit organisasi untuk membuat alokasi sumber daya pada jenjang yanglebih tinggi.\r\n3. Mempertimbangkan pembelian peralatan baru.\r\n4. Menyempurnakan metode dan prosedur kerja yang digunakan.\r\n5. Mengawasi pengembangan data, estimasi, statistik, saran, dan informasi lain yang berguna untuk pejabat yang lebih tinggi dalam menentukan tujuan dan sasaran yang diutamakan.\r\n6. Memutuskan metodologi yang digunakan untuk mencapai tujuan dan sasaran, dan untuk menentukan strategi manajemen lainnya.\r\nATAU\r\nKetentuan b:\r\nJika pekerjaannya dikontrakkan, memberikan berbagai  input  teknis  dan  melakukan  pengawasan yang setara dengan semua atau hampir semua yang berikut ini:\r\n1. Menganalisis manfaat dan biaya pelaksanaan pekerjaan jika dilakukan dalam organisasi atau dikontrakkan kepada pihak lain.\r\n2. Memberikan rekomendasi apakah pekerjaan tersebut harus dikontrakkan kepada pihak lain.\r\n3. Memberikan persyaratan dan uraian teknis pekerjaan yang akan dilaksanakan.\r\n4. Merencanakan dan menyusun jadwal , batas waktu, dan standar pekerjaan yang dapat diterima.\r\n5. Mengkoordinasikan dan mengintegrasikan jadwal pekerjaan kontraktor dengan pekerjaan bawahan atau pihak lain.\r\n6. Melacak kemajuan dan kualitas kerja.\r\n7. Mengatur bawahan melakukan inspeksi yang dipersyaratkan.\r\n8. Memutuskan dapat diterima, ditolak, atau dikoreksi produk atau jasa kerja yang mempengaruhi pembayaran kepada kontraktor.\r\nATAU\r\nKetentuan c:\r\nMelaksanakan sekurang - kurangnya tiga dari empat yang pertama, dan enam atau lebih dari sepuluh wewenang dan tanggung jawab berikut ini:\r\n1. Merencanakan pekerjaan yang dilaksanakan oleh bawahan, menetapkan dan menyesuaikan prioritas jangka pendek, dan menyusun jadwal penyelesaian pekerjaan.\r\n2. Memberikan pekerjaan kepada bawahan berdasarkan prioritas, tingkat kesulitan dan persyaratan tugas, dan kemampuan pegawai.\r\n3. Mengevaluasi kinerja bawahan.\r\n4. Memberikan saran atau petunjuk kepada pegawai masalah pekerjaan dan administrasi.\r\n5. Mewawancarai calon pegawai dalam unit kerja; memberikan rekomendasi pengangkatan, promosi atau alih tugas ke jabatan lain.\r\n6. Mendengarkan dan menyelesaikan pengaduan dari pegawai.\r\n7. Melakukan tindakan disiplin ringan, seperti peringatan dan teguran, memberikan rekomendasi tentang tindakan lain dalam kasus yang lebih berat.\r\n8. Mengidentifikasi kebutuhan pengembangan dan pelatihan pegawai dengan cara memberikan atau mengatur.\r\n9. Menemukan cara pengembangan output atau meningkatkan kualitas pekerjaan yang diarahkan.\r\n10. Mengembangkan standar kinerja.\r\n', 1, 450),
	(10, 'FS003', 'Untuk dapat mencapai tingkat ini, maka jabatan harus memenuhi:\r\nKetentuan a:\r\n1. Menjalankan wewenang manajerial untuk menetapkan rencana dan jadwal kerja tahunan dan multi tahun di lingkungan sendiri atau pekerjaan yang dikontrakkan.\r\n2. Memastikan pelaksanaan (oleh unit organisasi yang lebih rendah atau yang lain) tujuan dan sasaran segmen program atau fungsi yang diawasi.\r\n3. Menentukan tujuan dan sasaran yang perlu ditekankan; menentukan pendekatan atau solusi terbaik untuk menyelesaikan masalah anggaran; dan merencanakan kebutuhan staf jangka panjang, termasuk apakah akan mengontrakkan pekerjaan kepada pihak lain. Jabatan ini berhubungan erat dengan pejabat struktural tertinggi (atau pejabat kepegawaian instansi) untuk mengembangkan tujuan dan sasaran pengembangan staf, program, atau segmen program. Misalnya, jabatan mengarahkan pengembangan data; pengembangan keahlian dan wawasan, atau memperoleh pendapat umum, penyusunan makalah atau proposal legislasi, dan pelaksanaan kegiatan yang setara yang menunjang pengembangan tujuan dan sasaran yang berhubungan dengan manajemen program dan pengembangan atau perumusannya pada tingkat yang lebih tinggi.\r\nATAU\r\nKetentuan b:\r\nMelaksanakan semua atau hampir semua wewenang dan tanggung jawa b penyeliaan pada tingkat faktor 3- 1c:\r\n1. Merencanakan pekerjaan yang dilaksanakan oleh bawahan, menetapkan dan menyesuaikan prioritas jangka pendek, dan menyusun jadwal penyelesaian pekerjaan.\r\n2. Memberikan pekerjaan kepada bawahan berdasarkan prioritas, tingkat kesulitan dan persyaratan tugas, dan kemampuan pegawai;\r\n3. Mengevaluasi kinerja bawahan.\r\n4. Memberikan saran atau petunjuk kepada pegawaimasalah pekerjaan dan administrasi.\r\n5. Mewawancarai calon pegawai dalam unit kerja; memberikan rekomendasi pengangkatan, promosi atau alih tugas ke jabatan lain.\r\n6. Mendengarkan dan menyelesaikan pengaduan dari pegawai.\r\n7. Melakukan tindakan disiplin ringan, seperti peringatan dan teguran, memberikan rekomendasi tentang tindakan lain dalam kasus yang lebih berat.\r\n8. Mngidentifikasi kebutuhan pengembangan dan pelatihan pegawaldengan cara memberikan atau mengatur pengembangan dan pelatihan yang diperlukan.\r\n9. Menemukan cara pengembangan output atau meningkatkan kualitas pekerjaan yang diarahkan.\r\n10. Mengembangkan standar kinerja.\r\nDAN\r\nSekurang-kurangnya delapan dari yang berikut ini:\r\n1. Mengarahkan, mengkoordinasikan atau mengawasi pekerjaan dengan menggunakan salah satu dari pejabat penyelia, pemimpin tim kerja, koordinator kelompok, ketua komite, atau pegawai yang setara dan/atau melakukan pengawasan serupa terhadap kontraktor.\r\n2. Menjalankan tanggung jawab yang cukup besar dafam menangani pejabat dalam unit organisasi atau organisasi lain, atau memberikan nasihat kepada pejabat yang kelasnya lebih tinggi.\r\n3. Memastikan keadilan (di antara unit, kelompok, tim, proyek, dan lain-lain) tentang standar kinerja dan teknik penentuan nilai yang dikembangkan oleh bawahan. atau memastikan keadilan tentang penilaian bawahan pada kemampuan kontraktor atau pekerjaan yang diselesaikan oleh kontraktor.\r\n4. Mengarahkan program atau segmen program yang menggunakan sumber daya yang besar (misalnya program multi miliar rupiah dalam anggaran tahunan).\r\n5. Mengambil keputusan tentang  masalah pekerjaan yang diajukan oleh penyelia bawahan, pemimpin tim, atau pegawai yang setara, atau kontraktor.\r\n6. Mengevaluasi pejabat penyelia bawahan atau pemimpin tim dan berfungsi sebagai pejabat peninjau pada evaluasi kinerja pegawai fungsional yang dinilai oleh pejabat penyelia bawahan.\r\n7. Melakukan atau menyetujui seleksi pegawai fungsional di lingkungannya .\r\n8. Merekomendasikan seleksi pejabat penyelia bawahan, pemirnpin tirn, pemimpin kelompok, atau jabatan direktur proyek yang mengkoordinasikan pekerjaan pihak lain, dan jabatan serupa.\r\n9. Mendengarkan dan menyelesaikan keluhan kelompok atau pengaduan pegawai.\r\n10. Meninjau dan menyetujui tindakan disiplin (misalnya teguran) pegawai fungsional bawahan.\r\n11. Mengambil keputusan tentang kebutuhan pelatihan yang mahal atau kontroversial dan pengajuan pelatihan pegawai.\r\n12. Menentukan apakah pekerjaan yang dilakukan kontraktor sudah memenuhi standar kecukupan yang diperlukan untuk otorisasi pembayaran.\r\n13. Menyetujui anggaran biaya kenaikan kelas jabatan, upah lembur, dan perjalanan dinas pegawai.\r\n14. Merekomendasi kan penghargaan bagi pegawai fungsional dan perubahan kelas jabatan.\r\n15. Menemukan dan melaksanakan cara untuk menghapuskan atau mengurangi hambatan dalam pekerjaan, meningkatkan pengembangan tim, atau menyempurnakan metode kerja.', 2, 775),
	(11, 'FS003', 'Jabatan pada tingkat faktor ini harus memenuhi tingkat faktor 3-1 atau tingkat faktor 3-2 ditambah dengan kriteria ketentuan a atau b di bawah ini:\r\nKetentuan a:\r\n1. Mengawasi seluruh perencanaan, pengarahan, dan pelaksanaan suatu program, beberapa segmen program (yang dikelola melalui beberapa unit organisasi bawahan), atau beberapa fungsi staf yang setara, termasuk pengembangan, penugasan, dan pencapaian tujuan dan sasaran pejabat penyelia unit organisasi bawahan.\r\n2. Menyetujui rencana kerja jangka panjang (multi ­ tahun) yang dikembangkan oleh pejabat penyelia unit organisasi bawahan dan mengetola seluruh pekerjaan untuk meningkatkan pencapaian tujuan dan sasaran.\r\n3. Mengawasi perubahan rencana jangka panjang, tujuan dan sasaran pekerjaan yang diarahkan.\r\n4. Mengelola perubahan tingkat alokasi dana atau perubahan lain sebagai akibat dari perkembangan perubahan kebijakan atau undang­ undang.\r\n5. Mengelola perubahan organisasi yang diarahkan, atau perubahan besar pada struktur dan isi program atau segmen program yang diarahkan.\r\n6. Mengalokasikan anggaran dalam organisasi.\r\nATAU\r\nKetentuan b:\r\nMenetapkan tindakan kepegawaian dan proposal desain organisasi yang direkomendasikan oleh pejabat penyelia bawahan.\r\n', 3, 900),
	(12, 'FS004A', 'Hubungan dengan bawahan dalam unit organisasi yang diselia, dengan rekan yang setingkat yang mengawasi  unit  organisasi  yang  setara  dalam instansi, dan/atau dengan staf administrasi dan penunjang yang berada dalam organisasi yang sama dengan penyelia. Hubungan bersifat informal dan terjadi secara perseorangan di tempat kerja, dalam rapat rutin, atau melalui telepon.', 1, 25),
	(13, 'FS004A', 'Hubungan dengan salah satu dari mereka dibawah ini:\r\n1. anggota dunia usaha atau masyarakat luas;\r\n2. pejabat penyelia yang kelasnya lebih tinggi dan stat unit kerja di lapangan atau unit utama organisasi dalam instansi;\r\n3. wakil kelompok kepentingan masyarakat setempat;\r\n4. pegawai di kantor DPRD:\r\n5. pegawai pemerintah daerah; atau\r\n6. wartawan media massa lokal.\r\nHubungan dapat bersifat informal, dalam konferensi dan dalam rapat atau berlangsung melalui telepon, televisi, radio atau cara lain yang serupa, dan ada kalanya memerlukan persiapan khusus.\r\n\r\n', 2, 50),
	(14, 'FS004A', 'Hubungan dengan salah satu dari mereka di bawah ini:\r\n1. pejabat penyelia yang kelasnya lebih tinggi, dan staf biro dan organisasi utama dalam instansi, staf penunjang kantor pusat instansi, atau pegawai yang setara dalam instansi lain;\r\n2. staf kelompok kepentingan masyarakat yang memiliki pengaruh politik;\r\n3. wartawan media masa besar atau koran daerah yang berpengaruh atau liputan radio atau televisi yang setara;\r\n4. asisten staf ahli DPR;\r\n5. stat perusahaan industri yang berskala besar; atau\r\n6. pejabat asosiasi perdagangan daerah atau organisasi keahhan tingkat nasional, kelompok aksi masyarakat, atau organisasi profesional; dan/atau pejabat penyelia instansi pemerintah .\r\nHubungan terjadi dalam rapat dan konferensi dan mereka yang dihubungi tanpa rencana yang untuk ini pegawai terkait ditunjuk sebagai penghubung oleh pihak manajemen yang lebih tinggi. Hubungan memerlukan persiapan yang panjang berupa bahan pengarahan (briefing) atau materi teknis dengan topik yang rumit.\r\n', 3, 75),
	(15, 'FS004A', 'Hubungan dengan salah satu dari mereka di bawah ini:\r\n1. orang atau kelompok organisasi yang berpengaruh dari luar instansi, seperti direktur perusahaan yang mengadakan kontrak dengan instansi atau perwakilan organisasi pegawai;\r\n2. pejabat daerah atau nasional atau perwakilan asisiasi perdagangan, kelompok aksi masyarakat, atau organisasi profesi tingkat nasional;\r\n3. stat ahli DPR;\r\n4. para representatif yang diangkat dan berasaldari pemerintah daerah;\r\n5. wartawan media masa, majalah, televisi atau radio tingkat ibukota,daerah, atau nasional; atau\r\n6. para pejabat dengan kelas yang lebih tinggi dalam instansi lain.\r\nubungan terjadi dalam rapat, pengarahan (briefing), pidato, presentasi, atau pemeriksaan dan mungkin memerlukan jawaban yang tidak diduga sebelumnya. Persiapan meliputi briefing atau bahan presentasi yang memerlukan analisis yang ekstensif oleh pegawai dan bawahan, dan/atau meliputi bantuan oleh staf penunjang.\r\n', 4, 100),
	(16, 'FS004B', 'Tujuan hubungan adalah membahas pekerjaan untuk memberikan atau menerima pelayanan; untuk tukar menukar informasi tentang operasi kerja dan masalah kepegawaian, dan untuk memberikan pelatihan, nasihat, dan bimbingan kepada bawahan.', 1, 30),
	(17, 'FS004B', 'Tujuan hubungan adalah untuk menjamin bahwa informasi yang diberikan kepada pihak luar tepat dan konsisten; untuk merencanakan dan mengkoor ­ dinasikan pekerjaan dengan pegawai yang berada di luar organisasi bawahan; dan/atau untuk menyelesaikan perbedaan pendapat diantara pejabat penyelia, peqawai, kontraktor dan pihak lain.', 2, 75),
	(18, 'FS004B', 'Tujuan hubungan adalah untuk menimbang, mempertahankan atau merundingkan proyek,segmen program unit organisasi yang diarahkan untuk memperoleh sumber daya dan kesesuaian dengan kebijakan, peraturan, atau kontrak	yang sudah ditetapkan.\r\nHubungan biasanya partisipasi aktif dalam konferensi, rapat, pemeriksaan atau presentasi masalah atau isu berdampak cukup besar pada program atau segmen program yang diarahkan.', 3, 100),
	(19, 'FS004B', 'Tujuan hubungan adalah untuk mempengaruhi, memotivasi, atau melakukan persuasi terhadap orang atau kelompok untuk dapat menerima pendapat atau mengambil tindakan untuk mengembangkan tujuan dan sasaran program atau segmen program yang diarahkan, atau untuk mendapatkan komitmen atau distribusi sumber daya yang sangat penting bila terjadi pertentangan atau perlawanan yang gigih yang harus dihadapi karena adanya konflik organisasi atau filosofis yang cukup besar, sasaran yang berbeda, keterbatasan sumber daya atau pengurangannya, atau isu yang setara.\r\nPada tingkat ini, mereka yang dihubungi cukup merasa takut, merasa ragu, atau tidak bekerjasama sehingga kepemimpinan dan keterampilan komunikasi, negosiasi, penyelesaian konflik, harus digunakan untuk memperoleh hasil yang diinginkan.\r\n', 4, 125),
	(20, 'FS005', 'Jabatan dalam faktor ini membawahi jabatan yang nilai jabatannya pada kelas 4 dan dibawahnya atau yang setara.', 1, 75),
	(21, 'FS005', 'Jabatan dalam faktor ini membawahi jabatan yang nilai jabatannya pada kelas 5 atau 6 atau yang setara.', 2, 205),
	(22, 'FS005', 'Jabatan dalam faktor ini membawahi jabatan yang nilai jabatannya pada kelas 7 atau 8 atau yang setara.', 3, 340),
	(23, 'FS005', 'Jabatan dalam faktor ini membawahi jabatan yang nilai jabatannya pada kelas 9 atau 10 atau yang setara.\r\n', 4, 505),
	(24, 'FS005', 'Jabatan dalam faktor ini membawahi jabatan yang nilai jabatannya pada kelas 11 atau 12 atau yang setara.\r\n', 5, 650),
	(25, 'FS005', 'Jabatan dalam faktor ini membawahi jabatan yang nilai jabatannya pada kelas 13 atau yang setara.', 6, 800),
	(26, 'FS005', 'Jabatan dalam faktor ini membawahi jabatan yang nilai jabatannya pada kelas 14 atau yang setara.', 7, 930),
	(27, 'FS005', 'Jabatan dalam faktor ini membawahi jabatan yang nilai jabatannya pada kelas 15 atau lebih tinggi atau yang setara.', 8, 1030),
	(28, 'FS006', 'Pekerjaan yang diselia (disupervisi) meliput pekerjaan klerek, pekerjaan teknis, atau pekerjaan lain yang setara dengan kelas 8 atau lebih rendah. Pekerjaan ini bervariasi dari pekerjaan yang bersifat rutin hingga pekerjaan penyeliaan yang memerlukan koordinasi dalam unit kerja, untuk memastikan waktu, bentuk, prosedur, kesesuaian, standar kualitas dan kuantitas dipenuhi dalam setiap jenis pekerjaan.\r\n', 1, 310),
	(29, 'FS006', 'a. Pekerjaan yang diselia (disupervisi) meliputi pekerjaan klerek, pekerjaan teknis, atau pekerjaan lain yang setara dengan kelas 9 atau kelas 10, atau pekerjaan kelas 6, kelas 7, atau kelas 8 dimana penyelia memiliki wewenang teknis yang penuh dan final terhadap pekerjaan tersebut,yang memerlukan koordinasi dan integrasi pelaksanaan pekerjaan di dalam unit kerja atau dengan unit kerja lain untuk guna menghasilkan produk/jasa akhir. (Wewenang teknis yang penuh dan final berarti bahwa pejabat penyelia bertanggung jawab atas semua keputusan teknis yang timbul dari pekerjaan itu tanpa nasehat atau bantuan teknis dalam masalah yang lebih sulit dan tidak lazim, dan tanpa peninjauan lebih lanjut kecuali dari sudut pandang evaluasi administratif atau evaluasi program). Koordinasi yang diperlukan untuk memastikan konsistensi produk, jasa, penafsiran atau nasihat terhadap kesesuaian output unit lain dan dengan standar atau kebijakan instansi. Pejabat penyelia berkoordinasi dengan pejabat penyelia dari unit lain untuk menangani persyaratan dan masalah yang mempengaruhi pihak lain diluar organisasi.\r\nATAU\r\nb. Jabatan tersebut mengarahkan pejabat penyelia bawahan yang menanganipekerjaan kelas 8 atau yang lebih rendah, dimana pengkoordinasian pekerjaan yang dilakukan unit bawahan memerlukan upaya yang berkefanjutan guna memastikan standar kuaiitas dan jasa , kecepatan, waktu, bentuk, prosedur, kesesuaian, dan kuantitas.', 2, 575),
	(30, 'FS006', 'a. Penyeliaan pada tingkat ini memerlukan koordinasi, integrasi, atau konsolidasi pekerjaan administratif, pekerjaan teknis, atau pekerjaan lain yang setara dengan kelas 11 atau kelas 12, atau pekerjaan kelas 9 atau kelas 10 dimana penyelia memiliki wewenang teknis penuh dan final terhadap pekerjaannya. Tingkat ini dicapai bila pekerjaan yang diarahkan bersifat analisis, penafsiran, penilaian, evaluasi, atau kreatif. Pekerjaan tersebut menuntut penyelia menyelesaikan konflik dan mempertahankan kesesuaian penafsiran, penilaian, logika dan penerapan kebijakan,  karena:\r\n• fakta, informasi, dan keadaan sering bervariasi;\r\n• pedoman tidak lengkap dan tidak dengan segera memberikan hasil yang identik; atau\r\n• perbedaan penilaian, rekomendasi, tafsiran, atau keputusan dapat berakibat terhadap pekerjaan bawahan lain.\r\nb. Pekerjaan mungkin diselesaikan oleh suatu tim, dimana setiap anggota tim memberikan kontribusi bagian dari analisis, fakta, informasi usulan tindakan, atau rekomendasi, yang kemudian diintegrasikan oleh pejabat penyelia.\r\nATAU\r\nJabatan ini mengarahkan penyelia bawahan dengan jabatan kelas 9 atau kelas 10 atau yang setara, yang memerlukan konsolidasi atau koordinasi yang serupa dengan yang diuraikan pada tingkat faktor 6-2a di kalangan unit bawahan atau dengan unit tuar.\r\n\r\n', 3, 975),
	(31, 'FS006', 'a. Penyetiaan pada tingkat ini memerlukan koordinasi dan integrasi yang cukup besar dari sejumlah pekerjaan, proyek atau segmen program pekerjaan profesional atau pekerjaan administratif dengan kelas 13. Misalnya, koordinasi melibatkan pekerjaan yang setara dengan salah satu dari yang berikut ini:\r\n1. Mengidentifikasi dan mengintegrasikan program internal dan eksternal yang mempengaruhi langsung organisasi, seperti yang berkaitan dengan faktor teknis, keuangan, organisasi, dan administrasi.\r\n2. Mengintrogasikan pekerjaan tim atau kelompok dimana setiap anggota tim ikut berperan memberikan sebagian analisis. fakta, informasi, usulan tindakan, atau rekomendasi dan/atau memastikan penafsiran sesuai dan konsisten dengan  penilaian, logika, dan kebijakan.\r\n3. Merekomendasikan sumber daya untuk dikerahkan kepada proyek tertentu atau untuk dialokasikan di antara segmen program;\r\n4. Kepemimpinan mengembangkan, melaksanakan, mengevaluasi, dan menyempurnakan proses dan prosedur guna memantau keefektifan, efisiensi, dan produktifitas segmen program dan/atau organisasi yang diarahkan.\r\n5. Meninjau dan menyetujui isi laporan, keputusan, dokumen kasus, kontrak, atau dokumen lain guna memastikan bahwa sudah sesuai dengan kebijakan dan pandangan instansi.\r\nATAU\r\nb. Jabatan itu mengarahkan penyelia bawahan dan/atau kontraktor yang mengarahkan pekerjaan kelas 11 atau kelas 12. Pekerjaan dasar memerlukan koordinasi yang serupa dengan yang diuraikan pada tingkat faktor 6-3a di atas, untuk penyelia tingkat pertama.\r\n', 4, 1120),
	(32, 'FS006', 'a. Penyeliaan pada tingkat ini  memerlukan koordinasi dan integ rasi yang cukup besar terhadap sejumlah proyek atau segmen program pekerjaan profesional, manajerial, atau pekerjaan administratif yang setara dengan kelas 14. Penyeliaan melibatkan sejumlah rekomendasi penting yang memiliki dampak langsung dan cukup besar terhadap organisasi dan proyek. Misalnya, membuat rekomendasi dalam sekurang-kurangnya 3 bidang yang tercantum di bawah ini atau dalam bidang lain yang setara:\r\n1. Program internal dan eksternal dan kebijakan yang penting yang mempengaruhi seluruh organisasi, seperti kondisi politik, teknologi, dan ekonomi selain sejumlah faktor yang disebutkan dalam butir pertama pada tingkat faktor 6-4a:\r\na. Mengidentifikasi dan mengintegrasikan program internal dan eksternal yang mempengaruhi langsung organisasi, seperti yang berkaitan dengan faktor teknis, keuangan, organisasi, dan administrasi.\r\nb. Mengintegrasikan pekerjaan tim atau kelompok dimana setiap anggota tim ikut berperan memberikan sebagian analisis, fakta, informasi, usulan tindakan, atau rekomendasi dan/atau memastikan penafsiran sesuai dan konsisten dengan penilaian,logika, dan kebijakan.\r\nc. Merekomendasikan sumber daya untuk dikerahkan kepada proyek tertentu atau untuk dialokasikan di antara segmen program.\r\nd. Kepemimpinan mengembangkan, melaksanakan, mengevaluasi, dan menyempurnakan proses dan prosedur guna memantau keefektifan, efisiensi, dan produktifitas segmen program dan/atau organisasi yang diarahkan.\r\ne. Meninjau dan menyetujui isi laporan, keputusan, dokumen kasus, kontrak, atau dokumen lain guna memastikan bahwa sudah sesuai dengan kebijakan dan pandangan instansi.\r\n2. Melakukan restrukturisasi, reorientasi, evaluasi ulang tujuan, sasaran, rencana, dan jadwal jangka pendek	dan jangka panjang untuk menyesuaikan dengan perubahan perundang-undangan, program,dan/atau pendanaan.\r\n3. Memustuskan proyek atau segmen program yang harus dilanjutkan, dikesampingkan, atau dikurangi.\r\n4. Merubah struktur organisasi, termasuk perubahan tertentu yang harus dilaksanakan.\r\n5. Optimalisasi pengurangan biaya operasional dan kepastian keefektifan program, termasuk diperkenalkannya alat yang menghemat tenaga kerja, proses otomatisasi, penyempurnaan metode, dan hal-halyang serupa.\r\n6. Sumber daya yang harus diterapkan untuk program tertentu (terutama mengenai anggaran organisasi).\r\n7. Perumusan kebijakan dan perencanaan jangka panjang sehubungan dengan peruba­ han fungsi dan program yang memberikan pengharapan.\r\nATAU\r\nb. Penyeliaan pekerjaan profesional, administratif, atau yang setara dengan kelas 15 atau di atasnya, yang sangat mendesak, yang tidak lazim untuk penelitian, pengembangan, tes dan evaluasi, desain, analisis kebijakan, pelayanan masyarakat, kesehatan masyarakat, dan implikasi medis, peraturan, atau implikasi lain yang setara.\r\nATAU\r\nc. Mengelola pekerjaan melalui penyelia bawahan dan/atau kontraktor yang masing-masing mengarahkan pekerjaan dengan kelas 13. Pekerjaan dasar semacam ini memerlukan koordinasi yang serupa dengan yang diuraikan pada tingkat faktor 6-4a di atas untuk penyelia tingkat pertama.\r\n', 5, 1225),
	(33, 'FS006', 'a. Penyeliaan pada tingkat ini memerlukan koordinasi dan integrasi yang luar biasa dari sejumlah segmen program pekerjaan profesional manajerial, atau pekeaan administratif yang setara dengan kelas 15 atau yang lebih tinggi. Penyeliaan dan pengelolaan sumber daya pada tingkat ini meliputi sejumlah keputusan dan tindakan penting yang memiliki dampak langsung dan cukup besar terhadap organisasi dan program yang dikelola. Misalnya, penyelia pada tingkat ini menyusun rekomendasi dan/atau keputusan akhir tentang sebagian besar bidang yang diuraikan pada tingkat faktor 6-5a:\r\n1. Program internal dan eksternal dan kebijakan yang penting yang mempengaruhi seluruh organisasi, seperti kondisi politik, teknologi, dan ekonomi selain sejumlah faktor yang disebutkan dalam butir pertama pada tingkat faktor 6-4a:\r\na. Mengidentifikasi dan mengintegrasikan program internal dan eksternal yang mempengaruhi langsung organisasi, seperti yang berkaitan dengan faktor teknis, keuangan, organisasi, dan administrasi.\r\nb. Mengintegrasikan pekerjaan tim atau kelompok dimana setiap anggota tim ikut berperan memberikan sebagian analisis, fakta, informasi, usulan tindakan, atau rekomendasi dan/atau memastikan penafsiran sesuai dan konsisten dengan penilaian, logika, dan kebijakan.\r\nc. Merekomendasikan sumber daya untuk dikerahkan kepada proyek tertentu atau untuk dialokasikan di antara segmen program.\r\nd. Kepemimpinan mengembangkan, melaksanakan, mengevaluasi, dan menyempurnakan proses dan prosedur guna memantau keefektifan, efisiensi, dan produktifitas segmen program dan/atau organisasiyang diarahkan.\r\ne. Meninjau dan menyetujui isi laporan, keputusan, dokumen kasus, kontrak, atau dokumen lain guna memastikan bahwa sudah sesuai dengan kebijakan dan pandangan instansi.\r\n2. Melakukan restrukturisasi, reorientasi, evaluasi ulang tujuan, sasaran, rencana, dan jadwal jangka pendek dan jangka panjang untuk menyesuaikan dengan perubahan perundang­ undangan, program, dan/atau pendanaan.\r\n3. Memustuskan proyek atau segmen program yang harus dilanjutkan, dikesampingkan, atau dikurangi.\r\n4. Merubah struktur organisasi, termasuk perubahan tertentu yang harus dilaksanakan.\r\n5. Optimalisasi pengurangan biaya operasional dan kepastian keefektifan program, termasuk diperkenalkannya alat yang menghemat tenaga kerja, proses otomatisasi, penyempurnaan metode, dan hal-hal yang serupa.\r\n6. Sumber daya yang harus diterapkan untuk program tertentu (terutama mengenai anggaran organisasi).\r\n7. Perumusan kebijakan dan perencanaan jangka panjang  sehubungan  dengan perubahan fungsi dan program yang memberikan pengharapan. \r\nAtau yang setara.\r\nATAU\r\nb. Mengelola pekerjaan melalui penyelia bawahan dan/atau kontraktor yang mengarahkan pekerjaan dengan kelas 14 atau yang lebih tinggi. Pekerjaan ini memerlukan koordinasi yang serupa dengan yang diuraikan pada tingkat faktor 6-Sa di atas untuk penyelia tingkat pertama.\r\n', 6, 1325),
	(34, 'FF001', 'Pengetahuan tentang tugas atau operasi yang sederhana, rutin, atau berulang, yang secara khusus mengikuti instruksi langkah demi langkah, dan sedikit atau sama sekali tidak membutuhkan pelatihan atau pengalaman sebelumnya;\r\nATAU\r\nKeterampilan untuk menjalankan peralatan sederhana atau peralatan yang beroperasi secara berkala, yang sedikit atau sama sekali tiidak membutuhkan pelatihan atau pengalaman sebelumnya;\r\nATAU\r\nPengetahuan dan keterampilan yang setara.\r\n', 1, 50),
	(35, 'FF001', 'Pengetahuan tentang prosedur, peraturan. atau operasi dasar atau umum, yang secara khusus membutuhkan sedikit pelatihan atau pengalaman sebelumnya;\r\nATAU\r\nKeterampilan dasar untuk mengoperasikan peralatan yang membutuhkan sedikit pelatihan dan pengalaman sebelumnya, seperti peralatan keyboard;\r\nATAU\r\nPengetahuan dan keterampilan yana setara.\r\n', 2, 200),
	(36, 'FF001', 'Pengetahuan tentang sejumlah peraturan, prosedur, dan operasi, yang membutuhkan pelatihan dan pengalaman yang cukup untuk melaksanakan pekerjaan klerek dan menyelesaikan masalah yang muncul;\r\nATAU\r\nKeterampilan, yang membutuhkan pelatihan dan pengalaman yang cukup, untuk mengoperasikan dan menyesuaikan peralatan dalam berbagai tujuan, seperti melaksanakan sejumlah tes atau operasi standar;\r\nATAU\r\nPengetahuan dan keterampilan yang setara.', 3, 250),
	(37, 'FF001', 'Pengetahuan tentang sejumlah peraturan, prosedur, atau operasi, yang membutuhkan pelatihan dan pengalaman yang luas untuk melaksanakan berbagai pekerjaan yang tidak standar dan saling berhubungan, dan menyelesaikan berbagai macam masalah;\r\nATAU\r\nPengetahuan praktis tentang standar prosedur dibidang teknik, yang membutuhkan pelatihan atau pengalaman luas, untuk:\r\n1. melaksanakan pekerjaan yang membutuhkan peralatan yang membutuhkan pertimbangan dan karakteristik tertentu;\r\n2. menginterpretasikan hasil tes berdasarkan pengalaman dan observasi sebelumnya (tanpa membaca langsung instrumen atau alat pengukur lainnya); atau\r\n3. membuat intisari informasi dari berbagai sumber dan mempertimbangkan karakteristik dan kualitas surnber informasi tersebut untuk diterapkan;\r\nATAU\r\nPengetahuan dan keterampilan yang setara.', 4, 550),
	(38, 'FF001', 'Pengetahuan (yang diperoleh melalui program pendidikan sarjana atau  yang setara dalam pengalaman , pelatihan, atau belajar sendiri) dasar tentang prinsip, konsep, dan metodologi pekerjaan profesional atau pekerjaan administratif, dan keterampilan dalam penerapan pengetahuan tersebut untuk melaksanakan tugas, operasi, atau prosedur dasar;\r\nATAU\r\nSebagai tambahan pengetahuan praktis pada tingkat faktor 1-4, pengetahuan praktis tentang metode teknis melaksanakan pekerjaan seperti proyek yang membutuhkan teknik yang rumit dan khusus;\r\nATAU\r\nPengetahuan dan keterampilan yang setara.', 5, 750),
	(39, 'FF001', 'Pengetahuan tentang prinsip, konsep, dan metodologi pekerjaan profesional atau pekerjaan administratif seperti pada tingkat faktor 1-5, yang (a) ditambah dengan keterampilan yang diperoleh melalui pengalaman mengerjakan sendiri pekerjaan yang berulang, atau (b) ditambah dengan pengembangan pengetahuan profesional atau pengetahuan administratif yang diperoleh melalui pengalaman atau lulus sarjana yang relevan, yang memberikan keahlian dalam pelaksanaan tugas, operasi dan prosedur pekerjaan yang secara signifikan lebih sulit dan rumit dari yang dicakup pada tingkat faktor 1-5;\r\nATAU\r\nPengetahuan praktis dengan cakupan yang luas tentang metode, teknik, prinsip dan praktek yang serupa untuk pekerjaan profesional yang sempit, dan keterampilan dalam penerapan pengetahuan tersebut dalam pekerjaan desain dan perencanaan yang sulit tapi merupakan proyek yang dijadikan contoh;\r\nATAU\r\nPengetahuan dan keterampilan setara.', 6, 950),
	(40, 'FF001', 'Pengetahuan tentang berbagai konsep, prinsip, dan praktek pekerjaan profesional atau pekerjaan administratif, yang dapat diperoleh melalui pendidikan diatas sarjana atau pengalaman yang luas, dan keterampilan didalam penerapan pengetahuan tersebut dalam pekerjaan yang sulit dan kompleks;\r\nATAU\r\nPengetahuan praktis yang komprehensif dan intensif dari suatu bidang teknik, dan keterampilan dalam penerapan pengetahuan tersebut dalam pengembangan metode, pendekatan, atau prosedur baru;\r\nATAU\r\nPengetahuan dan keterampilan setara.\r\n', 7, 1250),
	(41, 'FF001', 'Pakar pekerjaan profesional atau pekerjaan administratif untuk:\r\n1. menerapkan teori eksperimental dan pengembangan baru dalam masalah yang tidak sesuai dengan metode yang telah dapat diterima;\r\nATAU\r\n2. membuat keputusan atau rekomendasi yang secara signifikan merubah, menafsirkan, atau mengembangkan program atau kebijakan publik yang penting;\r\nATAU\r\nPengetahuan dan keterampilan yang setara.', 8, 1550),
	(42, 'FF001', 'Pakar pekerjaan profesional untuk menciptakan dan mengembangkan teori dan hipotesa baru.\r\nATAU\r\nPengetahuan dan keterampilan yang setara.', 9, 1850),
	(43, 'FF002', 'Untuk tugas sejenis  dan berulang, penyelia membuat tugas tertentu dlsertai dengan instruksi yang jelas,terperinci, dan spesifik.\r\nPegawai bekerja sesuai instruksi dan berkonsultasi dengan penyelia sebagaimana dibutuhkan untuk semua persoalan yang tidak spesifik dicakup di dalam instruksi atau pedoman.\r\nUntuk semua jabatan, pekerjaan diawasi dengan teliti. Untuk beberapa jabatan, pengawasan berdasarkan sifat pekerjaan itu sendiri; untuk jabatan yang lain, pekerjaan diawasi sebagaimana pekerjaan itu dilaksanakan. Dalam situasi tertentu, penyelia melakukan peninjauan pekerjaan, termasuk pengecekan kemajuan pekerjaan atau peninjauan pekerjaan yang telah selesai untuk tujuan keakuratan, kecukupan, dan ketaatan pada instruksi dan prosedur yang ditetapkan.', 1, 25),
	(44, 'FF002', 'Penyelia memberikan tugas berkelanjutan atau tugas tertentu dengan mengindikasikan secara umum apa yang harus diselesaikan, batasan, kualitas, dan kuantitas yang diharapkan, batas waktu dan prioritas tugas. Penyelia memberikan tambahan instruksi untuk tugas baru, sulit, atau yang tidak biasa, termasuk metode kerja yang disarankan atau saran pada sumber materi yang tersedia.\r\nPegawai menggunakan inisiatif dalam melaksanakan tugas yang berulang secara mandiri tanpa instruksi spesifik, tapi melaporkan deviasi, rnasalah, dan situasi yang tidak lazim yang tidak dicakup dalam instruksi kepada penyelia untuk membuat keputusan atau meminta bantuan.\r\nPenyelia menjamin bahwa pekerjaan yang telah selesai dan metode yang digunakan adalah secara teknik akurat dan memenuhi instruksi atau prosedur yang ada. Tinjauan pekerjaan meningkat sesuai dengan tugas yang lebih sulit dan pegawai tidak melaksanakan tugas yang sama sebelumnya.\r\n', 2, 125),
	(45, 'FF002', 'Penyelia memberikan tugas dengan tujuan, prioritas, dan batas waktu yang ditentukan, dan membantu pegawai pada situasi yang tidak lazim dan belum ada contoh yang jelas.\r\nPegawai merencanakan dan melaksanakan langkah-langkah	yang sesuai dan menangani masalah dan deviasi pekerjaan sesuai dengan instruksi, kebijakan, latihan sebelumnya, atau praktek yang berlaku.\r\nPekerjaan yang telah selesai biasanya dievaluasi untuk kesesuaian teknik, kelayakan dan kesesuaian pada kebijakan dan persyaratan. Metode yang digunakan untuk mendapatkan hasil akhir biasanya tidak ditinjau secara terperinci.\r\n', 3, 275),
	(46, 'FF002', 'Penyelia menentukan tujuan dan sumber daya yang tersedia. Pegawai dan penyelia berkonsultasi mengembangkan batas waktu, proyek, dan pekerjaan yang harus dilakukan.\r\nPegawai yang mempunyai keahlian dalam pekerjaan, bertanggung jawab untuk perencanaan dan pelaksanaan tugas, pemecahan sebagian besar konflik yang timbul, pengkoordinasian pekerjaan dengan yang lainnya sebagaimana diperlukan, dan menginterpretasikan kebijakan atas inisiatif sendiri sesuai dengan tujuan yang ditetapkan. Dalam beberapa pekerjaan, pegawai menentukan pendekatan dan metodologi yang akan digunakan. Pegawai  menginformasikan penyeiia kemajuan dan masalah kontroversial.\r\nPekerjaan yang telah selesai dievaluasi secara umum dalam hal kelayakan, kesesuaian dengan pekerjaan yang lain, atau keefektifan dalam memenuhi persyaratan atau hasil yang diharapkan.\r\n', 4, 450),
	(47, 'FF002', 'Penyelia memberikan tugas dengan petunjuk dalam terminologi misi atau fungsi yang didefenisikan dengan luas.\r\nPegawai mempunyai tanggungjawab untuk perencanaan, desain dan melaksanakan program, proyek, studi, atau pekerjaan secara mandiri.\r\nHasil kerja dipertimbangkan secara teknis dan biasanya diterima tanpa perubahan yang besar. Jika pekerjaan dievaluasi, evaluasi dalam hal seperti kesesuaian dengan tujuan program. dampak dari saran dan pengaruh pada seluruh program, atau kontribusi pada kemajuan teknologi. Rekomendasi proyek baru dan perubahan tujuan biasanya dievaluasi untuk pertimbangan ketersediaan dana, sumber-sumber lain, tujuan program secara luas, atau prioritas nasional.', 5, 650),
	(48, 'FF003', 'Pedoman terperinci dan khusus, yang meliputi semua aspek penting tugas yang diberikan kepada pegawai.\r\nPegawai harus patuh dan taat pada pedoman, penyimpangan harus disetujui oleh penyelia.', 1, 25),
	(49, 'FF003', 'Prosedur metaksanakan pekerjaan ditetapkan dan sejumlah pedoman tersedia. Pegawai mengunakan pertimbangan dalam memilih pedoman, referensi, dan prosedur yang paling tepat untuk diterapkan pada kasus tertentu dengan deviasi yang terkecil (minor). Pegawai dapat menentukan alternatif yang ada untuk digunakan. Situasi dimana pedoman yang ada tidak dapat diterapkan atau terjadi penyimpangan dari pedoman yang diajukan harus mengacu pada penyelia.\r\n', 2, 125),
	(50, 'FF003', 'Pedoman tersedia tapi tidak sepenuhnya dapat diterapkan pada pekerjaan atau mempunyai gap (kesenjangan) dalam spesifikasi.\r\nPegawai menggunakan pertimbangan dalam menginterpretasikan dan mengadaptasikan pedoman seperti kebijakan lembaga, peraturan, dan langkah kerja untuk penerapan pada masalah atau kasus tertentu. Pegawai menganalisa hasil dan merekomendasikan perubahan.\r\n', 3, 275),
	(51, 'FF003', 'Kebijakan dan peraturan dapat diterapkan tetapi dinyatakan dalarn terminologi umum. Pedoman pelaksanaan pekerjaan langka atau penggunaannya terbatas.\r\nPegawai menggunakan inisiatif dan akal pikiran dalam penyimpangan dari metode atau kecenderungan dan pola yang ada untuk mengembangkan metode, kriteria, atau kebijakan baru.\r\n', 4, 450),
	(52, 'FF003', 'Pedoman dinyatakan secara luas dan tidak spesifik, antara lain, pernyataan kebijakan secara luas dan peraturan yang membutuhkan interpretasi yang luas.\r\nPegawai harus menggunakan  pertimbangan dan kecerdasan menginterpretasikan maksud dari pedoman yang ada untuk pemakaiannya pada pekerjaan. Pegawai diberikan wewenang teknis untuk mengembangkan dan menginterpretasikan pedoman.\r\n', 5, 650),
	(53, 'FF004', 'Pekerjaan terdiri dari tugas-tugas yang jelas dan berhubungan secara langsung.\r\nSedikit atau sama sekali tidak ada pilihan yang harus dibuat di dalam memutuskan apa yang harus dilakukan.\r\nTindakan yang akan diambil atau respons yang harus dibuat sudah dapat dilihat.\r\nPekerjaan secara cepat dapat dikuasai.\r\n', 1, 25),
	(54, 'FF004', 'Pekerjaan terdiri dari tugas yang mencakup langkah , proses, atau metode yang berhubungan.\r\nKeputusan mengenai apa yang harus dilakukan, mencakup berbagai macam pilihan yang mempersyaratkan pegawai perlu mengenali keberadaan dan perbedaan diantara beberapa situasi yang secara mudah dapat dikenali.\r\nTindakan yang diambil atau respons yang dibuat adalah berbeda tergantung pada sumber informasi, cara mendapatkan informasi (transaksi), atau perbedaan sifat faktual lainnya.\r\n', 2, 75),
	(55, 'FF004', 'Pekerjaan mencakup berbagai tugas yang melibatkan proses dan metode yang berbeda dan tidak berhubungan.\r\nKeputusan mengenai apa yang harus dilakukan, tergantung pada analisa subjek, fase, atau persoalan yang terlibat dalam setiap tugas, atau tindakan yang diambil harus dipilih dari berbagai macam alternatif.\r\nPekerjaan melibatkan kondisi dan elemen yang harus diidentifikasi dan dianalisa untuk melihat hubungan timbal balik.', 3, 150),
	(56, 'FF004', 'Pekerjaan mencakup berbagai tugas yang membutuhkan berbagai macam proses dan metode yang berbeda dan tidak berhubungan, seperti tugas yang berhubungan dengan bidang pekerjaan administratif atau bidang pekerjaan profesional.\r\nKeputusan mengenai apa yang harus dilakukan, membutuhkan penilaian dari keadaan yang tidak lazim, variasi pendekatan, dan data yang tidak lengkap atau yang berrnasalah.\r\nPekerjaan mempersyaratkan beberapa keputusan tentang penginterpretasian data yang sangat besar, perencanaan kerja, atau penyempurnaan metode dan teknik yang akan digunakan.\r\n\r\n', 4, 225),
	(57, 'FF004', 'Pekerjaan mencakup berbagai tugas yang membutuhkan berbagai macam proses dan metode yang berbeda dan tidak berhubungan, yang diterapkan pada suatu aktifitas yang luas atau analisa yang sangat dalam, khususnya untuk bidang pekerjaan administratif dan bidang pekerjaan profesional.\r\nKeputusan mengenai apa yang harus dilakukan, membutuhkan ketidakpastian yang besar dalam hal pendekatan, metodologi, atau interpretasi dan evaluasi proses, yang dihasilkan dari suatu unsur yang berubah dan berkelanjutan dalam program, pengembangan teknologi, fenomena yang tidak dapat diduga, atau persyaratan yang bermasalah.\r\nPekerjaan membutuhkan teknik baru, penetapan kriteria baru, atau pengembangan informasi baru.\r\n', 5, 325),
	(58, 'FF004', 'Pekerjaan terdiri dari fungsi dan proses yang luas dari bidang pekerjaan administratif dan pekerjaan profesional. Tugas ditandai dengan luas dan tingginya intensitas usaha yang diperlukan dan melibatkan beberapa fase yang harus diikuti secara bersamaan dengan dukungan dari dalam atau dari luar organisasi.\r\nKeputusan mengenai apa yang harus dilakukan, membutuhkan banyak isu atau elemen yang tidak terdefinisikan, yang membutuhkan analisa dan pernbuktian yang ekstensif untuk menentukan sifat dan lingkup rnasalah.\r\nPekerjaan membutuhkan usaha yang berkelanjutan untuk rnenetapkan konsep, teori, atau program, atau untuk memecahkan masalah yang sulit.\r\n', 6, 450),
	(59, 'FF005', 'Tugas meliputi pekerjaan tertentu bersifat rutin dengan beberapa prosedur yang terpisah.\r\nHasil kerja dan jasa yang diberikan untuk memfasilitasi pekerjaan orang lain tetapi mempunyai sedikit dampak di luar unit organisasi langsung.\r\n', 1, 25),
	(60, 'FF005', 'Pekerjaan meliputi pelaksanaan peraturan, regulasi, atau  prosedur tertentu, dan merupakan bagian dari suatu tugas atau proyek dengan ruang lingkup yang lebih luas.\r\nHasil kerja atau jasa mempengaruhi keakuratan, kelayakan, atau akseptabilitas dari proses atau pelayanan lebih lanjut.', 2, 75),
	(61, 'FF005', 'Pekerjaan meliputi perlakuan terhadap berbagai macam masalah, pertanyaan, atau situasi konvensional sesuai dengan kriteria yang ditetapkan.\r\nHasil kerja atau jasa mempengaruhi desain atau operasi dari sistem, program, atau peralatan, kelayakan kegiatan seperti investigasi lapangan, pengetesan operasi, atau hasil penelitian, atau kondisi sosial,fisik, dan ekonomi masyarakat\r\n', 3, 150),
	(62, 'FF005', 'Pekerjaan meliputi penetapan kriteria, memformulasikan proyek, menilai efektifitas program, atau menginvestigasi atau menganalisa berbagai kondisi, masalah, atau pertanyaan yang tidak lazim.\r\nHasil kerja atau jasa mempengaruhi berbagai aktivitas lembaga, aktifitas utama industri, atau operasi instansi lain.\r\n', 4, 225),
	(63, 'FF005', 'Pekerjaan meliputi pengisolasian dan pendefinisian kondisi yang tidak diketahui, pemecahan masalah kritis, atau pengembangan teori baru.\r\nHasil kerja atau jasa mempengaruhi pekerjaan para ahli lainnya, pengembangan aspek utama dari program atau misi pekerjaan administratif atau pengembangan ilmiah atau sifat-sifat dari orang yang banyak.\r\n', 5, 325),
	(64, 'FF005', 'Pekerjaan  meliputi perencanaan, pengembangan, dan pelaksanaan program utarna pekerjaan administratif atau pengembangan ilmiah.\r\nProgram tersebut penting untuk misi suatu lembaga atau mempengaruhi sejumlah besar orang dalam jangka panjang dan berkelanjutan.\r\n', 6, 450),
	(65, 'FF006', 'Hubungan dengan pegawai di	unit organisasi, kantor, proyek, atau unit kerja, dan di dalam unit pendukung.\r\nDAN/ATAU\r\nHubungan dengan anggota masyarakat luas di dalam situasi yang tertentu, antara lain, tujuan hubungan dan dengan siapa berhubungan relatif jelas. Ciri khas hubungan pada tingkat ini hanya pada bagaimana cara memulai hubungan.', 1, 10),
	(66, 'FF006', 'Hubungan dengan pegawai di dalam lembaga yang sama tetapi di luar unit organisasi. Pegawai yang dihubungi biasanya berbeda dalam fungsi,  misi, dan jenis kerja, antara lain perwakilan dari berbagai tingkat dalam suatu lembaga, seperti kantor pusat, kantor regional, kantor distrik atau kantor lapangan atau kantor pelaksana lainnya.\r\nDAN/ATAU\r\nHubungan dengan anggota masyarakat sebagai individu atau grup. Contoh, hubungan biasanya ditetapkan atas dasar rutin, biasanya pada ruang kerja pegawai, tujuan dari hubungan tidak jelas pada awalnya untuk satu atau lebih kelompok, dan satu atau lebih pihak tidak terinformasi mengenai peranan dan wewenang masing-masing. Ciri khas hubungan pada tingkat ini adalah dengan orang yang mencari reservasi tiket pesawat atau pelamar kerja pada pusat informasi pekerjaan.', 2, 25),
	(67, 'FF006', 'Hubungan dengan individu atau grup dari luar instansi. Sebagai contoh hubungan yang tidak ditetapkan atas dasar rutin, tujuan dan maksud dari setiap hubungan berbeda, dan peranan dan wewenang masing-masing dikembangkan dan diidentifikasikan selama berhubungan. Ciri khas hubungan dalam tingkat ini adalah orang sebagai pengacara, kontraktor atau perwakilan dari organisasi profesional, media berita atau kelompok aksi masyarakat.', 3, 60),
	(68, 'FF006', 'Hubungan dengan pejabat tinggi dari luar instansi pada level nasional atau internasional, misalnya antara lain, hubungan dengan pejabat yang secara relatif tidak mudah dapat dicapai, pengaturan mungkin harus dibuat untuk menemani anggota staf; penunjukan mungkin dibuat diawal, setiap pihak mungkin sangat tidak jelas peranan atau wewenangnya; dan setiap hubungan mungkin dilaksanakan di bawah peraturan yang berbeda. Ciri khas hubungan pada tingkat ini terdapat pada anggota DPR, pimpinan perwakilan dari pemerintahan luar negeri, pimpinan perusahaan nasional atau internasional, perwakilan media nasional, pimpinan organisasi nasional, gubernur, atau bupati/walikota.\r\n', 4, 110),
	(69, 'FF007', 'Tujuan hubungan adalah untuk memperoleh, mengklarifikasi, atau memberikan fakta atau informasi tanpa  menghiraukan sifat  fakta  tersebut antara lain, fakta atau informasi bervariasi dari yang mudah dimengerti sampai dengan yang sangat teknis.\r\n', 1, 20),
	(70, 'FF007', 'Tujuan hubungan adalah untuk merencanakan, mengkoordinasikan, atau mengarahkan pekerjaan atau untuk memecahkan masalah dengan mempengaruhi atau memotivasi individu atau kelompok untuk mencapai tujuan bersama dan yang pada dasarnya mempunyai sikap bekerja sama.\r\n', 2, 50),
	(71, 'FF007', 'Tujuan hubungan adalah untuk mempengaruhi, memotivasi, menginterogasi, mengawasi orang atau group (kelompok). Orang yang dihubungi mungkin penakut, skeptis, tidak mau bekerjasama, atau berbahaya. Pegawai harus mempunyai keahlian dalam mendekati individu atau kelompok untuk mencapai tujuan yang diinginkan, seperti untuk mencapai kesesuaian dengan kebijakan dan peraturan yang ada melalui persuasi  atau negosiasi, atau memperoleh informasi dengan membuat laporan kepada informan.', 3, 120),
	(72, 'FF007', 'Tujuan hubungan adalah untuk mempertimbangkan, membela, menegosiasi,atau menyelesaikan masalah mengenai hal-hal yang kontroversial atau signifikan. Pekerjaan biasanya mencakup partisipasi aktif dalam konferensi, pertemuan, ceramah, atau presentasi yang meliputi masalah atau persoalan yang sangat penting. Orang yang dihubungi secara khusus mempunyai pandangan, tujuan akhir, dan sasaran yang berbeda, yang membutuhkan pegawai untuk mencapai pengertian umum dari masalah dan solusi yang memuaskan, dengan meyakinkan mereka, pencapaian kompromi, atau pengembangan alternatif yang sesuai.', 4, 220),
	(73, 'FF008', 'Pekerjaan adalah menetap. Pegawai dapat duduk dengan nyaman untuk melakukan pekerjaan. Walaupun demikian mungkln kadang-kadang berjalan, berdiri, menunduk, membawa benda ringan seperti kertas, buku atau bagian yang kecil, atau mengendarai mobil. Tidak ada persyaratan fisik khusus yang dibutuhkan untuk melaksanakan pekerjaan.', 1, 5),
	(74, 'FF008', 'Pekerjaan membutuhkan tenaga fisik sepertiberdiri dalam waktu yang lama, berjalan di jalan yang kasar, tidak rata, atau permukaan berbatu, aktivitas memerlukan membengkok, meringkuk, membungkuk, merentangkan, mengapai, atau sejenisnya, mengangkat benda yang cukup berat berulang seperti mesin ketik atau kotak dokumen. Pekerjaan tersebut mungkin membutuhkan karakteristik dan kemampuan fisik seperti ketangkasan dan kegesitan diatas rata-rata.', 2, 20),
	(75, 'FF008', 'Pekerjaan ini membutuhkan tenaga fisik yang besar dan luar biasa seperti sering menaiki tangga yang tinggi, mengangkat benda berat di atas 20 kg, meringkuk atau merangkak di daerah terlarang, dan mempertahankan diri sendiri atau yang lain terhadap serangan fisik.', 3, 50),
	(76, 'FF009', 'Lingkungan membawa resiko dan ketidaknyamanan setiap hari, yang membutuhkan tindakan pencegahan keamanan khususnya pada tempat-tempat seperti, kantor, ruang rapat dan pelatihan, perpustakaan, perumahan, kendaraan umum, antara lain, penggunaan praktek kerja yang aman pada peralatan kantor, menghindari licin dan jatuh, pengamatan peraturan kebakaran dan tanda lalu lintas. Situasi kerja cukup terang, tidak panas, dan cukup ventilasi.', 1, 5),
	(77, 'FF009', 'Lingkungan pekerjaan membawa resiko dan ketidaknyamanan yang cukup besar, yang memerlukan tindakan pencegahan keamanan khusus antara lain, bekerja disekitar benda bergerak, kereta, atau mesin, berhadapan dengan penyakit menular atau iritasi bahan kimia. Pegawai dipersyaratkan menggunaka n pakaian pelindung, seperti topeng, baju, jaket, sepatu boot, kacamata debu, sarung tangan, atau baju pelindung.', 2, 20),
	(78, 'FF009', 'Lingkungan pekerjaan membawa resiko tinggi dengan berhadapan pada situasi bahaya yang sangat potensial atau stres lingkungan yang tidak umum, yang mempersyaratkan pengamanan dan tindakan pencegahan antara lain bekerja pada tempat yang sangat tinggi di bawah kondisi cuaca yang ekstrim, yang memungkinkan serangan fisik, atau situasi sejenis dimana kondisi tidak dapat dikontrol.', 3, 50);
/*!40000 ALTER TABLE `master_faktor_detail` ENABLE KEYS */;

-- Dumping structure for table ortala.master_instansi
CREATE TABLE IF NOT EXISTS `master_instansi` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `kode_instansi` varchar(50) DEFAULT NULL,
  `nama_instansi` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `kode_instansi` (`kode_instansi`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=latin1;

-- Dumping data for table ortala.master_instansi: ~33 rows (approximately)
/*!40000 ALTER TABLE `master_instansi` DISABLE KEYS */;
INSERT INTO `master_instansi` (`id`, `kode_instansi`, `nama_instansi`) VALUES
	(1, 'IB001', 'BAPPEDA'),
	(2, 'IB002', 'BPKAD'),
	(3, 'IB003', 'BAPENDA'),
	(4, 'IB004', 'BKPSDMD'),
	(5, 'IB005', 'BALITBANG'),
	(7, 'ID002', 'DINAS KESEHATAN'),
	(8, 'ID003', 'DINAS PEKERJAAN UMUM'),
	(9, 'ID004', 'DINAS PENATAAN RUANG'),
	(10, 'ID005', 'DINAS PERUMAHAN DAN KAWASAN PERMUKIMAN'),
	(11, 'ID006', 'SATPOL PP'),
	(12, 'ID007', 'DINAS PEMADAM KEBAKARAN'),
	(13, 'ID008', 'DINAS SOSIAL'),
	(14, 'ID009', 'DINAS PEMBERDAYAAN PEREMPUAN'),
	(15, 'ID010', 'DINAS KETENAGAKERJAAN'),
	(16, 'ID011', 'DINAS KETAHANAN PANGAN'),
	(17, 'ID012', 'DINAS LINGKUNGAN HIDUP'),
	(18, 'ID013', 'DINAS KEPENDUDUKAN DAN CATATAN SIPIL'),
	(19, 'ID014', 'DINAS PENGENDALIAN PENDUDUK DAN KB'),
	(20, 'ID015', 'DINAS PERHUBUNGAN'),
	(21, 'ID016', 'DINAS KOMUNIKASI DAN INFORMATIKA'),
	(22, 'ID017', 'DINAS KOPERASI'),
	(23, 'ID018', 'DINAS PENANAMAN MODAL'),
	(24, 'ID019', 'DINAS PEMUDA DAN OLAHRAGA'),
	(25, 'ID020', 'DINAS KEBUDAYAAN'),
	(26, 'ID021', 'DINAS PERIKANAN'),
	(27, 'ID022', 'DINAS PARIWISATA'),
	(28, 'ID023', 'DINAS PERDAGANGAN'),
	(29, 'ID024', 'DINAS PERPUSTAKAAN'),
	(30, 'ID025', 'DINAS KEARSIPAN'),
	(31, 'ID026', 'DINAS PERTAHANAN'),
	(32, 'ISEKDA', 'SEKRETARIAT DAERAH'),
	(33, 'ISETWAN', 'SEKRETARIAT DEWAN'),
	(34, 'INSPEK', 'INSPEKTORAT');
/*!40000 ALTER TABLE `master_instansi` ENABLE KEYS */;

-- Dumping structure for table ortala.master_jabatan
CREATE TABLE IF NOT EXISTS `master_jabatan` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `kode_instansi` varchar(50) NOT NULL DEFAULT '0',
  `kode_jabatan` varchar(50) NOT NULL DEFAULT '0',
  `nama_jabatan` varchar(255) DEFAULT NULL,
  `tipe_jabatan` varchar(255) DEFAULT NULL,
  `peran_jabatan` text NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `kode_jabatan` (`kode_jabatan`),
  KEY `kode_instansi` (`kode_instansi`),
  CONSTRAINT `FK_master_jabatan_master_instansi` FOREIGN KEY (`kode_instansi`) REFERENCES `master_instansi` (`kode_instansi`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=200 DEFAULT CHARSET=latin1;

-- Dumping data for table ortala.master_jabatan: ~173 rows (approximately)
/*!40000 ALTER TABLE `master_jabatan` DISABLE KEYS */;
INSERT INTO `master_jabatan` (`id`, `kode_instansi`, `kode_jabatan`, `nama_jabatan`, `tipe_jabatan`, `peran_jabatan`) VALUES
	(1, 'IB001', 'KB1001', 'Kepala Badan', '0', 'Badan Perencanaan Pembangunan Daerah mempunyai tugas  membantu Walikota melaksanakan fungsi penunjang Urusan Pemerintahan bidang perencanaan yang menjadi kewenangan Daerah.'),
	(2, 'IB001', 'KB1002', 'Sekretariat', '0', 'Sekretariat mempunyai tugas melaksanakan koordinasi pelaksanaan tugas, pembinaan dan pelayanan administrasi kepada semua unit organisasi di lingkungan badan.'),
	(3, 'IB001', 'KB1003', 'Subbagian Perencanaan dan Pelaporan', '0', 'Subbagian Perencanaan dan Pelaporan mempunyai tugas melakukan penyiapan bahan koordinasi dan penyusunan rencana program kerja, monitoring dan evaluasi serta pelaporan pelaksanaan program dan kegiatan badan.'),
	(4, 'IB001', 'KB1004', 'Subbagian Keuangan', '', 'Subbagian Keuangan mempunyai tugas melakukan administrasi dan akuntansi keuangan.'),
	(5, 'IB001', 'KB1005', 'Subbagian Umum dan Kepegawaian', '', 'Subbagian Umum dan Kepegawaian mempunyai tugas melakukan urusan umum, penatausahaan surat menyurat, urusan rumah tangga, kehumasan, dokumentasi dan inventarisasi barang serta administrasi kepegawaian.'),
	(6, 'IB001', 'KB1006', 'Bidang Perencanaan dan Pengendalian', '', 'Bidang Perencanaan dan Pengendalian mempunyai tugas menyusun perencanaan meliputi perencanaan pembangunan kota jangka panjang, menengah dan pendek, Musyawarah Perencanaan Pembangunan, Kebijakan Umum Anggaran (KUA) dan Prioritas Plafon Anggaran Sementara (PPAS) serta Sistem Informasi Perencanaan Pembangunan Daerah (SIPPD), pengendalian meliputi monitoring Anggaran Pembangunan dan Belanja Daerah, Dana Alokasi Khusus, Tim pemonitor dan evaluasi, Sistem Informasi Monitoring dan Evaluasi, pelaporan meliputi Laporan Akuntabilitas Kinerja Instansi Pemerintah Kota, Laporan Keterangan Pertanggungjawaban, Perjanjian Kinerja, Refleksi Akhir Tahun, Sistem Informasi Pembangunan Daerah, Geografhic Information System (GIS).'),
	(7, 'IB001', 'KB1007', 'Subbagian Perencanaan Makro', '', 'Subbidang Perencanaan Makro mempunyai tugas mempersiapkan dan mengoordinasikan perencanaan pembangunan kota jangka panjang, menengah dan pendek, Musyawarah Perencanaan Pembangunan, Kebijakan Umum Anggaran (KUA) dan Prioritas Plafon Anggaran Sementara (PPAS) serta Sistem Informasi Perencanaan Pembangunan Daerah (SIPPD).'),
	(8, 'IB001', 'KB1008', 'Subbagian Pengendalian', '', 'Subbidang Pengendalian mempunyai tugas melakukan pemantauan dan pengendalian perencanaan dan pelaksanaan program/kegiatan pembangunan Daerah yang meliputi monitoring Anggaran Pendapatan dan Belanja Daerah, Dana Alokasi Khusus, Tim pemonitor dan evaluasi, pengendalian kebijakan Sistem Monitoring dan Evaluasi (SIMONEV).'),
	(9, 'IB001', 'KB1009', 'Subbagian Pelaporan', '', 'Subbidang Pelaporan mempunyai tugas membuat, menghimpun, mengolah, menganalisa dan menyajikan Laporan Akuntablitas Kinerja Instansi Pemerintah Kota, Laporan Keterangan Pertanggungjawaban, Perjanjian Kinerja, Refleksi Akhir Tahun, Sistem Informasi Pembangunan Daerah dan Geografhic Information System (GIS).'),
	(10, 'IB001', 'KB1010', 'Bidang Ekonomi dan Sumber Daya Alam', '', 'Bidang Ekonomi dan Sumber Daya Manusia mempunyai tugas menyusun rencana dan mengkoordinasikan penyusunan perencanaan di bidang ekonomi dan sumber daya alam meliputi bidang perindustrian, perdagangan, koperasi, usaha kecil dan menengah, tenaga kerja, keuangan, pendapatan, penanaman modal, pariwisata, pertanian, peternakan dan perikanan.'),
	(11, 'IB001', 'KB1011', 'Subbagian Perdagangan, Perindustrian dan Koperasi', '', 'Subbidang Perdagangan, Perindustrian dan Koperasi mempunyai tugas  menyusun rencana kerja, menyiapkan bahan, mengolah dan menganalisas program pembangunan meliputi bidang perindustrian, perdagangan, koperasi, usaha kecil dan menengah dan tenaga kerja.'),
	(12, 'IB001', 'KB1012', 'Subbagian Keuangan, Penanaman Modal dan Pariwisata', '', 'Subbidang Keuangan, Penanaman Modal dan Pariwisata mempunyai tugas menyusun rencana kerja, menyiapkan bahan, mengolah dan menganalisa program pembangunan meliputi bidang keuangan, pendapatan, penanaman modal dan pariwisata.'),
	(13, 'IB001', 'KB1013', 'Subbagian Pangan, Perikanan dan Pertanian', '', 'Subbidang Pangan, Perikanan dan Pertanian mempunyai tugas menyusun rencana kerja, menyiapkan bahan, mengolah dan menganalisas program pembangunan meliputi bidang pangan, pertanian, peternakan dan perikanan.'),
	(14, 'IB001', 'KB1014', 'Bidang Sosial, Budaya dan Pemerintahan Umum', '', 'Bidang Sosial, Budaya dan Pemerintahan Umum yang mempunyai tugas menyusun rencana kerja dan mengoordinasikan penyusunan perencanaan di bidang sosial budaya dan pemerintahan umum meliputi bidang pemberdayaan perempuan dan perlindungan anak, pengendalian penduduk dan keluarga berencana, pemberdayaan masyarakat, penanggulangan bencana, sosial, administrasi kependudukan dan pencatatan sipil, ketentraman dan ketertiban umum, kesatuan bangsa dan politik, sekretariat Dewan perwakilan Rakyat Daerah, sekretariat Daerah, pengawasan, kepegawaian, kebudayaan, kesehatan, pendidikan, pemuda dan olahraga, perpustakaan, dan kearsipan.'),
	(15, 'IB001', 'KB1015', 'Subbidang Kesejahteraan Rakyat', '', 'Subbidang Kesejahteraan Rakyat mempunyai tugas menyusun rencana kerja, menyiapkan bahan, mengolah dan menganalisas program pembangunan meliputi bidang pemberdayaan perempuan dan perlindungan anak, pengendalian penduduk dan keluarga berencana, pemberdayaan masyarakat, penanggulangan bencana, sosial, administrasi kependudukan dan pencatatan sipil.'),
	(16, 'IB001', 'KB1016', 'Subbidang Pemerintahan dan Aparatur', '', 'Subbidang Pemerintahan dan Aparatur mempunyai tugas menyusun rencana kerja, menyiapkan bahan, mengolah dan menganalisas program pembangunan meliputi bidang ketentraman dan ketertiban umum, perlindungan masyarakat, kesatuan bangsa dan politik, sekretariat Dewan Perwakilan Rakyat, sekretariat Daerah, pengawasan, dan kepegawaian.'),
	(17, 'IB001', 'KB1017', 'Subbidang Pendidikan, Kesehatan dan Kebudayaan', '', 'Subbidang Pendidikan, Kesehatan dan Kebudayaan mempunyai tugas menyusun rencana kerja, menyiapkan bahan, mengolah dan menganalisas program pembangunan meliputi bidang kebudayaan, kesehatan, pendidikan, pemuda dan olahraga, perpustakaan, dan kearsipan.'),
	(18, 'IB001', 'KB1018', 'Bidang Infrastruktur dan Pengembangan Wilayah', '', 'Bidang Infrastruktur dan Pengembangan Wilayah mempunyai tugas menyusun rencana kerja dan mengoordinasikan penyusunan perencanaan di bidang infrastruktur dan pengembangan wilayah meliputi bidang pekerjaan umum, pemukiman dan perumahan, pemadam kebakaran, komunikasi dan informatika, penelitian dan pengembangan, statistik, persandian dan perhubungan, penataan ruang, pertanahan, lingkungan hidup, dan kecamatan.'),
	(19, 'IB001', 'KB1019', 'Subbidang Infrastruktur', '', 'Subbidang Infrastruktur mempunyai tugas menyusun rencana kerja, menyiapkan bahan, mengolah dan menganalisas program pembangunan meliputi bidang pekerjaan umum, pemukiman dan perumahan, dan pemadam kebakaran.'),
	(20, 'IB001', 'KB1020', 'Subbidang Perhubungan dan Komunikasi', '', 'Subbidang Perhubungan dan Komunikasi mempunyai tugas menyusun rencana kerja, menyiapkan bahan, mengolah dan menganalisas program pembangunan meliputi bidang komunikasi dan informatika, penelitian dan pengembangan, statistik, persandian dan perhubungan.'),
	(21, 'IB001', 'KB1021', 'Subbidang Pengembangan Wilayah', '', 'Subbidang Pengembangan Wilayah mempunyai tugas menyusun rencana kerja, menyiapkan bahan, mengolah dan menganalisas program pembangunan meliputi bidang penataan ruang, pertanahan, lingkungan hidup, dan kecamatan.'),
	(22, 'IB001', 'KB1022', 'Kelompok Jabatan Fungsional', '', 'Kelompok Jabatan Fungsional mempunyai tugas melakukan kegiatan sesuai dengan jabatan fungsional masing-masing berdasarkan peraturan perundang-undangan yang berlaku.'),
	(23, 'IB001', 'KB1023', 'Unit Pelaksana Teknis', '', ''),
	(24, 'IB002', 'KB2001', 'Kepala Badan', '', ''),
	(25, 'IB002', 'KB2002', 'Sekretariat', '', ''),
	(26, 'IB002', 'KB2003', 'Subbagian Perencanaan dan Pelaporan', '', ''),
	(27, 'IB002', 'KB2004', 'Subbagian Keuangan', '', ''),
	(28, 'IB002', 'KB2005', 'Subbagian Umum dan Kepegawaian', '', ''),
	(29, 'IB002', 'KB2006', 'Bidang Anggaran', '', ''),
	(30, 'IB002', 'KB2007', 'Subbagian Perencanaan dan Penyusunan Anggaran', '', ''),
	(31, 'IB002', 'KB2008', 'Subbagian Pengendalian Anggaran', '', ''),
	(32, 'IB002', 'KB2009', 'Bidang Pebendaharaan', '', ''),
	(33, 'IB002', 'KB2010', 'Subbidang Pengelolaan Kas Daerah', '', ''),
	(34, 'IB002', 'KB2011', 'Subbidang Pebendaharaan', '', ''),
	(35, 'IB002', 'KB2012', 'Subbidang Pengelolaan Gaji', '', ''),
	(36, 'IB002', 'KB2013', 'Bidang Akuntansi', '', ''),
	(37, 'IB002', 'KB2014', 'Subbidang Pembukuan', '', ''),
	(38, 'IB002', 'KB2015', 'Subbidang Pelaporan', '', ''),
	(39, 'IB002', 'KB2016', 'Bidang Aset', '', ''),
	(40, 'IB002', 'KB2017', 'Subbidang Mutasi dan Inventarisasi Aset', '', ''),
	(41, 'IB002', 'KB2018', 'Subbidang Pengadaan dan Pemanfaatan Aset', '', ''),
	(42, 'IB002', 'KB2019', 'Kelompok Jabatan Fungsional', '', ''),
	(43, 'IB002', 'KB2020', 'Unit Pelaksana Teknis', '', ''),
	(44, 'IB003', 'KB3001', 'Kepala Badan', '', ''),
	(45, 'IB003', 'KB3002', 'Sekretariat', '', ''),
	(46, 'IB003', 'KB3003', 'Subbagian Perencanaan dan Pelaporan', '', ''),
	(47, 'IB003', 'KB3004', 'Subbagian Keuangan', '', ''),
	(48, 'IB003', 'KB3005', 'Subbagian Umum dan Kepegawaian', '', ''),
	(49, 'IB003', 'KB3006', 'Bidang Pendaftaran dan Pendataan', '', ''),
	(50, 'IB003', 'KB3007', 'Subbidang Pendataan Wilayah I', '', ''),
	(51, 'IB003', 'KB3008', 'Subbidang Pendataan Wilayah II', '', ''),
	(52, 'IB003', 'KB3009', 'Subbidang Pengolahan Data dan Informasi', '', ''),
	(53, 'IB003', 'KB3010', 'Bidang Pajak I dan Retribusi Daerah', '', ''),
	(54, 'IB003', 'KB3011', 'Subbidang Restoran, Minerba dan Burung Walet', '', ''),
	(55, 'IB003', 'KB3012', 'Subbidang Reklame, Parkir dan Retribusi Daerah', '', ''),
	(56, 'IB003', 'KB3013', 'Subbidang Penetapan, Pembukuan dan Pelaporan Pajak dan Retribusi Daerah', '', ''),
	(57, 'IB003', 'KB3014', 'Bidang Pajak Daerah II', '', ''),
	(58, 'IB003', 'KB3015', 'Subbidang Hotel dan Air Bawah Tanah', '', ''),
	(59, 'IB003', 'KB3016', 'Subbidang Hiburan dan Pajak Penerangan Jalan', '', ''),
	(60, 'IB003', 'KB3017', 'Subbidang Penetapan, Pembukuan dan Pelaporan Pajak', '', ''),
	(61, 'IB003', 'KB3018', 'Bidang Koordinasi, Pengawasan dan Perencanaan', '', ''),
	(62, 'IB003', 'KB3019', 'Subbidang Koordinasi, Perencanaan dan Regulasi', '', ''),
	(63, 'IB003', 'KB3020', 'Subbidang Penagihan Pajak Daerah dan Retribusi Daerah', '', ''),
	(64, 'IB003', 'KB3021', 'Subbidang Pembinaan, Pengawasan dan Penindakan', '', ''),
	(65, 'IB003', 'KB3022', 'Kelompok Jabatan Fungsional', '', ''),
	(66, 'IB003', 'KB3023', 'Unit Pelaksana Teknis', '', ''),
	(67, 'IB004', 'KB4001', 'Kepala Badan', '', ''),
	(68, 'IB004', 'KB4002', 'Sekretariat', '', ''),
	(69, 'IB004', 'KB4003', 'Subbagian Perencanaan dan Pelaporan', '', ''),
	(70, 'IB004', 'KB4004', 'Subbagian Keuangan', '', ''),
	(71, 'IB004', 'KB4005', 'Subbagian Umum dan Kepegawaian', '', ''),
	(72, 'IB004', 'KB4006', 'Bidang Pengadaan dan Informasi', '', ''),
	(73, 'IB004', 'KB4007', 'Subbidang Pengadaan dan Pengangkatan Aparatur Sipil Negara', '', ''),
	(74, 'IB004', 'KB4008', 'Subbidang Pengolahan Data dan Informasi Aparatur Sipil Negara', '', ''),
	(75, 'IB004', 'KB4009', 'Subbidang Fasilitasi Profesi Aparatur Sipil Negara', '', ''),
	(76, 'IB004', 'KB4010', 'Bidang Pengembangan Karier dan Hak-Hak Aparatur Sipil Negara', '', ''),
	(77, 'IB004', 'KB4011', 'Subbidang Kepangkatan, Jabatan dan Hak-Hak Aparatur Sipil Negara', '', ''),
	(78, 'IB004', 'KB4012', 'Subbidang Mutasi dan Promosi', '', ''),
	(79, 'IB004', 'KB4013', 'Subbidang Jaminan Pensiun dan Jaminan Hari Tua', '', ''),
	(80, 'IB004', 'KB4014', 'Bidang Diklat dan Pengembangan Kompetensi', '', ''),
	(81, 'IB004', 'KB4015', 'Subbidang Analisis Kebutuhan dan Kerjasama Diklat', '', ''),
	(82, 'IB004', 'KB4016', 'Subbidang Pendidikan dan Pelatihan Aparatur Sipil Negara', '', ''),
	(83, 'IB004', 'KB4017', 'Subbidang Pola Karier dan Pengembangan Kompetensi', '', ''),
	(84, 'IB004', 'KB4018', 'Bidang Kinerja dan Penghargaan', '', ''),
	(85, 'IB004', 'KB4019', 'Subbidang Penilaian Kinerja Aparatur Sipil Negara', '', ''),
	(86, 'IB004', 'KB4020', 'Subbidang Perlindungan, Penghentian dan Disiplin Aparatur Sipil Negara', '', ''),
	(87, 'IB004', 'KB4021', 'Subbidang Penghargaan Aparatur Sipil Negara', '', ''),
	(88, 'IB004', 'KB4022', 'Kelompok Jabatan Fungsional', '', ''),
	(89, 'IB004', 'KB4023', 'Unit Pelaksana Teknis', '', ''),
	(90, 'IB005', 'KB5001', 'Kepala Badan', '', ''),
	(91, 'IB005', 'KB5002', 'Sekretariat', '', ''),
	(92, 'IB005', 'KB5003', 'Subbidang Perencanaan dan Keuangan', '', ''),
	(93, 'IB005', 'KB5004', 'Subbidang Umum dan Kepegawaian', '', ''),
	(94, 'IB005', 'KB5005', 'Bidang Penelitian dan Pengembangan Kebijakan Daerah', '', ''),
	(95, 'IB005', 'KB5006', 'Subbidang Penelitian Kebijakan Daerah', '', ''),
	(96, 'IB005', 'KB5007', 'Subbidang Pengembangan Kebijakan Daerah', '', ''),
	(97, 'IB005', 'KB5008', 'Subbidang Dokumentasi, Data, Evaluasi dan Monitoring Kebijakan Daerah', '', ''),
	(98, 'IB005', 'KB5009', 'Bidang Penelitian dan Pengembangan Pembangunan', '', ''),
	(99, 'IB005', 'KB5010', 'Subbidang Penelitian dan Pengembangan Pembangunan Fisik', '', ''),
	(100, 'IB005', 'KB5011', 'Subbidang Penelitian dan Pengembangan Pembangunan Non Fisik', '', ''),
	(101, 'IB005', 'KB5012', 'Subbidang Dokumentasi, Data, Evaluasi dan Monitoring Pembangunan Daerah', '', ''),
	(102, 'IB005', 'KB5013', 'Bidang Pengembangan Inovasi dan Teknologi', '', ''),
	(103, 'IB005', 'KB5014', 'Subbidang Pengembangan Teknologi', '', ''),
	(104, 'IB005', 'KB5015', 'Subbidang Pengembangan Inovasi Daerah', '', ''),
	(105, 'IB005', 'KB5016', 'Subbidang Dokumentasi, Data, Evaluasi dan Monitoring Pengembangan Inovasi dan Teknologi', '', ''),
	(106, 'IB005', 'KB5017', 'Kelompok Jabatan Fungsional', '', ''),
	(107, 'IB005', 'KB5018', 'Unit Pelaksana Teknis', '', ''),
	(134, 'ID002', 'KD2004', 'Subbagian Keuangan', '', ''),
	(135, 'ID002', 'KD2005', 'Subbagian Umum dan Kepegawaian', '', ''),
	(136, 'ID002', 'KD2006', 'Bidang Kesehatan Masyarakat', '', ''),
	(137, 'ID002', 'KD2007', 'Seksi Kesehatan Keluarga dan Gizi', '', ''),
	(138, 'ID002', 'KD2008', 'Seksi Promosi Kesehatan dan Pemberdayaan Masyarakat', '', ''),
	(139, 'ID002', 'KD2009', 'Seksi Kesehatan Lingkungan, Kerja dan Olahraga', '', ''),
	(140, 'ID002', 'KD2010', 'Bidang Pencegahan dan Pengendalian Penyakit', '', ''),
	(141, 'ID002', 'KD2011', 'Seksi Surveilans dan Imunisiasi', '', ''),
	(142, 'ID002', 'KD2012', 'Seksi Pencegahan dan Pengendalian Penyakit Menular', '', ''),
	(143, 'ID002', 'KD2013', 'Seksi Pencegahan dan Pengendalian Penyakit Tidak Menular', '', ''),
	(144, 'ID002', 'KD2014', 'Bidang Pelayanan Kesehatan', '', ''),
	(145, 'ID002', 'KD2015', 'Seksi Pelayanan Kesehatan Primer dan Tradisional', '', ''),
	(146, 'ID002', 'KD2016', 'Seksi Pelayanan Kesehatan Rujukan', '', ''),
	(147, 'ID002', 'KD2017', 'Seksi Fasyankes dan Peningkatan Mutu', '', ''),
	(148, 'ID002', 'KD2018', 'Bidang Pengembangan Sumber Daya Kesehatan', '', ''),
	(149, 'ID002', 'KD2019', 'Seksi Kefarmasian', '', ''),
	(150, 'ID002', 'KD2020', 'Seksi Alat, Perbekalan dan Jaminan Kesehatan', '', ''),
	(151, 'ID002', 'KD2021', 'Seksi Sumber Daya Manusia dan Register Kesehatan', '', ''),
	(152, 'ID002', 'KD2022', 'Kelompok Jabatan Fungsional', '', ''),
	(153, 'ID002', 'KD2023', 'Unit Pelaksana Teknis', '', ''),
	(154, 'ID003', 'KD3001', 'Kepala Dinas', '', ''),
	(155, 'ID003', 'KD3002', 'Sekretariat', '', ''),
	(156, 'ID003', 'KD3003', 'Subbagian Perencanaan dan Pelaporan', '', ''),
	(157, 'ID003', 'KD3004', 'Subbagian Keuangan', '', ''),
	(158, 'ID003', 'KD3005', 'Subbagian Umum dan Kepegawaian', '', ''),
	(159, 'ID003', 'KD3006', 'Bidang Jalan dan Jembatan', '', ''),
	(160, 'ID003', 'KD3007', 'Seksi Pembangunan Jalan dan Jembatan', '', ''),
	(161, 'ID003', 'KD3008', 'Seksi Pemeliharaan Jalan dan Jembatan', '', ''),
	(162, 'ID003', 'KD3009', 'Seksi Jalan Lingkungan', '', ''),
	(163, 'ID003', 'KD3010', 'Bidang Prasarana dan Bangunan Pemerintah', '', ''),
	(164, 'ID003', 'KD3011', 'Seksi Sanitasi dan Air Bersih', '', ''),
	(165, 'ID003', 'KD3012', 'Seksi Pembangunan Gedung Pemerintah', '', ''),
	(166, 'ID003', 'KD3013', 'Seksi Pemeliharaan Gedung Pemerintah', '', ''),
	(167, 'ID003', 'KD3014', 'Bidang Pengelolaan Sumber Daya Air dan Drainase', '', ''),
	(168, 'ID003', 'KD3015', 'Seksi Pembangunan Sumber Daya Air dan Drainase', '', ''),
	(169, 'ID003', 'KD3016', 'Seksi Pemeliharaan Sumber Daya Air dan Drainase', '', ''),
	(170, 'ID003', 'KD3017', 'Seksi Pengendalian Sumber Daya Air dan Drainase', '', ''),
	(171, 'ID003', 'KD3018', 'Bidang Bina Teknik', '', ''),
	(172, 'ID003', 'KD3019', 'Seksi Perencanaan dan Desain', '', ''),
	(173, 'ID003', 'KD3020', 'Seksi Pengawasan dan Manajemen Konstruksi', '', ''),
	(174, 'ID003', 'KD3021', 'Seksi Pembinaan Jasa Konstruksi', '', ''),
	(175, 'ID003', 'KD3022', 'Kelompok Jabatan Fungsional', '', ''),
	(176, 'ID003', 'KD3023', 'Unit Pelaksana Teknis', '', ''),
	(177, 'ID004', 'KD4001', 'Kepala Dinas', '', ''),
	(178, 'ID004', 'KD4002', 'Sekretariat', '', ''),
	(179, 'ID004', 'KD4003', 'Subbagian Perencanaan dan Pelaporan', '', ''),
	(180, 'ID004', 'KD4004', 'Subbagian Keuangan', '', ''),
	(181, 'ID004', 'KD4005', 'Subbagian Umum dan Kepegawaian', '', ''),
	(182, 'ID004', 'KD4006', 'Bidang Tata Ruang', '', ''),
	(183, 'ID004', 'KD4007', 'Seksi Perencanaan Tata Ruang', '', ''),
	(184, 'ID004', 'KD4008', 'Seksi Pemanfaatan Ruang', '', ''),
	(185, 'ID004', 'KD4009', 'Seksi Pengendalian Ruang', '', ''),
	(186, 'ID004', 'KD4010', 'Bidang Pengembangan Tata Ruang dan Bangunan', '', ''),
	(187, 'ID004', 'KD4011', 'Seksi Pelayanan Informasi Tata Ruang dan Bangunan', '', ''),
	(188, 'ID004', 'KD4012', 'Seksi Pemetaan dan Pengembangan Sistem Data dan Informasi', '', ''),
	(189, 'ID004', 'KD4013', 'Seksi Dokumentasi dan Evaluasi', '', ''),
	(190, 'ID004', 'KD4014', 'Bidang Penataan Bangunan dan Lingkungan', '', ''),
	(191, 'ID004', 'KD4015', 'Seksi Perencanaan dan Desain Bangunan', '', ''),
	(192, 'ID004', 'KD4016', 'Seksi Kelayakan Bangunan', '', ''),
	(193, 'ID004', 'KD4017', 'Seksi Pengendalian Pemanfaatan Bangunan', '', ''),
	(194, 'ID004', 'KD4018', 'Bidang Penertiban Ruang dan Bangunan', '', ''),
	(195, 'ID004', 'KD4019', 'Seksi Pengaduan dan Pengawasan Ruang dan Bangunan', '', ''),
	(196, 'ID004', 'KD4020', 'Seksi Pengkajian Pelanggran Hukum Ruang dan Bangunan', '', ''),
	(197, 'ID004', 'KD4021', 'Seksi Penindakan Hukum Tata Ruang dan Bangunan', '', ''),
	(198, 'ID004', 'KD4022', 'Kelompok Jabatan Fungsional', '', ''),
	(199, 'ID004', 'KD4023', 'Unit Pelaksana Teknis', '', '');
/*!40000 ALTER TABLE `master_jabatan` ENABLE KEYS */;

-- Dumping structure for table ortala.master_jabatan_hasil_kerja
CREATE TABLE IF NOT EXISTS `master_jabatan_hasil_kerja` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `kode_jabatan` varchar(50) DEFAULT NULL,
  `detail_hk` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `kode_jabatan` (`kode_jabatan`),
  CONSTRAINT `FK_master_jabatan_hasil_kerja_master_jabatan` FOREIGN KEY (`kode_jabatan`) REFERENCES `master_jabatan` (`kode_jabatan`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=201 DEFAULT CHARSET=latin1;

-- Dumping data for table ortala.master_jabatan_hasil_kerja: ~0 rows (approximately)
/*!40000 ALTER TABLE `master_jabatan_hasil_kerja` DISABLE KEYS */;
/*!40000 ALTER TABLE `master_jabatan_hasil_kerja` ENABLE KEYS */;

-- Dumping structure for table ortala.master_jabatan_tanggung_jawab
CREATE TABLE IF NOT EXISTS `master_jabatan_tanggung_jawab` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `kode_jabatan` varchar(50) DEFAULT NULL,
  `detail_tj` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `kode_jabatan` (`kode_jabatan`),
  CONSTRAINT `FK_master_jabatan_tanggung_jawab_master_jabatan` FOREIGN KEY (`kode_jabatan`) REFERENCES `master_jabatan` (`kode_jabatan`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=200 DEFAULT CHARSET=latin1;

-- Dumping data for table ortala.master_jabatan_tanggung_jawab: ~0 rows (approximately)
/*!40000 ALTER TABLE `master_jabatan_tanggung_jawab` DISABLE KEYS */;
/*!40000 ALTER TABLE `master_jabatan_tanggung_jawab` ENABLE KEYS */;

-- Dumping structure for table ortala.master_jabatan_uraian
CREATE TABLE IF NOT EXISTS `master_jabatan_uraian` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `kode_jabatan` varchar(50) DEFAULT NULL,
  `detail_uraian` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `kode_jabatan` (`kode_jabatan`),
  CONSTRAINT `FK_master_jabatan_uraian_master_jabatan` FOREIGN KEY (`kode_jabatan`) REFERENCES `master_jabatan` (`kode_jabatan`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=414 DEFAULT CHARSET=latin1;

-- Dumping data for table ortala.master_jabatan_uraian: ~413 rows (approximately)
/*!40000 ALTER TABLE `master_jabatan_uraian` DISABLE KEYS */;
INSERT INTO `master_jabatan_uraian` (`id`, `kode_jabatan`, `detail_uraian`) VALUES
	(1, 'KB1001', 'a. merumuskan dan melaksanakan kebijakan di bidang perencanaan;'),
	(2, 'KB1001', 'b. merumuskan dan melaksanakan visi dan misi badan; '),
	(3, 'KB1001', 'c. merumuskan dan mengendalikan pelaksanaan program dan kegiatan Sekretariat dan Bidang Perencanaan dan Pengendalian, Bidang Ekonomi dan Sumber Daya Alam, Bidang Sosial, Budaya dan Pemerintahan Umum dan Bidang Infrastruktur dan Pengembangan Wilayah;'),
	(4, 'KB1001', 'd. merumuskan Rencana Strategis (RENSTRA) dan Rencana Kerja (RENJA), Indikator Kinerja Utama (IKU), Rencana Kerja dan Anggaran (RKA)/RKPA, Dokumen Pelaksanaan Anggaran (DPA)/DPPA dan Perjanjian Kinerja (PK) badan;'),
	(5, 'KB1001', 'e. mengoordinasikan dan mermuskan bahan penyiapan penyusunan Laporan Penyelenggaraan Pemerintahan Daerah (LPPD), Laporan Keterangan Pertanggungjawaban (LKPJ) dan Laporan Akuntabilitas Kinerja Instansi Pemerintah (LAKIP)/Sistem Akuntabilitas Kinerja Instansi Pemerintah (SAKIP) Kota dan segala bentuk pelaporan lainnya sesuai bidang tugasnya;'),
	(6, 'KB1001', 'f. merumuskan Laporan Akuntabilitas Kinerja Instansi Pemerintah (LAKIP)/Sistem Akuntabilitas Kinerja Instansi Pemerintah (SAKIP) badan;'),
	(7, 'KB1001', 'g. merumuskan Standar Operasional Prosedur (SOP) dan Standar Pelayanan (SP) badan;'),
	(8, 'KB1001', 'h. mengoordinasikan pembinaan dan pengembangan kapasitas organisasi dan tata laksana;'),
	(9, 'KB1001', 'i. menyusun Rencana Pembangunan Jangka Panjang Daerah (RPJPD) yang memuat Visi, misi dan arah pembangunan daerah;'),
	(10, 'KB1001', 'j. menyusun Rencana Pembangunan Jangka Menengah Daerah (RPJMD) yang memuat strategi pembangunan daerah, kebijakan umum, arah kebijakan keuangan daerah, program Satuan Kerja Perangkat Daerah, lintas Satuan Kerja Perangkat Daerah, kewilayahan dan lintas kewilayahan yang berisi kegiatan dalam kerangka regulasi dan kerangka anggaran;'),
	(11, 'KB1001', 'k. menyusun Rencana Kerja Pemerintah Daerah (RKPD) yang memuat Prioritas Pembangunan Daerah, Rancangan Kerangka Ekonomi Makro Daerah, Arah Kebijakan Keuangan Daerah, program Satuan Kerja Perangkat Daerah, lintas Satuan Kerja Perangkat Daerah, kewilayahan dan lintas kewilayahan yang berisi kegiatan dalam kerangka regulasi dan kerangka anggaran;'),
	(12, 'KB1001', 'l. mengoordinasikan perencanaan dan pelaksanaan pembangunan Daerah diantara Satuan Kerja Perangkat Daerah, lintas Satuan Kerja Perangkat Daerah, kewilayahan dan lintas kewilayahan;'),
	(13, 'KB1001', 'm. penyusunan rencana anggaran pokok dan perubahan Anggaran Pendapatan dan Belanja Daerah bersama-sama dengan unit kerja terkait, dengan koordinasi sekretaris Daerah;'),
	(14, 'KB1001', 'n. menilai dan mengendalikan pelaksanaan pembangunan;'),
	(15, 'KB1001', 'o. melaksanakan perencanaan dan pengendalian teknis operasional pengelolaan keuangan, kepegawaian dan pengurusan barang milik Daerah yang berada dalam penguasaannya;'),
	(16, 'KB1001', 'p. mengevaluasi pelaksanaan tugas dan menginventarisasi permasalahan di lingkup tugasnya serta mencari alternatif pemecahannya;'),
	(17, 'KB1001', 'q. mempelajari, memahami dan melaksanakan peraturan perundang-undangan yang berkaitan dengan lingkup tugasnya sebagai pedoman dalam melaksanakan tugas;'),
	(18, 'KB1001', 'r. memberikan saran dan pertimbangan teknis kepada pimpinan;'),
	(19, 'KB1001', 's. melaksanakan koordinasi dengan instansi terkait lainnya sesuai dengan lingkup tugasnya;'),
	(20, 'KB1001', 't. membina, membagi tugas, memberi petunjuk, menilai dan mengevaluasi hasil kerja bawahan agar pelaksanaan tugas dapat berjalan lancar sesuai dengan ketentuan yang berlaku;'),
	(21, 'KB1001', 'u. melaksanakan pembinaan jabatan fungsional;'),
	(22, 'KB1001', 'v. melaksanakan pembinaan unit pelaksana teknis;'),
	(23, 'KB1001', 'w. menyampaikan laporan hasil pelaksanaan tugas kepada walikota melalui sekretaris Daerah;'),
	(24, 'KB1001', 'x. melaksanakan tugas kedinasan lainnya yang diberikan oleh walikota.'),
	(25, 'KB1002', 'a. merencanakan, menyusun dan melaksanakan program dan kegiatan  Sekretariat;'),
	(26, 'KB1002', 'b. melaksanakan penyusunan kebijakan teknis urusan perencanaan dan pelaporan, keuangan, umum dan kepegawaian;'),
	(27, 'KB1002', 'c. mengoordinasikan pelaksanaan tugas Subbagian Perencanaan dan Pelaporan, Subbagian Keuangan dan Subbagian Umum dan Kepegawaian;'),
	(28, 'KB1002', 'd. menghimpun dan menyusun bahan Rencana Kerja dan Anggaran (RKA)/RKPA, Dokumen Pelaksanaan Anggaran (DPA)/DPPA Sekretariat;'),
	(29, 'KB1002', 'e. mengoordinasikan, mengawasi dan mengendalikan pelaksanaan Dokumen Pelaksanaan Anggaran (DPA)/DPPA Sekretariat;'),
	(30, 'KB1002', 'f. mengoordinasikan setiap bidang dalam penyusunan Rencana Strategis (RENSTRA) dan Rencana Kerja (RENJA), Indikator Kinerja Utama (IKU), Rencana Kerja dan Anggaran (RKA)/RKPA, Dokumen Pelaksanaan Anggaran (DPA)/DPPA dan Perjanjian Kinerja (PK), Laporan Akuntabilitas Kinerja Instansi Pemerintah (LAKIP)/Sistem Akuntabilitas Kinerja Instansi Pemerintah (SAKIP) Badan;'),
	(31, 'KB1002', 'g. mengoordinasikan setiap bidang dalam penyiapan bahan penyusunan Laporan Penyelenggaraan Pemerintahan Daerah (LPPD), Laporan Keterangan Pertanggungjawaban (LKPJ) dan Laporan Akuntabilitas Kinerja Instansi Pemerintah (LAKIP)/Sistem Akuntabilitas Kinerja Instansi Pemerintah (SAKIP) kota dan segala bentuk pelaporan lainnya sesuai bidang tugasnya;'),
	(32, 'KB1002', 'h. mengoordinasikan setiap bidang dalam penyusunan Standar Operasional Prosedur (SOP) dan Standar Pelayanan (SP) badan;'),
	(33, 'KB1002', 'i. mengoordinasikan setiap bidang dalam pembinaan dan pengembangan kapasitas organisasi dan tata laksana;'),
	(34, 'KB1002', 'j. mengoordinasikan penyelenggaraan urusan ketatausahaan, administrasi kepegawaian, administrasi keuangan dan aset serta urusan kehumasan, dokumentasi dan protokoler badan;'),
	(35, 'KB1002', 'k. mengevaluasi pelaksanaan tugas dan menginventarisasi permasalahan di lingkup tugasnya serta mencari alternatif pemecahannya;'),
	(36, 'KB1002', 'l. mempelajari, memahami dan melaksanakan peraturan perundang-undangan yang berkaitan dengan lingkup tugasnya sebagai pedoman dalam melaksanakan tugas;'),
	(37, 'KB1002', 'm. memberikan saran dan pertimbangan teknis kepada atasan;'),
	(38, 'KB1002', 'n. melaksanakan pembinaan disiplin aparatur sipil negara di lingkup badan;'),
	(39, 'KB1002', 'o. membagi tugas, memberi petunjuk, menilai dan mengevaluasi hasil kerja bawahan agar pelaksanaan tugas dapat berjalan lancar sesuai dengan ketentuan yang berlaku;'),
	(40, 'KB1002', 'p. menyampaikan laporan pelaksanaan tugas dan/atau kegiatan kepada atasan;'),
	(41, 'KB1002', 'q. melaksanakan tugas kedinasan lainnya yang diberikan oleh atasan.'),
	(42, 'KB1003', 'a. merencanakan, menyusun dan melaksanakan program dan kegiatan  Subbagian Perencanaan dan Pelaporan;'),
	(43, 'KB1003', 'b. menyusun bahan Rencana Kerja dan Anggaran (RKA)/RKPA, Dokumen Pelaksanaan Anggaran (DPA)/DPPA Subbagian Perencanaan dan Pelaporan;'),
	(44, 'KB1003', 'c. melaksanakan Dokumen Pelaksanaan Anggaran (DPA)/DPPA Subbagian Perencanaan dan Pelaporan;'),
	(45, 'KB1003', 'd. menghimpun bahan dan menyusun Rencana Strategis (RENSTRA) dan Rencana Kerja (RENJA), Indikator Kinerja Utama (IKU), Rencana Kerja dan Anggaran (RKA)/RKPA, Dokumen Pelaksanaan Anggaran (DPA)/DPPA dan Perjanjian Kinerja (PK) Badan;'),
	(46, 'KB1003', 'e. menghimpun bahan dan menyusun Laporan Akuntabilitas Kinerja Instansi Pemerintah (LAKIP)/Sistem Akuntabilitas Kinerja Instansi Pemerintah (SAKIP) Badan;'),
	(47, 'KB1003', 'f. menyiapkan bahan penyusunan Laporan Penyelenggaraan Pemerintahan Daerah (LPPD), Laporan Keterangan Pertanggungjawaban (LKPJ) dan Laporan Akuntabilitas Kinerja Instansi Pemerintah (LAKIP)/Sistem Akuntabilitas Kinerja Instansi Pemerintah (SAKIP) Kota dan segala bentuk pelaporan lainnya sesuai bidang tugasnya;'),
	(48, 'KB1003', 'g. menghimpun, memaduserasikan dan menyiapkan bahan Rencana Kerja dan Anggaran (RKA)/RKPA, Dokumen Pelaksanaan Anggaran (DPA)/DPPA dari setiap bidang untuk dikoordinasikan dengan SKPD terkait;'),
	(49, 'KB1003', 'h. menghimpun dan menganalisa data pelaporan kegiatan dari setiap bidang sebagai bahan evaluasi;'),
	(50, 'KB1003', 'i. mengevaluasi pelaksanaan tugas dan menginventarisasi permasalahan dilingkup tugasnya serta mencari alternatif pemecahannya;'),
	(51, 'KB1003', 'j. mempelajari, memahami dan melaksanakan peraturan perundang-undangan yang berkaitan dengan lingkup tugasnya sebagai pedoman dalam melaksanakan tugas;'),
	(52, 'KB1003', 'k. memberikan saran dan pertimbangan teknis kepada atasan;'),
	(53, 'KB1003', 'l. membagi tugas, memberi petunjuk, menilai dan mengevaluasi hasil kerja bawahan agar pelaksanaan tugas dapat berjalan lancar sesuai dengan ketentuan yang berlaku;'),
	(54, 'KB1003', 'm. menyampaikan laporan pelaksanaan tugas dan/atau kegiatan kepada atasan;'),
	(55, 'KB1003', 'n. melaksanakan tugas kedinasan lain yang diberikan oleh atasan.'),
	(56, 'KB1004', 'a. merencanakan, menyusun dan melaksanakan program dan kegiatan Subbagian Keuangan;'),
	(57, 'KB1004', 'b. menyusun bahan Rencana Kerja dan Anggaran (RKA)/RKPA, Dokumen Pelaksanaan Anggaran (DPA)/DPPA Subbagian Keuangan;'),
	(58, 'KB1004', 'c. melaksanakan Dokumen Pelaksanaan Anggaran (DPA)/DPPA Subbagian Keuangan;'),
	(59, 'KB1004', 'd. melaksanakan kegiatan administrasi dan akuntansi keuangan di lingkup badan  sesuai dengan peraturan perundang-undangan yang berlaku;'),
	(60, 'KB1004', 'e. meneliti dan memverifikasi kelengkapan Surat Perintah Pembayaran (SPP) dan dokumen pencairan anggaran lainnya sesuai dengan ketentuan peraturan perundang-undangan yang berlaku;'),
	(61, 'KB1004', 'f. menyiapkan dan menerbitkan Surat Perintah Membayar (SPM) lingkup badan;'),
	(62, 'KB1004', 'g. menyusun segala bentuk pelaporan keuangan lingkup badan sesuai dengan peraturan perundang-undangan yang berlaku;'),
	(63, 'KB1004', 'h. mengevaluasi pelaksanaan tugas dan menginventarisasi permasalahan dilingkup tugasnya serta mencari alternatif pemecahannya;'),
	(64, 'KB1004', 'i. mempelajari, memahami dan melaksanakan peraturan perundang-undangan yang berkaitan dengan lingkup tugasnya sebagai pedoman dalam melaksanakan tugas;'),
	(65, 'KB1004', 'j. memberikan saran dan pertimbangan teknis kepada atasan;'),
	(66, 'KB1004', 'k. membagi tugas, memberi petunjuk, menilai dan mengevaluasi hasil kerja bawahan agar pelaksanaan tugas dapat berjalan lancar sesuai dengan ketentuan yang berlaku;'),
	(67, 'KB1004', 'l. menyampaikan laporan pelaksanaan tugas dan/atau kegiatan kepada atasan;'),
	(68, 'KB1004', 'm. melaksanakan tugas kedinasan lain yang diberikan oleh atasan.'),
	(69, 'KB1005', 'a.     merencanakan, menyusun dan melaksanakan program dan kegiatan Subbagian Umum dan Kepegawaian;'),
	(70, 'KB1005', 'b.     menyusun bahan Rencana Kerja dan Anggaran (RKA)/RKPA, Dokumen Pelaksanaan Anggaran (DPA)/DPPA Subbagian Umum dan Kepegawaian;'),
	(71, 'KB1005', 'c.     melaksanakan Dokumen Pelaksanaan Anggaran (DPA)/DPPA Subbagian Umum dan Kepegawaian;'),
	(72, 'KB1005', 'd.     mengatur administrasi dan pelaksanaan surat masuk dan surat keluar sesuai dengan tata naskah dinas yang berlaku;'),
	(73, 'KB1005', 'e.     melaksanakan urusan administrasi kepegawaian di lingkup badan;'),
	(74, 'KB1005', 'f.      meminta dan menganalisa rencana kebutuhan barang unit dari setiap bidang;'),
	(75, 'KB1005', 'g.     membuat daftar kebutuhan barang dan rencana tahunan barang unit;'),
	(76, 'KB1005', 'h.    menyusun kebutuhan biaya pemeliharaan barang;'),
	(77, 'KB1005', 'i.      melaksanakan pengadaan, pemeliharaan dan pendistribusian barang di lingkup badan;'),
	(78, 'KB1005', 'j.      melakukan penyimpanan dokumen dan surat berharga lainnya tentang barang inventaris Daerah;'),
	(79, 'KB1005', 'k.     melaksanakan tugas kehumasan dan protokoler badan;'),
	(80, 'KB1005', 'l.      menghimpun bahan dan menyusun Standar Operasional Prosedur (SOP) dan Standar Pelayanan (SP) badan;'),
	(81, 'KB1005', 'm.   menyiapkan bahan pembinaan dan pengembangan kapasitas organisasi dan tata laksana;'),
	(82, 'KB1005', 'n.    mengevaluasi pelaksanaan tugas dan menginventarisasi permasalahan dilingkup tugasnya serta mencari alternatif pemecahannya;'),
	(83, 'KB1005', 'o.     mempelajari, memahami dan melaksanakan peraturan perundang-undangan yang berkaitan dengan lingkup tugasnya sebagai pedoman dalam melaksanakan tugas;'),
	(84, 'KB1005', 'p.     memberikan saran dan pertimbangan teknis kepada atasan;'),
	(85, 'KB1005', 'q.     membagi tugas, memberi petunjuk, menilai dan mengevaluasi hasil kerja bawahan agar pelaksanaan tugas dapat berjalan lancar sesuai dengan ketentuan yang berlaku;'),
	(86, 'KB1005', 'r.      menyampaikan laporan pelaksanaan tugas dan/atau kegiatan kepada atasan;'),
	(87, 'KB1005', 's.  melaksanakan tugas kedinasan lain yang diberikan oleh atasan.'),
	(88, 'KB1006', 'a.     merencanakan, menyusun dan melaksanakan program dan kegiatan Bidang Perencanaan dan Pengendalian;'),
	(89, 'KB1006', 'b.     menghimpun dan menyusun bahan Rencana Kerja dan Anggaran (RKA)/RKPA, Dokumen Pelaksanaan Anggaran (DPA)/DPPA Bidang Perencanaan dan Pengendalian;'),
	(90, 'KB1006', 'c.     mengoordinasikan, mengawasi dan mengendalikan pelaksanaan Dokumen Pelaksanaan Anggaran (DPA)/DPPA Bidang Perencanaan dan Pengendalian;'),
	(91, 'KB1006', 'd.     mengoordinasikan penyusunan pancangan RPJPD, RPJMD dan RKPD Kota Makassar;'),
	(92, 'KB1006', 'e.     memverifikasi rancangan Rencana Strategis Satuan Kerja Perangkat Daerah Kota Makassar;'),
	(93, 'KB1006', 'f.      mengoordinasikan pelaksanaan Musrenbang RPJPD, RPJMD dan RKPD Kota Makassar;'),
	(94, 'KB1006', 'g.     mengoordinasikan pelaksanaan sinergitas dan harmonisasi RTRW Daerah dan RPJMD Kota Makassar;'),
	(95, 'KB1006', 'h.    mengoordinasikan pelaksanaan kesepakatan dengan DPRD terkait RPJPD, RPJMD, RKPD Kota Makassar;'),
	(96, 'KB1006', 'i.      mengoordinasikan pelaksanaan kesepakatan dengan DPRD terkait APBD Kota Makassar;'),
	(97, 'KB1006', 'j.      mengoordinasikan sinergitas dan harmonisasi kegiatan Satuan Kerja Perangkat Daerah Kota Makassar;'),
	(98, 'KB1006', 'k.     mengoordinasikan pelaksanaan sinergitas dan harmonisasi kegiatan Kementrian/Lembaga, Provinsi di Pemerintah Kota Makassar;'),
	(99, 'KB1006', 'l.      mengoordinasikan pembinaan teknis perencanaan kepada Satuan Kerja Perangkat Daerah lingkup Pemerintah Kota Makassar;'),
	(100, 'KB1006', 'm.   melaksanakan pengendalian/monitoring pelaksanaan perencanaan pembangunan daerah lingkup Pemerintah Kota Makassar;'),
	(101, 'KB1006', 'n.    melaksanakan pengelolaan data dan informasi perencanaan pembangunan daerah lingkup Pemerintah Kota Makassar;   '),
	(102, 'KB1006', 'o.     melaksanakan evaluasi dan pelaporan atas pelaksanaan perencanaan pembangunan daerah Pemerintah Kota Makassar;'),
	(103, 'KB1006', 'p.     mengevaluasi pelaksanaan tugas dan menginventarisasi permasalahan di lingkup tugasnya serta mencari alternatif pemecahannya;'),
	(104, 'KB1006', 'q.     mempelajari, memahami dan melaksanakan peraturan perundang-undangan yang berkaitan dengan lingkup tugasnya sebagai pedoman dalam melaksanakan tugas;'),
	(105, 'KB1006', 'r.      memberikan saran dan pertimbangan teknis kepada atasan;'),
	(106, 'KB1006', 's.     membagi tugas, memberi petunjuk, menilai dan mengevaluasi hasil kerja bawahan agar pelaksanaan tugas dapat berjalan lancar sesuai dengan ketentuan yang berlaku;'),
	(107, 'KB1006', 't.      menyampaikan laporan pelaksanaan tugas dan/atau kegiatan kepada atasan;'),
	(108, 'KB1006', 'u.  melaksanakan tugas kedinasan lainnya yang diberikan oleh atasan.'),
	(109, 'KB1007', 'a.     merencanakan, menyusun dan melaksanakan program dan kegiatan Subbidang Perencanaan Makro;'),
	(110, 'KB1007', 'b.     menyusun bahan Rencana Kerja dan Anggaran (RKA)/RKPA, Dokumen Pelaksanaan Anggaran (DPA)/DPPA Subbidang Perencanaan Makro;'),
	(111, 'KB1007', 'c.     melaksanakan Dokumen Pelaksanaan Anggaran (DPA)/DPPA Subbidang Perencanaan Makro;'),
	(112, 'KB1007', 'd.     merancang penyusunan rancangan RPJPD, RPJMD, Musrenbang, RKPD, Kebijakan Umum Anggaran (KUA) dan Prioritas Plafon Anggaran Sementara (PPAS) serta Sistem Informasi Perencanaan Pembangunan Daerah (SIPPD);'),
	(113, 'KB1007', 'e.     menganalisis rancangan Renstra Satuan Kerja Perangkat Daerah Kota Makassar;'),
	(114, 'KB1007', 'f.      menyiapkan pelaksanaan Musrenbang RPJPD Kota Makassar;'),
	(115, 'KB1007', 'g.     merencanakan pelaksanaan sinergitas dan harmonisasi RTRW Daerah dan RPJMD Kota Makassar;'),
	(116, 'KB1007', 'h.    membuat konsep pembinaan teknis perencanaan kepada Satuan kerja Perangkat Daerah Kota Makassar;'),
	(117, 'KB1007', 'i.      merencanakan pelaksanaan kesepakatan dengan DPRD terkait RPJPD, RPJMD, RKPD Pemerintah Kota Makassar;'),
	(118, 'KB1007', 'j.      merencanakan pelaksanaan kesepakatan dengan DPRD terkait APBD Kota Makassar;'),
	(119, 'KB1007', 'k.     merencanakan sinergitas dan harmonisasi kegiatan Satuan Kerja Perangkat Daerah Kota Makassar;'),
	(120, 'KB1007', 'l.      merencanakan dukungan pelaksanaa kegiatan Pusat, Provinsi dengan Pemerintah Kota Makassar;'),
	(121, 'KB1007', 'm.   mengevaluasi pelaksanaan tugas dan menginventarisasi permasalahan dilingkup tugasnya serta mencari alternatif pemecahannya;'),
	(122, 'KB1007', 'n.    mempelajari, memahami dan melaksanakan peraturan perundang-undangan yang berkaitan dengan lingkup tugasnya sebagai pedoman dalam melaksanakan tugas;'),
	(123, 'KB1007', 'o.     memberikan saran dan pertimbangan teknis kepada atasan;'),
	(124, 'KB1007', 'p.     membagi tugas, memberi petunjuk, menilai dan mengevaluasi hasil kerja bawahan agar pelaksanaan tugas dapat berjalan lancar sesuai dengan ketentuan yang berlaku;'),
	(125, 'KB1007', 'q.     menyampaikan laporan pelaksanaan tugas dan/atau kegiatan kepada atasan;'),
	(126, 'KB1007', 'r.  melaksanakan tugas kedinasan lain yang diberikan oleh atasan.'),
	(127, 'KB1008', 'a.     merencanakan, menyusun dan melaksanakan program dan kegiatan Subbidang Pengendalian;'),
	(128, 'KB1008', 'b.     menyusun bahan Rencana Kerja dan Anggaran (RKA)/RKPA, Dokumen Pelaksanaan Anggaran (DPA)/DPPA Subbidang Pengendalian;'),
	(129, 'KB1008', 'c.     melaksanakan Dokumen Pelaksanaan Anggaran (DPA)/DPPA Subbidang Pengendalian;'),
	(130, 'KB1008', 'd.     mengoordinasikan dan menyusun rencana kerja perjanjian kinerja Kota Makassar;'),
	(131, 'KB1008', 'e.     melakukan pengendalian/monitoring pelaksanaan perencanaan pembangunan daerah Kota Makassar;'),
	(132, 'KB1008', 'f.      mengumpulkan data program/kegiatan menurut sektor pembangunan Kota Makassar;'),
	(133, 'KB1008', 'g.     melakukan koordinasi dengan unit kerja lain yang berkaitan dengan bidang tugasnya;'),
	(134, 'KB1008', 'h.    mengevaluasi pelaksanaan tugas dan menginventarisasi permasalahan dilingkup tugasnya serta mencari alternatif pemecahannya;'),
	(135, 'KB1008', 'i.      mempelajari, memahami dan melaksanakan peraturan perundang-undangan yang berkaitan dengan lingkup tugasnya sebagai pedoman dalam melaksanakan tugas;'),
	(136, 'KB1008', 'j.      memberikan saran dan pertimbangan teknis kepada atasan;'),
	(137, 'KB1008', 'k.     membagi tugas, memberi petunjuk, menilai dan mengevaluasi hasil kerja bawahan agar pelaksanaan tugas dapat berjalan lancar sesuai dengan ketentuan yang berlaku;'),
	(138, 'KB1008', 'l.      menyampaikan laporan pelaksanaan tugas dan/atau kegiatan kepada atasan;'),
	(139, 'KB1008', 'm. melaksanakan tugas kedinasan lain yang diberikan oleh atasan.'),
	(140, 'KB1009', 'a.     merencanakan, menyusun dan melaksanakan program dan kegiatan Subbidang Pelaporan;'),
	(141, 'KB1009', 'b.     menyusun bahan Rencana Kerja dan Anggaran (RKA)/RKPA, Dokumen Pelaksanaan Anggaran (DPA)/DPPA Subbidang Pelaporan;'),
	(142, 'KB1009', 'c.     melaksanakan Dokumen Pelaksanaan Anggaran (DPA)/DPPA Subbidang Pelaporan;'),
	(143, 'KB1009', 'd.     menyiapkan bahan penyusnan Laporan Akuntablitas Kinerja Instansi Pemerintah Kota, Laporan Keterangan Pertanggungjawaban, Perjanjian Kinerja Kota Makassar, Refleksi Akhir Tahun, Sistem Informasi Pembangunan Daerah, Geografhic Information System (GIS);'),
	(144, 'KB1009', 'e.     menghimpun data dan informasi berupa data primer maupun data sekunder;'),
	(145, 'KB1009', 'f.      mengolah data dan informasi dengan metode statistik;'),
	(146, 'KB1009', 'g.     merencanakan dan menyusun evaluasi dan pelaporan atas pelaksanaan perencanaan pembangunan daerah Pemerintah Kota Makassar;'),
	(147, 'KB1009', 'h.    menyusun laporan pelaksanaan program/kegiatan untuk 1 bulan, 3 bulan dan tahunan se Kota Makassar;'),
	(148, 'KB1009', 'i.      mengevaluasi pelaksanaan pembangunan sesuai tingkat kemajuan fisik dan keuangan serta permasalahan yang dihadapi;'),
	(149, 'KB1009', 'j.      membuat pelaporan pelaksanaan pembangunan sesuai tingkat kemajuan fisik dan keuangan serta permasalahan yang dihadapi;'),
	(150, 'KB1009', 'k.     menganalisis data dan informasi sesuai standar perencanaan;'),
	(151, 'KB1009', 'l.      menyajikan hasil analisis dalam bentuk grafik, diagram atau dalam bentuk analisa kualitatif;'),
	(152, 'KB1009', 'm.   mengembangkan sistem informasi pelaporan;'),
	(153, 'KB1009', 'n.    melakukan koordinasi dengan unit kerja lain yang berkaitan dengan bidang tugasnya;'),
	(154, 'KB1009', 'o.     mengevaluasi pelaksanaan tugas dan menginventarisasi permasalahan dilingkup tugasnya serta mencari alternatif pemecahannya;'),
	(155, 'KB1009', 'p.     mempelajari, memahami dan melaksanakan peraturan perundang-undangan yang berkaitan dengan lingkup tugasnya sebagai pedoman dalam melaksanakan tugas;'),
	(156, 'KB1009', 'q.     memberikan saran dan pertimbangan teknis kepada atasan;'),
	(157, 'KB1009', 'r.      membagi tugas, memberi petunjuk, menilai dan mengevaluasi hasil kerja bawahan agar pelaksanaan tugas dapat berjalan lancar sesuai dengan ketentuan yang berlaku;'),
	(158, 'KB1009', 's.     menyampaikan laporan pelaksanaan tugas dan/atau kegiatan kepada atasan;'),
	(159, 'KB1009', 'r.  melaksanakan tugas kedinasan lain yang diberikan oleh atasan.'),
	(160, 'KB1010', 'a.     merencanakan, menyusun dan melaksanakan program dan kegiatan Bidang Ekonomi dan Sumber Daya Manusia;'),
	(161, 'KB1010', 'b.     menghimpun dan menyusun bahan Rencana Kerja dan Anggaran (RKA)/RKPA, Dokumen Pelaksanaan Anggaran (DPA)/DPPA Bidang Ekonomi dan Sumber Daya Manusia;'),
	(162, 'KB1010', 'c.     mengoordinasikan, mengawasi dan mengendalikan pelaksanaan Dokumen Pelaksanaan Anggaran (DPA)/DPPA Bidang Ekonomi dan Sumber Daya Manusia;'),
	(163, 'KB1010', 'd.     mengoordinasikan Penyusunan Rancangan RPJPD, RPJMD dan RKPD di bidang ekonomi dan sumber daya alam;'),
	(164, 'KB1010', 'e.     memverifikasi rancangan Renstra Satuan Kerja Perangkat Daerah di bidang ekonomi dan sumber daya alam;'),
	(165, 'KB1010', 'f.      mengoordinasikan pelaksanaan Musrenbang RPJPD, RPJMD, RKPD di bidang ekonomi dan sumber daya alam;'),
	(166, 'KB1010', 'g.     mengoordinasikan pelaksanaan sinergitas dan harmonisasi RTRW Daerah dan RPJMD di bidang ekonomi dan sumber daya alam;'),
	(167, 'KB1010', 'h.    mengoordinasikan pelaksanaan kesepakatan dengan DPRD terkait RPJPD, RPJMD, RKPD di bidang ekonomi dan sumber daya alam;'),
	(168, 'KB1010', 'i.      mengoordinasikan pelaksanaan kesepakatan dengan DPRD terkait APBD di bidang ekonomi dan sumber daya alam;'),
	(169, 'KB1010', 'j.      mengoordinasikan sinergitas dan harmonisasi kegiatan Satuan Kerja Perangkat Daerah di bidang ekonomi dan sumber daya alam;'),
	(170, 'KB1010', 'k.     mengoordinasikan pelaksanaan sinergitas dan harmonisasi kegiatan Kementrian/Lembaga, Provinsi di bidang ekonomi dan sumber daya alam;'),
	(171, 'KB1010', 'l.      mengoordinasikan Pembinaan teknis perencanaan kepada Satuan Kerja Perangkat Daerah di bidang ekonomi dan sumber daya alam;'),
	(172, 'KB1010', 'm.   melaksanakan pengendalian/monitoring pelaksanaan perencanaan pembangunan daerah di bidang ekonomi dan sumber daya alam;'),
	(173, 'KB1010', 'n.    melaksanakan Pengelolaan Data dan informasi perencanaan pembangunan daerah di bidang ekonomi dan sumber daya alam;   '),
	(174, 'KB1010', 'o.     melaksanakan evaluasi dan pelaporan atas pelaksanaan perencanaan pembangunan daerah di bidang ekonomi dan sumber daya alam;'),
	(175, 'KB1010', 'p.     mengevaluasi pelaksanaan tugas dan menginventarisasi permasalahan di lingkup tugasnya serta mencari alternatif pemecahannya;'),
	(176, 'KB1010', 'q.     mempelajari, memahami dan melaksanakan peraturan perundang-undangan yang berkaitan dengan lingkup tugasnya sebagai pedoman dalam melaksanakan tugas;'),
	(177, 'KB1010', 'r.      memberikan saran dan pertimbangan teknis kepada atasan;'),
	(178, 'KB1010', 's.     membagi tugas, memberi petunjuk, menilai dan mengevaluasi hasil kerja bawahan agar pelaksanaan tugas dapat berjalan lancar sesuai dengan ketentuan yang berlaku;'),
	(179, 'KB1010', 't.      menyampaikan laporan pelaksanaan tugas dan/atau kegiatan kepada atasan;'),
	(180, 'KB1010', 'u.  melaksanakan tugas kedinasan lainnya yang diberikan oleh atasan.'),
	(181, 'KB1011', 'a.        merencanakan, menyusun dan melaksanakan program dan kegiatan Subbidang Perdagangan, Perindustrian dan Koperasi;'),
	(182, 'KB1011', 'b.        menyusun bahan Rencana Kerja dan Anggaran (RKA)/RKPA, Dokumen Pelaksanaan Anggaran (DPA)/DPPA Subbidang Perdagangan, Perindustrian dan Koperasi;'),
	(183, 'KB1011', 'c.         melaksanakan Dokumen Pelaksanaan Anggaran (DPA)/DPPA Subbidang Perdagangan, Perindustrian dan Koperasi;'),
	(184, 'KB1011', 'd.        merancang penyusun rancangan RPJPD, RPJMD, RKPD meliputi bidang perindustrian, perdagangan, koperasi, usaha kecil dan menengah, tenaga kerja;'),
	(185, 'KB1011', 'e.         menganalisis rancangan Renstra Satuan Kerja Perangkat Daerah meliputi bidang perindustrian, perdagangan, koperasi, usaha kecil dan menengah, tenaga kerja;'),
	(186, 'KB1011', 'f.          menyiapkan Pelaksanaan Musrenbang RPJPD meliputi bidang perindustrian, perdagangan, koperasi, usaha kecil dan menengah, tenaga kerja;'),
	(187, 'KB1011', 'g.        merencanakan pelaksanaan Sinergitas dan Harmonisasi RTRW Daerah dan RPJMD meliputi bidang perindustrian, perdagangan, koperasi, usaha kecil dan menengah, tenaga kerja;'),
	(188, 'KB1011', 'h.        membuat konsep pembinaan teknis perencanaan kepada Satuan kerja Perangkat Daerah meliputi bidang perindustrian, perdagangan, koperasi, usaha kecil dan menengah, tenaga kerja;'),
	(189, 'KB1011', 'i.          merencanakan pelaksanaan kesepakatan dengan DPRD terkait RPJPD, RPJMD, RKPD meliputi bidang perindustrian, perdagangan, koperasi, usaha kecil dan menengah, tenaga kerja;'),
	(190, 'KB1011', 'j.          merencanakan pelaksanaan kesepakatan dengan DPRD terkait APBD meliputi bidang perindustrian, perdagangan, koperasi, usaha kecil dan menengah, tenaga kerja;'),
	(191, 'KB1011', 'k.        merencanakan sinergitas dan harmonisasi kegiatan Satuan kerja Perangkat Daerah meliputi bidang perindustrian, perdagangan, koperasi, usaha kecil dan menengah, tenaga kerja;'),
	(192, 'KB1011', 'l.          merencanakan dukungan pelaksanaa kegiatan Pusat, Provinsi meliputi bidang perindustrian, perdagangan, koperasi, usaha kecil dan menengah, tenaga kerja;'),
	(193, 'KB1011', 'm.      merencanakan pengendalian/monitoring pelaksanaan perencanaan pembangunan daerah meliputi bidang perindustrian, perdagangan, koperasi, usaha kecil dan menengah, tenaga kerja;'),
	(194, 'KB1011', 'n.        melaksanaan pengelolaan data dan informasi perencanaan pembangunan daerah meliputi bidang perindustrian, perdagangan, koperasi, usaha kecil dan menengah, tenaga kerja;'),
	(195, 'KB1011', 'o.        merencanakan dan menyusun evaluasi dan pelaporan atas pelaksanaan perencanaan pembangunan daerah meliputi bidang perindustrian, perdagangan, koperasi, usaha kecil dan menengah, tenaga kerja;'),
	(196, 'KB1011', 'p.        mengevaluasi pelaksanaan tugas dan menginventarisasi permasalahan dilingkup tugasnya serta mencari alternatif pemecahannya;'),
	(197, 'KB1011', 'q.        mempelajari, memahami dan melaksanakan peraturan perundang-undangan yang berkaitan dengan lingkup tugasnya sebagai pedoman dalam melaksanakan tugas;'),
	(198, 'KB1011', 'r.         memberikan saran dan pertimbangan teknis kepada atasan;'),
	(199, 'KB1011', 's.         membagi tugas, memberi petunjuk, menilai dan mengevaluasi hasil kerja bawahan agar pelaksanaan tugas dapat berjalan lancar sesuai dengan ketentuan yang berlaku;'),
	(200, 'KB1011', 't.         menyampaikan laporan pelaksanaan tugas dan/atau kegiatan kepada atasan;'),
	(201, 'KB1011', 'u.   melaksanakan tugas kedinasan lain yang diberikan oleh atasan.'),
	(202, 'KB1012', 'a.     merencanakan, menyusun dan melaksanakan program dan kegiatan Subbidang Keuangan, Penanaman Modal dan Pariwisata;'),
	(203, 'KB1012', 'b.     menyusun bahan Rencana Kerja dan Anggaran (RKA)/RKPA, Dokumen Pelaksanaan Anggaran (DPA)/DPPA Subbidang Keuangan, Penanaman Modal dan Pariwisata;'),
	(204, 'KB1012', 'c.     melaksanakan Dokumen Pelaksanaan Anggaran (DPA)/DPPA Subbidang Keuangan, Penanaman Modal dan Pariwisata;'),
	(205, 'KB1012', 'd.     merancang penyusun rancangan RPJPD, RPJMD, RKPD meliputi bidang keuangan, pendapatan, penanaman modal dan pariwisata sebagai bahan acuan dalam melaksanakan tugas;'),
	(206, 'KB1012', 'e.     menganalisis rancangan Renstra Satuan Kerja Perangkat Daerah meliputi bidang keuangan, pendapatan, penanaman modal dan pariwisata;'),
	(207, 'KB1012', 'f.      menyiapkan pelaksanaan Musrenbang RPJPD meliputi bidang keuangan, pendapatan, penanaman modal dan pariwisata;'),
	(208, 'KB1012', 'g.     merencanakan pelaksanaan sinergitas dan harmonisasi RTRW Daerah dan RPJMD meliputi bidang keuangan, pendapatan, penanaman modal dan pariwisata;'),
	(209, 'KB1012', 'h.    membuat konsep pembinaan teknis perencanaan kepada Satuan Kerja Perangkat Daerah meliputi bidang keuangan, pendapatan, penanaman modal, pariwisata;'),
	(210, 'KB1012', 'i.      merencanakan pelaksanaan kesepakatan dengan DPRD terkait RPJPD, RPJMD, RKPD meliputi bidang keuangan, pendapatan, penanaman modal dan pariwisata;'),
	(211, 'KB1012', 'j.      merencanakan pelaksanaan kesepakatan dengan DPRD terkait APBD di bidang keuangan, pendapatan, penanaman modal dan pariwisata;'),
	(212, 'KB1012', 'k.     merencanakan sinergitas dan harmonisasi kegiatan Satuan Kerja Perangkat Daerah meliputi bidang keuangan, pendapatan, penanaman modal dan pariwisata;'),
	(213, 'KB1012', 'l.      merencanakan dukungan pelaksanaa kegiatan Pusat, Provinsi meliputi bidang keuangan, pendapatan, penanaman modal dan pariwisata;'),
	(214, 'KB1012', 'm.   merencanakan pengendalian/monitoring pelaksanaan perencanaan pembangunan daerah meliputi bidang keuangan, pendapatan, penanaman modal dan pariwisata;'),
	(215, 'KB1012', 'n.    melaksanaan pengelolaan data dan informasi perencanaan pembangunan Daerah meliputi bidang keuangan, pendapatan, penanaman modal dan pariwisata;'),
	(216, 'KB1012', 'o.     merencanakan dan menyusun evaluasi dan pelaporan atas pelaksanaan perencanaan pembangunan daerah meliputi bidang keuangan, pendapatan, penanaman modal dan pariwisata;'),
	(217, 'KB1012', 'p.     mengevaluasi pelaksanaan tugas dan menginventarisasi permasalahan dilingkup tugasnya serta mencari alternatif pemecahannya;'),
	(218, 'KB1012', 'q.     mempelajari, memahami dan melaksanakan peraturan perundang-undangan yang berkaitan dengan lingkup tugasnya sebagai pedoman dalam melaksanakan tugas;'),
	(219, 'KB1012', 'r.      memberikan saran dan pertimbangan teknis kepada atasan;'),
	(220, 'KB1012', 's.     membagi tugas, memberi petunjuk, menilai dan mengevaluasi hasil kerja bawahan agar pelaksanaan tugas dapat berjalan lancar sesuai dengan ketentuan yang berlaku;'),
	(221, 'KB1012', 't.      menyampaikan laporan pelaksanaan tugas dan/atau kegiatan kepada atasan;'),
	(222, 'KB1012', 'u.  melaksanakan tugas kedinasan lain yang diberikan oleh atasan.'),
	(223, 'KB1013', 'a.     merencanakan, menyusun dan melaksanakan program dan kegiatan Subbidang Pangan, Perikanan dan Pertanian;'),
	(224, 'KB1013', 'b.     menyusun bahan Rencana Kerja dan Anggaran (RKA)/RKPA, Dokumen Pelaksanaan Anggaran (DPA)/DPPA Subbidang Pangan, Perikanan dan Pertanian;'),
	(225, 'KB1013', 'c.     melaksanakan Dokumen Pelaksanaan Anggaran (DPA)/DPPA Subbidang Pangan, Perikanan dan Pertanian;'),
	(226, 'KB1013', 'd.     merancang penyusun rancangan RPJPD, RPJMD, RKPD meliputi bidang pangan, pertanian, peternakan dan perikanan sebagai bahan acuan dalam melaksanakan tugas;'),
	(227, 'KB1013', 'e.     menganalisis rancangan Renstra Satuan Kerja Perangkat Daerah meliputi bidang pangan, pertanian, peternakan dan perikanan;'),
	(228, 'KB1013', 'f.      menyiapkan pelaksanaan Musrenbang RPJPD meliputi bidang pangan, pertanian, peternakan dan perikanan;'),
	(229, 'KB1013', 'g.     merencanakan pelaksanaan sinergitas dan harmonisasi RTRW Daerah dan RPJMD meliputi bidang pangan, pertanian, peternakan dan perikanan;'),
	(230, 'KB1013', 'h.    membuat konsep pembinaan teknis perencanaan kepada Satuan kerja Perangkat Daerah meliputi bidang pangan, pertanian, peternakan dan perikanan;'),
	(231, 'KB1013', 'i.      merencanakan pelaksanaan kesepakatan dengan DPRD terkait RPJPD, RPJMD, RKPD meliputi bidang pangan, pertanian, peternakan dan perikanan;'),
	(232, 'KB1013', 'j.      merencanakan pelaksanaan kesepakatan dengan DPRD terkait APBD meliputi bidang pangan, pertanian, peternakan dan perikanan;'),
	(233, 'KB1013', 'k.     merencanakan sinergitas dan harmonisasi kegiatan Satuan Kerja Perangkat Daerah meliputi bidang pangan, pertanian, peternakan dan perikanan;'),
	(234, 'KB1013', 'l.      merencanakan dukungan pelaksanaa kegiatan Pusat, Provinsi meliputi bidang pangan, pertanian, peternakan dan perikanan;'),
	(235, 'KB1013', 'm.   merencanakan pengendalian/monitoring pelaksanaan perencanaan pembangunan daerah meliputi bidang pangan, pertanian, peternakan dan perikanan;  '),
	(236, 'KB1013', 'n.    melaksanaan pengelolaan data dan informasi perencanaan pembangunan daerah meliputi bidang pangan, pertanian, peternakan dan perikanan;'),
	(237, 'KB1013', 'o.     merencanakan dan menyusun evaluasi dan pelaporan atas pelaksanaan perencanaan pembangunan daerah meliputi bidang pangan, pertanian, peternakan dan perikanan;'),
	(238, 'KB1013', 'p.     mengevaluasi pelaksanaan tugas dan menginventarisasi permasalahan dilingkup tugasnya serta mencari alternatif pemecahannya;'),
	(239, 'KB1013', 'q.     mempelajari, memahami dan melaksanakan peraturan perundang-undangan yang berkaitan dengan lingkup tugasnya sebagai pedoman dalam melaksanakan tugas;'),
	(240, 'KB1013', 'r.      memberikan saran dan pertimbangan teknis kepada atasan;'),
	(241, 'KB1013', 's.     membagi tugas, memberi petunjuk, menilai dan mengevaluasi hasil kerja bawahan agar pelaksanaan tugas dapat berjalan lancar sesuai dengan ketentuan yang berlaku;'),
	(242, 'KB1013', 't.      menyampaikan laporan pelaksanaan tugas dan/atau kegiatan kepada atasan;'),
	(243, 'KB1013', 'u.  melaksanakan tugas kedinasan lain yang diberikan oleh atasan.'),
	(244, 'KB1014', 'a.     merencanakan, menyusun dan melaksanakan program dan kegiatan Bidang Sosial, Budaya dan Pemerintahan Umum;'),
	(245, 'KB1014', 'b.     menghimpun dan menyusun bahan Rencana Kerja dan Anggaran (RKA)/RKPA, Dokumen Pelaksanaan Anggaran (DPA)/DPPA Bidang  Sosial, Budaya dan Pemerintahan Umum;'),
	(246, 'KB1014', 'c.     mengoordinasikan, mengawasi dan mengendalikan pelaksanaan Dokumen Pelaksanaan Anggaran (DPA)/DPPA Bidang Sosial, Budaya dan Pemerintahan Umum;'),
	(247, 'KB1014', 'd.     mengoordinasikan penyusunan rancangan RPJPD, RPJMD dan RKPD di bidang sosial budaya dan pemerintahan umum;'),
	(248, 'KB1014', 'e.     memverifikasi rancangan Renstra Satuan Kerja Perangkat Daerah di bidang sosial budaya dan pemerintahan umum;'),
	(249, 'KB1014', 'f.      mengoordinasikan Pelaksanaan Musrenbang RPJPD, RPJMD, RKPD di bidang sosial budaya dan pemerintahan umum;'),
	(250, 'KB1014', 'g.     mengoordinasikan pelaksanaan sinergitas dan harmonisasi RTRW Daerah dan RPJMD di bidang sosial budaya dan pemerintahan umum;'),
	(251, 'KB1014', 'h.    mengoordinasikan pelaksanaan kesepakatan dengan DPRD terkait RPJPD, RPJMD, RKPD di bidang sosial budaya dan pemerintahan umum;'),
	(252, 'KB1014', 'i.      mengoordinasikan pelaksanaan kesepakatan dengan DPRD terkait APBD di bidang sosial budaya dan pemerintahan umum;'),
	(253, 'KB1014', 'j.      mengoordinasikan sinergitas dan harmonisasi kegiatan Satuan Kerja Perangkat Daerah di bidang sosial budaya dan pemerintahan umum;'),
	(254, 'KB1014', 'k.     mengoordinasikan pelaksanaan sinergitas dan harmonisasi kegiatan Kementrian/Lembaga, Provinsi di bidang sosial budaya dan pemerintahan umum;'),
	(255, 'KB1014', 'l.      mengoordinasikan pembinaan teknis perencanaan kepada Satuan kerja Perangkat Daerah di bidang sosial budaya dan pemerintahan umum;'),
	(256, 'KB1014', 'm.   melaksanakan pengendalian/monitoring pelaksanaan perencanaan pembangunan daerah di bidang sosial budaya dan pemerintahan umum;'),
	(257, 'KB1014', 'n.    melaksanakan pengelolaan data dan informasi perencanaan pembangunan daerah di bidang sosial budaya dan pemerintahan umum;   '),
	(258, 'KB1014', 'o.     melaksanakan evaluasi dan pelaporan atas pelaksanaan perencanaan pembangunan daerah di bidang sosial budaya dan pemerintahan umum;'),
	(259, 'KB1014', 'p.     mengevaluasi pelaksanaan tugas dan menginventarisasi permasalahan di lingkup tugasnya serta mencari alternatif pemecahannya;'),
	(260, 'KB1014', 'q.     mempelajari, memahami dan melaksanakan peraturan perundang-undangan yang berkaitan dengan lingkup tugasnya sebagai pedoman dalam melaksanakan tugas;'),
	(261, 'KB1014', 'r.      memberikan saran dan pertimbangan teknis kepada atasan;'),
	(262, 'KB1014', 's.     membagi tugas, memberi petunjuk, menilai dan mengevaluasi hasil kerja bawahan agar pelaksanaan tugas dapat berjalan lancar sesuai dengan ketentuan yang berlaku;'),
	(263, 'KB1014', 't.      menyampaikan laporan pelaksanaan tugas dan/atau kegiatan kepada atasan;'),
	(264, 'KB1014', 'u.  melaksanakan tugas kedinasan lainnya yang diberikan oleh atasan.'),
	(265, 'KB1015', 'a.     merencanakan, menyusun dan melaksanakan program dan kegiatan Subbidang Kesejahteraan Rakyat;'),
	(266, 'KB1015', 'b.     menyusun bahan Rencana Kerja dan Anggaran (RKA)/RKPA, Dokumen Pelaksanaan Anggaran (DPA)/DPPA Subbidang Kesejahteraan Rakyat;'),
	(267, 'KB1015', 'c.     melaksanakan Dokumen Pelaksanaan Anggaran (DPA)/DPPA Subbidang Kesejahteraan Rakyat;'),
	(268, 'KB1015', 'd.     merancang penyusun rancangan RPJPD, RPJMD, RKPD meliputi bidang pemberdayaan perempuan dan perlindungan anak, pengendalian penduduk dan keluarga berencana, pemberdayaan masyarakat, penanggulangan bencana, sosial, administrasi kependudukan dan pencatatan sipil sebagai bahan acuan dalam melaksanakan tugas;'),
	(269, 'KB1015', 'e.     menganalisis rancangan Renstra Satuan Kerja Perangkat Daerah meliputi bidang pemberdayaan perempuan dan perlindungan anak, pengendalian penduduk dan keluarga berencana, pemberdayaan masyarakat, penanggulangan bencana, sosial, administrasi kependudukan dan pencatatan sipil;'),
	(270, 'KB1015', 'f.      menyiapkan pelaksanaan Musrenbang RPJPD meliputi bidang pemberdayaan perempuan dan perlindungan anak, pengendalian penduduk dan keluarga berencana, pemberdayaan masyarakat, penanggulangan bencana, sosial, administrasi kependudukan dan pencatatan sipil;'),
	(271, 'KB1015', 'g.     merencanakan pelaksanaan sinergitas dan harmonisasi RTRW Daerah dan RPJMD meliputi pemberdayaan perempuan dan perlindungan anak, pengendalian penduduk dan keluarga berencana, pemberdayaan masyarakat, penanggulangan bencana, sosial, administrasi kependudukan dan pencatatan sipil;'),
	(272, 'KB1015', 'h.    membuat konsep pembinaan teknis perencanaan kepada Satuan kerja Perangkat Daerah meliputi bidang pemberdayaan perempuan dan perlindungan anak, pengendalian penduduk dan keluarga berencana, pemberdayaan masyarakat, penanggulangan bencana, sosial, administrasi kependudukan dan pencatatan sipil;'),
	(273, 'KB1015', 'i.      merencanakan pelaksanaan kesepakatan dengan DPRD terkait RPJPD, RPJMD, RKPD meliputi bidang pemberdayaan perempuan dan perlindungan anak, pengendalian penduduk dan keluarga berencana, pemberdayaan masyarakat, penanggulangan bencana, sosial, administrasi kependudukan dan pencatatan sipil;'),
	(274, 'KB1015', 'j.      merencanakan pelaksanaan kesepakatan dengan DPRD terkait APBD meliputi bidang pemberdayaan perempuan dan perlindungan anak, pengendalian penduduk dan keluarga berencana, pemberdayaan masyarakat, penanggulangan bencana, sosial, administrasi kependudukan dan pencatatan sipil;'),
	(275, 'KB1015', 'k.     merencanakan sinergitas dan harmonisasi kegiatan Satuan Kerja Perangkat Daerah meliputi bidang pemberdayaan perempuan dan perlindungan anak, pengendalian penduduk dan keluarga berencana, pemberdayaan masyarakat, penanggulangan bencana, sosial, administrasi kependudukan dan pencatatan sipil;'),
	(276, 'KB1015', 'l.      merencanakan dukungan pelaksanaa kegiatan Pusat, Provinsi meliputi bidang pemberdayaan perempuan dan perlindungan anak, pengendalian penduduk dan keluarga berencana, pemberdayaan masyarakat, penanggulangan bencana, sosial, administrasi kependudukan dan pencatatan sipil;'),
	(277, 'KB1015', 'm.   merencanakan pengendalian/monitoring pelaksanaan perencanaan pembangunan daerah meliputi bidang pemberdayaan perempuan dan perlindungan anak, pengendalian penduduk dan keluarga berencana, pemberdayaan masyarakat, penanggulangan bencana, sosial, administrasi kependudukan dan pencatatan sipil;  '),
	(278, 'KB1015', 'n.    melaksanaan pengelolaan data dan informasi perencanaan pembangunan daerah meliputi bidang pemberdayaan perempuan dan perlindungan anak, pengendalian penduduk dan keluarga berencana, pemberdayaan masyarakat, penanggulangan bencana, sosial, administrasi kependudukan dan pencatatan sipil;'),
	(279, 'KB1015', 'o.     merencanakan dan menyusun evaluasi dan pelaporan atas pelaksanaan perencanaan pembangunan daerah meliputi bidang pemberdayaan perempuan dan perlindungan anak, pengendalian penduduk dan keluarga berencana, pemberdayaan masyarakat, penanggulangan bencana, sosial, administrasi kependudukan dan pencatatan sipil;'),
	(280, 'KB1015', 'p.     mengevaluasi pelaksanaan tugas dan menginventarisasi permasalahan dilingkup tugasnya serta mencari alternatif pemecahannya;'),
	(281, 'KB1015', 'q.     mempelajari, memahami dan melaksanakan peraturan perundang-undangan yang berkaitan dengan lingkup tugasnya sebagai pedoman dalam melaksanakan tugas;'),
	(282, 'KB1015', 'r.      memberikan saran dan pertimbangan teknis kepada atasan;'),
	(283, 'KB1015', 's.     membagi tugas, memberi petunjuk, menilai dan mengevaluasi hasil kerja bawahan agar pelaksanaan tugas dapat berjalan lancar sesuai dengan ketentuan yang berlaku;'),
	(284, 'KB1015', 't.      menyampaikan laporan pelaksanaan tugas dan/atau kegiatan kepada atasan;'),
	(285, 'KB1015', 'u.  melaksanakan tugas kedinasan lain yang diberikan oleh atasan.'),
	(286, 'KB1016', 'a.     merencanakan, menyusun dan melaksanakan program dan kegiatan Subbidang Pemerintahan dan Aparatur;'),
	(287, 'KB1016', 'b.     menyusun bahan Rencana Kerja dan Anggaran (RKA)/RKPA, Dokumen Pelaksanaan Anggaran (DPA)/DPPA Subbidang Pemerintahan dan Aparatur;'),
	(288, 'KB1016', 'c.     melaksanakan Dokumen Pelaksanaan Anggaran (DPA)/DPPA Subbidang Pemerintahan dan Aparatur;'),
	(289, 'KB1016', 'd.     merancang penyusun rancangan RPJPD, RPJMD, RKPD meliputi bidang ketentraman dan ketertiban umum, perlindungan masyarakat, kesatuan bangsa dan politik, sekretariat Dewan Perwakilan Rakyat, sekretariat Daerah, pengawasan, dan kepegawaian sebagai bahan acuan dalam melaksanakan tugas;'),
	(290, 'KB1016', 'e.     menganalisis rancangan Renstra Satuan Kerja Perangkat Daerah meliputi bidang ketentraman dan ketertiban umum, perlindungan masyarakat, kesatuan bangsa dan politik, sekretariat Dewan Perwakilan Rakyat, sekretariat Daerah, pengawasan, dan kepegawaian;'),
	(291, 'KB1016', 'f.      menyiapkan pelaksanaan Musrenbang RPJPD meliputi bidang ketentraman dan ketertiban umum, perlindungan masyarakat, kesatuan bangsa dan politik, sekretariat Dewan Perwakilan Rakyat, sekretariat Daerah, pengawasan, dan kepegawaian;'),
	(292, 'KB1016', 'g.     merencanakan pelaksanaan sinergitas dan harmonisasi RTRW Daerah dan RPJMD meliputi ketentraman dan ketertiban umum, perlindungan masyarakat, kesatuan bangsa dan politik, sekretariat Dewan Perwakilan Rakyat, sekretariat Daerah, pengawasan, dan kepegawaian;'),
	(293, 'KB1016', 'h.    membuat konsep pembinaan teknis perencanaan kepada Satuan kerja Perangkat Daerah meliputi bidang ketentraman dan ketertiban umum, perlindungan masyarakat, kesatuan bangsa dan politik, sekretariat Dewan Perwakilan Rakyat, sekretariat Daerah, pengawasan, dan kepegawaian;'),
	(294, 'KB1016', 'i.      merencanakan pelaksanaan kesepakatan dengan DPRD terkait RPJPD, RPJMD, RKPD meliputi bidang ketentraman dan ketertiban umum, perlindungan masyarakat, kesatuan bangsa dan politik, sekretariat Dewan Perwakilan Rakyat, sekretariat Daerah, pengawasan, dan kepegawaian;'),
	(295, 'KB1016', 'j.      merencanakan pelaksanaan kesepakatan dengan DPRD terkait APBD meliputi bidang ketentraman dan ketertiban umum, perlindungan masyarakat, kesatuan bangsa dan politik, sekretariat Dewan Perwakilan Rakyat, sekretariat Daerah, pengawasan, dan kepegawaian;'),
	(296, 'KB1016', 'k.     merencanakan sinergitas dan harmonisasi kegiatan Satuan Kerja Perangkat Daerah meliputi bidang ketentraman dan ketertiban umum, perlindungan masyarakat, kesatuan bangsa dan politik, sekretariat Dewan Perwakilan Rakyat, sekretariat Daerah, pengawasan, dan kepegawaian;'),
	(297, 'KB1016', 'l.      merencanakan dukungan pelaksanaa kegiatan Pusat, Provinsi meliputi bidang ketentraman dan ketertiban umum, perlindungan masyarakat, kesatuan bangsa dan politik, sekretariat Dewan Perwakilan Rakyat, sekretariat Daerah, pengawasan, dan kepegawaian;'),
	(298, 'KB1016', 'm.   merencanakan pengendalian/monitoring pelaksanaan perencanaan pembangunan daerah meliputi bidang ketentraman dan ketertiban umum, perlindungan masyarakat, kesatuan bangsa dan politik, sekretariat Dewan Perwakilan Rakyat, sekretariat Daerah, pengawasan, dan kepegawaian;  '),
	(299, 'KB1016', 'n.    melaksanaan pengelolaan data dan informasi perencanaan pembangunan daerah meliputi bidang ketentraman dan ketertiban umum, perlindungan masyarakat, kesatuan bangsa dan politik, sekretariat Dewan Perwakilan Rakyat, sekretariat Daerah, pengawasan, dan kepegawaian;'),
	(300, 'KB1016', 'o.     merencanakan dan menyusun evaluasi dan pelaporan atas pelaksanaan perencanaan pembangunan daerah meliputi bidang ketentraman dan ketertiban umum, perlindungan masyarakat, kesatuan bangsa dan politik, sekretariat Dewan Perwakilan Rakyat, sekretariat Daerah, pengawasan, dan kepegawaian;'),
	(301, 'KB1016', 'p.     mengevaluasi pelaksanaan tugas dan menginventarisasi permasalahan dilingkup tugasnya serta mencari alternatif pemecahannya;'),
	(302, 'KB1016', 'q.     mempelajari, memahami dan melaksanakan peraturan perundang-undangan yang berkaitan dengan lingkup tugasnya sebagai pedoman dalam melaksanakan tugas;'),
	(303, 'KB1016', 'r.      memberikan saran dan pertimbangan teknis kepada atasan;'),
	(304, 'KB1016', 's.     membagi tugas, memberi petunjuk, menilai dan mengevaluasi hasil kerja bawahan agar pelaksanaan tugas dapat berjalan lancar sesuai dengan ketentuan yang berlaku;'),
	(305, 'KB1016', 't.      menyampaikan laporan pelaksanaan tugas dan/atau kegiatan kepada atasan;'),
	(306, 'KB1016', 'u.  melaksanakan tugas kedinasan lain yang diberikan oleh atasan.'),
	(307, 'KB1017', 'a.     merencanakan, menyusun dan melaksanakan program dan kegiatan Subbidang Pendidikan, Kesehatan dan Kebudayaan;'),
	(308, 'KB1017', 'b.     menyusun bahan Rencana Kerja dan Anggaran (RKA)/RKPA, Dokumen Pelaksanaan Anggaran (DPA)/DPPA Subbidang Pendidikan, Kesehatan dan Kebudayaan;'),
	(309, 'KB1017', 'c.     melaksanakan Dokumen Pelaksanaan Anggaran (DPA)/DPPA Subbidang Pendidikan, Kesehatan dan Kebudayaan;'),
	(310, 'KB1017', 'd.     merancang penyusun rancangan RPJPD, RPJMD, RKPD meliputi bidang kebudayaan, kesehatan, pendidikan, pemuda dan olahraga, perpustakaan, dan kearsipan sebagai bahan acuan dalam melaksanakan tugas;'),
	(311, 'KB1017', 'e.     menganalisis rancangan Renstra Satuan Kerja Perangkat Daerah meliputi bidang kebudayaan, kesehatan, pendidikan, pemuda dan olahraga, perpustakaan, dan kearsipan;'),
	(312, 'KB1017', 'f.      menyiapkan pelaksanaan Musrenbang RPJPD meliputi bidang kebudayaan, kesehatan, pendidikan, pemuda dan olahraga, perpustakaan, dan kearsipan;'),
	(313, 'KB1017', 'g.     merencanakan pelaksanaan sinergitas dan harmonisasi RTRW Daerah dan RPJMD meliputi kebudayaan, kesehatan, pendidikan, pemuda dan olahraga, perpustakaan, dan kearsipan;'),
	(314, 'KB1017', 'h.    membuat konsep pembinaan teknis perencanaan kepada Satuan kerja Perangkat Daerah meliputi bidang kebudayaan, kesehatan, pendidikan, pemuda dan olahraga, perpustakaan, dan kearsipan;'),
	(315, 'KB1017', 'i.      merencanakan pelaksanaan kesepakatan dengan DPRD terkait RPJPD, RPJMD, RKPD meliputi bidang kebudayaan, kesehatan, pendidikan, pemuda dan olahraga, perpustakaan, dan kearsipan;'),
	(316, 'KB1017', 'j.      merencanakan pelaksanaan kesepakatan dengan DPRD terkait APBD meliputi bidang kebudayaan, kesehatan, pendidikan, pemuda dan olahraga, perpustakaan, dan kearsipan;'),
	(317, 'KB1017', 'k.     merencanakan sinergitas dan harmonisasi kegiatan Satuan Kerja Perangkat Daerah meliputi bidang kebudayaan, kesehatan, pendidikan, pemuda dan olahraga, perpustakaan, dan kearsipan;'),
	(318, 'KB1017', 'l.      merencanakan dukungan pelaksanaa kegiatan Pusat, Provinsi meliputi bidang kebudayaan, kesehatan, pendidikan, pemuda dan olahraga, perpustakaan, dan kearsipan;'),
	(319, 'KB1017', 'm.   merencanakan pengendalian/monitoring pelaksanaan perencanaan pembangunan daerah meliputi bidang kebudayaan, kesehatan, pendidikan, pemuda dan olahraga, perpustakaan, dan kearsipan;  '),
	(320, 'KB1017', 'n.    melaksanaan pengelolaan data dan informasi perencanaan pembangunan daerah meliputi bidang kebudayaan, kesehatan, pendidikan, pemuda dan olahraga, perpustakaan, dan kearsipan;'),
	(321, 'KB1017', 'o.     merencanakan dan menyusun evaluasi dan pelaporan atas pelaksanaan perencanaan pembangunan daerah meliputi bidang kebudayaan, kesehatan, pendidikan, pemuda dan olahraga, perpustakaan, dan kearsipan;'),
	(322, 'KB1017', 'p.     mengevaluasi pelaksanaan tugas dan menginventarisasi permasalahan dilingkup tugasnya serta mencari alternatif pemecahannya;'),
	(323, 'KB1017', 'q.     mempelajari, memahami dan melaksanakan peraturan perundang-undangan yang berkaitan dengan lingkup tugasnya sebagai pedoman dalam melaksanakan tugas;'),
	(324, 'KB1017', 'r.      memberikan saran dan pertimbangan teknis kepada atasan;'),
	(325, 'KB1017', 's.     membagi tugas, memberi petunjuk, menilai dan mengevaluasi hasil kerja bawahan agar pelaksanaan tugas dapat berjalan lancar sesuai dengan ketentuan yang berlaku;'),
	(326, 'KB1017', 't.      menyampaikan laporan pelaksanaan tugas dan/atau kegiatan kepada atasan;'),
	(327, 'KB1017', 'u.  melaksanakan tugas kedinasan lain yang diberikan oleh atasan.'),
	(328, 'KB1018', 'a.     merencanakan, menyusun dan melaksanakan program dan kegiatan Bidang Infrastruktur dan Pengembangan Wilayah;'),
	(329, 'KB1018', 'b.     menghimpun dan menyusun bahan Rencana Kerja dan Anggaran (RKA)/RKPA, Dokumen Pelaksanaan Anggaran (DPA)/DPPA Bidang Infrastruktur dan Pengembangan Wilayah;'),
	(330, 'KB1018', 'c.     mengoordinasikan, mengawasi dan mengendalikan pelaksanaan Dokumen Pelaksanaan Anggaran (DPA)/DPPA Bidang Infrastruktur dan Pengembangan Wilayah;'),
	(331, 'KB1018', 'd.     mengoordinasikan penyusunan rancangan RPJPD, RPJMD dan RKPD di bidang infrastruktur dan pengembangan wilayah;'),
	(332, 'KB1018', 'e.     memverifikasi rancangan Renstra Satuan Kerja Perangkat Daerah di bidang infrastruktur dan pengembangan wilayah;'),
	(333, 'KB1018', 'f.      mengoordinasikan pelaksanaan Musrenbang RPJPD, RPJMD, RKPD di bidang infrastruktur dan pengembangan wilayah;'),
	(334, 'KB1018', 'g.     mengoordinasikan pelaksanaan sinergitas dan harmonisasi RTRW Daerah dan RPJMD di bidang infrastruktur dan pengembangan wilayah;'),
	(335, 'KB1018', 'h.    mengoordinasikan pelaksanaan kesepakatan dengan DPRD terkait RPJPD, RPJMD, RKPD di bidang infrastruktur dan pengembangan wilayah;'),
	(336, 'KB1018', 'i.      mengoordinasikan pelaksanaan kesepakatan dengan DPRD terkait APBD di bidang infrastruktur dan pengembangan wilayah;'),
	(337, 'KB1018', 'j.      mengoordinasikan sinergitas dan harmonisasi kegiatan Satuan Kerja Perangkat Daerah di bidang infrastruktur dan pengembangan wilayah;'),
	(338, 'KB1018', 'k.     mengoordinasikan pelaksanaan sinergitas dan harmonisasi kegiatan Kementrian/Lembaga, Provinsi di bidang infrastruktur dan pengembangan wilayah;'),
	(339, 'KB1018', 'l.      mengoordinasikan pembinaan teknis perencanaan kepada Satuan kerja Perangkat Daerah di bidang sosial budaya dan pemerintahan umum;'),
	(340, 'KB1018', 'm.   melaksanakan pengendalian/monitoring pelaksanaan perencanaan pembangunan daerah di bidang infrastruktur dan pengembangan wilayah;'),
	(341, 'KB1018', 'n.    melaksanakan pengelolaan data dan informasi perencanaan pembangunan daerah di bidang infrastruktur dan pengembangan wilayah;   '),
	(342, 'KB1018', 'o.     melaksanakan evaluasi dan pelaporan atas pelaksanaan perencanaan pembangunan daerah di bidang infrastruktur dan pengembangan wilayah;'),
	(343, 'KB1018', 'p.     mengevaluasi pelaksanaan tugas dan menginventarisasi permasalahan di lingkup tugasnya serta mencari alternatif pemecahannya;'),
	(344, 'KB1018', 'q.     mempelajari, memahami dan melaksanakan peraturan perundang-undangan yang berkaitan dengan lingkup tugasnya sebagai pedoman dalam melaksanakan tugas;'),
	(345, 'KB1018', 'r.      memberikan saran dan pertimbangan teknis kepada atasan;'),
	(346, 'KB1018', 's.     membagi tugas, memberi petunjuk, menilai dan mengevaluasi hasil kerja bawahan agar pelaksanaan tugas dapat berjalan lancar sesuai dengan ketentuan yang berlaku;'),
	(347, 'KB1018', 't.      menyampaikan laporan pelaksanaan tugas dan/atau kegiatan kepada atasan;'),
	(348, 'KB1018', 'u.  melaksanakan tugas kedinasan lainnya yang diberikan oleh atasan.'),
	(349, 'KB1019', 'a.     merencanakan, menyusun dan melaksanakan program dan kegiatan  Subbidang Infrastruktur;'),
	(350, 'KB1019', 'b.     menyusun bahan Rencana Kerja dan Anggaran (RKA)/RKPA, Dokumen Pelaksanaan Anggaran (DPA)/DPPA Subbidang Infrastruktur;'),
	(351, 'KB1019', 'c.     melaksanakan Dokumen Pelaksanaan Anggaran (DPA)/DPPA Subbidang Infrastruktur;'),
	(352, 'KB1019', 'd.     merancang penyusun rancangan RPJPD, RPJMD, RKPD meliputi bidang pekerjaan umum, pemukiman dan perumahan, dan pemadam kebakaran sebagai bahan acuan dalam melaksanakan tugas;'),
	(353, 'KB1019', 'e.     menganalisis rancangan Renstra Satuan Kerja Perangkat Daerah meliputi bidang pekerjaan umum, pemukiman dan perumahan, dan pemadam kebakaran;'),
	(354, 'KB1019', 'f.      menyiapkan pelaksanaan Musrenbang RPJPD meliputi bidang pekerjaan umum, pemukiman dan perumahan, dan pemadam kebakaran;'),
	(355, 'KB1019', 'g.     merencanakan pelaksanaan sinergitas dan harmonisasi RTRW Daerah dan RPJMD meliputi pekerjaan umum, pemukiman dan perumahan, dan pemadam kebakaran;'),
	(356, 'KB1019', 'h.    membuat konsep pembinaan teknis perencanaan kepada Satuan kerja Perangkat Daerah meliputi bidang pekerjaan umum, pemukiman dan perumahan, dan pemadam kebakaran;'),
	(357, 'KB1019', 'i.      merencanakan pelaksanaan kesepakatan dengan DPRD terkait RPJPD, RPJMD, RKPD meliputi bidang pekerjaan umum, pemukiman dan perumahan, dan pemadam kebakaran;'),
	(358, 'KB1019', 'j.      merencanakan pelaksanaan kesepakatan dengan DPRD terkait APBD meliputi bidang pekerjaan umum, pemukiman dan perumahan, dan pemadam kebakaran;'),
	(359, 'KB1019', 'k.     merencanakan sinergitas dan harmonisasi kegiatan Satuan Kerja Perangkat Daerah meliputi bidang pekerjaan umum, pemukiman dan perumahan, dan pemadam kebakaran;'),
	(360, 'KB1019', 'l.      merencanakan dukungan pelaksanaa kegiatan Pusat, Provinsi meliputi bidang pekerjaan umum, pemukiman dan perumahan, dan pemadam kebakaran;'),
	(361, 'KB1019', 'm.   merencanakan pengendalian/monitoring pelaksanaan perencanaan pembangunan daerah meliputi bidang pekerjaan umum, pemukiman dan perumahan, dan pemadam kebakaran;  '),
	(362, 'KB1019', 'n.    melaksanaan pengelolaan data dan informasi perencanaan pembangunan daerah meliputi bidang pekerjaan umum, pemukiman dan perumahan, dan pemadam kebakaran;'),
	(363, 'KB1019', 'o.     merencanakan dan menyusun evaluasi dan pelaporan atas pelaksanaan perencanaan pembangunan daerah meliputi bidang pekerjaan umum, pemukiman dan perumahan, dan pemadam kebakaran;'),
	(364, 'KB1019', 'p.     mengevaluasi pelaksanaan tugas dan menginventarisasi permasalahan dilingkup tugasnya serta mencari alternatif pemecahannya;'),
	(365, 'KB1019', 'q.     mempelajari, memahami dan melaksanakan peraturan perundang-undangan yang berkaitan dengan lingkup tugasnya sebagai pedoman dalam melaksanakan tugas;'),
	(366, 'KB1019', 'r.      memberikan saran dan pertimbangan teknis kepada atasan;'),
	(367, 'KB1019', 's.     membagi tugas, memberi petunjuk, menilai dan mengevaluasi hasil kerja bawahan agar pelaksanaan tugas dapat berjalan lancar sesuai dengan ketentuan yang berlaku;'),
	(368, 'KB1019', 't.      menyampaikan laporan pelaksanaan tugas dan/atau kegiatan kepada atasan;'),
	(369, 'KB1019', 'u.  melaksanakan tugas kedinasan lain yang diberikan oleh atasan.'),
	(370, 'KB1020', 'a.     merencanakan, menyusun dan melaksanakan program dan kegiatan Subbidang Perhubungan dan Komunikasi;'),
	(371, 'KB1020', 'b.     menyusun bahan Rencana Kerja dan Anggaran (RKA)/RKPA, Dokumen Pelaksanaan Anggaran (DPA)/DPPA Subbidang Perhubungan dan Komunikasi;'),
	(372, 'KB1020', 'c.     melaksanakan Dokumen Pelaksanaan Anggaran (DPA)/DPPA Subbidang Perhubungan dan Komunikasi;'),
	(373, 'KB1020', 'd.     merancang penyusun rancangan RPJPD, RPJMD, RKPD meliputi bidang komunikasi dan informatika, penelitian dan pengembangan, statistik, persandian dan perhubungan sebagai bahan acuan dalam melaksanakan tugas;'),
	(374, 'KB1020', 'e.     menganalisis rancangan Renstra Satuan Kerja Perangkat Daerah meliputi bidang komunikasi dan informatika, penelitian dan pengembangan, statistik, persandian dan perhubungan;'),
	(375, 'KB1020', 'f.      menyiapkan pelaksanaan Musrenbang RPJPD meliputi bidang komunikasi dan informatika, penelitian dan pengembangan, statistik, persandian dan perhubungan;'),
	(376, 'KB1020', 'g.     merencanakan pelaksanaan sinergitas dan harmonisasi RTRW Daerah dan RPJMD meliputi komunikasi dan informatika, penelitian dan pengembangan, statistik, persandian dan perhubungan;'),
	(377, 'KB1020', 'h.    membuat konsep pembinaan teknis perencanaan kepada Satuan kerja Perangkat Daerah meliputi bidang komunikasi dan informatika, penelitian dan pengembangan, statistik, persandian dan perhubungan;'),
	(378, 'KB1020', 'i.      merencanakan pelaksanaan kesepakatan dengan DPRD terkait RPJPD, RPJMD, RKPD meliputi bidang komunikasi dan informatika, penelitian dan pengembangan, statistik, persandian dan perhubungan;'),
	(379, 'KB1020', 'j.      merencanakan pelaksanaan kesepakatan dengan DPRD terkait APBD meliputi bidang komunikasi dan informatika, penelitian dan pengembangan, statistik, persandian dan perhubungan;'),
	(380, 'KB1020', 'k.     merencanakan sinergitas dan harmonisasi kegiatan Satuan Kerja Perangkat Daerah meliputi bidang komunikasi dan informatika, penelitian dan pengembangan, statistik, persandian dan perhubungan;'),
	(381, 'KB1020', 'l.      merencanakan dukungan pelaksanaa kegiatan Pusat, Provinsi meliputi bidang komunikasi dan informatika, penelitian dan pengembangan, statistik, persandian dan perhubungan;'),
	(382, 'KB1020', 'm.   merencanakan pengendalian/monitoring pelaksanaan perencanaan pembangunan daerah meliputi bidang komunikasi dan informatika, penelitian dan pengembangan, statistik, persandian dan perhubungan;  '),
	(383, 'KB1020', 'n.    melaksanaan pengelolaan data dan informasi perencanaan pembangunan daerah meliputi bidang komunikasi dan informatika, penelitian dan pengembangan, statistik, persandian dan perhubungan;'),
	(384, 'KB1020', 'o.     merencanakan dan menyusun evaluasi dan pelaporan atas pelaksanaan perencanaan pembangunan daerah meliputi bidang komunikasi dan informatika, penelitian dan pengembangan, statistik, persandian dan perhubungan;'),
	(385, 'KB1020', 'p.     mengevaluasi pelaksanaan tugas dan menginventarisasi permasalahan dilingkup tugasnya serta mencari alternatif pemecahannya;'),
	(386, 'KB1020', 'q.     mempelajari, memahami dan melaksanakan peraturan perundang-undangan yang berkaitan dengan lingkup tugasnya sebagai pedoman dalam melaksanakan tugas;'),
	(387, 'KB1020', 'r.      memberikan saran dan pertimbangan teknis kepada atasan;'),
	(388, 'KB1020', 's.     membagi tugas, memberi petunjuk, menilai dan mengevaluasi hasil kerja bawahan agar pelaksanaan tugas dapat berjalan lancar sesuai dengan ketentuan yang berlaku;'),
	(389, 'KB1020', 't.      menyampaikan laporan pelaksanaan tugas dan/atau kegiatan kepada atasan;'),
	(390, 'KB1020', 'u.  melaksanakan tugas kedinasan lain yang diberikan oleh atasan.'),
	(391, 'KB1021', 'a.     merencanakan, menyusun dan melaksanakan program dan kegiatan Subbidang Pengembangan Wilayah;'),
	(392, 'KB1021', 'b.     menyusun bahan Rencana Kerja dan Anggaran (RKA)/RKPA, Dokumen Pelaksanaan Anggaran (DPA)/DPPA Subbidang Pengembangan Wilayah;'),
	(393, 'KB1021', 'c.     melaksanakan Dokumen Pelaksanaan Anggaran (DPA)/DPPA Subbidang Pengembangan Wilayah;'),
	(394, 'KB1021', 'd.     merancang penyusun rancangan RPJPD, RPJMD, RKPD meliputi bidang penataan ruang, pertanahan, lingkungan hidup, dan kecamatan sebagai bahan acuan dalam melaksanakan tugas;'),
	(395, 'KB1021', 'e.     menganalisis rancangan Renstra Satuan Kerja Perangkat Daerah meliputi bidang penataan ruang, pertanahan, lingkungan hidup, dan kecamatan;'),
	(396, 'KB1021', 'f.      menyiapkan pelaksanaan Musrenbang RPJPD meliputi bidang penataan ruang, pertanahan, lingkungan hidup, dan kecamatan;'),
	(397, 'KB1021', 'g.     merencanakan pelaksanaan sinergitas dan harmonisasi RTRW Daerah dan RPJMD meliputi penataan ruang, pertanahan, lingkungan hidup, dan kecamatan;'),
	(398, 'KB1021', 'h.    membuat konsep pembinaan teknis perencanaan kepada Satuan kerja Perangkat Daerah meliputi bidang penataan ruang, pertanahan, lingkungan hidup, dan kecamatan;'),
	(399, 'KB1021', 'i.      merencanakan pelaksanaan kesepakatan dengan DPRD terkait RPJPD, RPJMD, RKPD meliputi bidang penataan ruang, pertanahan, lingkungan hidup, dan kecamatan;'),
	(400, 'KB1021', 'j.      merencanakan pelaksanaan kesepakatan dengan DPRD terkait APBD meliputi bidang penataan ruang, pertanahan, lingkungan hidup, dan kecamatan;'),
	(401, 'KB1021', 'k.     merencanakan sinergitas dan harmonisasi kegiatan Satuan Kerja Perangkat Daerah meliputi bidang penataan ruang, pertanahan, lingkungan hidup, dan kecamatan;'),
	(402, 'KB1021', 'l.      merencanakan dukungan pelaksanaa kegiatan Pusat, Provinsi meliputi bidang penataan ruang, pertanahan, lingkungan hidup, dan kecamatan;'),
	(403, 'KB1021', 'm.   merencanakan pengendalian/monitoring pelaksanaan perencanaan pembangunan daerah meliputi bidang penataan ruang, pertanahan, lingkungan hidup, dan kecamatan;  '),
	(404, 'KB1021', 'n.    melaksanaan pengelolaan data dan informasi perencanaan pembangunan daerah meliputi bidang penataan ruang, pertanahan, lingkungan hidup, dan kecamatan;'),
	(405, 'KB1021', 'o.     merencanakan dan menyusun evaluasi dan pelaporan atas pelaksanaan perencanaan pembangunan daerah meliputi bidang penataan ruang, pertanahan, lingkungan hidup, dan kecamatan;'),
	(406, 'KB1021', 'p.     mengevaluasi pelaksanaan tugas dan menginventarisasi permasalahan dilingkup tugasnya serta mencari alternatif pemecahannya;'),
	(407, 'KB1021', 'q.     mempelajari, memahami dan melaksanakan peraturan perundang-undangan yang berkaitan dengan lingkup tugasnya sebagai pedoman dalam melaksanakan tugas;'),
	(408, 'KB1021', 'r.      memberikan saran dan pertimbangan teknis kepada atasan;'),
	(409, 'KB1021', 's.     membagi tugas, memberi petunjuk, menilai dan mengevaluasi hasil kerja bawahan agar pelaksanaan tugas dapat berjalan lancar sesuai dengan ketentuan yang berlaku;'),
	(410, 'KB1021', 't.      menyampaikan laporan pelaksanaan tugas dan/atau kegiatan kepada atasan;'),
	(411, 'KB1021', 'u.  melaksanakan tugas kedinasan lain yang diberikan oleh atasan.'),
	(412, 'KB1022', ''),
	(413, 'KB1023', '');
/*!40000 ALTER TABLE `master_jabatan_uraian` ENABLE KEYS */;

-- Dumping structure for table ortala.master_kompetensi
CREATE TABLE IF NOT EXISTS `master_kompetensi` (
  `id` bigint(255) NOT NULL AUTO_INCREMENT,
  `kode_kompetensi` varchar(255) NOT NULL,
  `nama_kompetensi` varchar(255) NOT NULL,
  UNIQUE KEY `unique_id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;

-- Dumping data for table ortala.master_kompetensi: ~3 rows (approximately)
/*!40000 ALTER TABLE `master_kompetensi` DISABLE KEYS */;
INSERT INTO `master_kompetensi` (`id`, `kode_kompetensi`, `nama_kompetensi`) VALUES
	(17, 'KO-M7X', 'Kemampuan Berpikir'),
	(18, 'KO-59S', 'Mengelola Diri'),
	(19, 'KO-YDB', 'Mengelola Orang lain');
/*!40000 ALTER TABLE `master_kompetensi` ENABLE KEYS */;

-- Dumping structure for table ortala.master_kompetensi_def
CREATE TABLE IF NOT EXISTS `master_kompetensi_def` (
  `id` bigint(255) NOT NULL AUTO_INCREMENT,
  `kode_kompetensi` varchar(255) NOT NULL,
  `kode_def` varchar(255) NOT NULL,
  `definisi_kompetensi` varchar(255) NOT NULL,
  UNIQUE KEY `unique_id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=utf8;

-- Dumping data for table ortala.master_kompetensi_def: ~7 rows (approximately)
/*!40000 ALTER TABLE `master_kompetensi_def` DISABLE KEYS */;
INSERT INTO `master_kompetensi_def` (`id`, `kode_kompetensi`, `kode_def`, `definisi_kompetensi`) VALUES
	(34, 'KO-M7X', 'DEF-0P8PJ', 'fleksibilitas Berpikir'),
	(35, 'KO-M7X', 'DEF-UUCR0', 'Kemampuan memunculkan ide / gagasan dan pemikiran baru dalam rangka meningkatkan efektivitas kerja'),
	(36, 'KO-M7X', 'DEF-A7B2H', 'Kemampuan menguraikan permasalahan berdasarkan informasi yang relevan dari berbagai sumber secara komprehensif untuk mengidentifikasi penyebab dan dampak terhadap organisasi'),
	(37, 'KO-59S', 'DEF-7LQP4', 'Kemampuan menyesuaikan diri terhadap perubahan sehingga tetap dapat mempertahankan efektivitas kerja'),
	(38, 'KO-59S', 'DEF-SM189', 'kemampuan bertindak secara komprehensif dan transparan dalam segala situasi dan kondisi sesuai dengan nilai-nilai, norma atau etika yang berlaku'),
	(39, 'KO-YDB', 'DEF-6204R', 'Kemampuan menyelesaikan pekerjaan secara bersama-sama dengan menjadi bagian dari suatu kelompok untuk mencapai tujuan organisasi'),
	(40, 'KO-YDB', 'DEF-NKBWF', 'Kemampuan melakukan upaya untuk  mendorong pengembangan potensi orang lain agar dapat bekerja lebih baik');
/*!40000 ALTER TABLE `master_kompetensi_def` ENABLE KEYS */;

-- Dumping structure for table ortala.master_kompetensi_level
CREATE TABLE IF NOT EXISTS `master_kompetensi_level` (
  `id` bigint(255) NOT NULL AUTO_INCREMENT,
  `kode_kompetensi` varchar(255) NOT NULL,
  `kode_def` varchar(255) NOT NULL,
  `level` tinyint(1) NOT NULL,
  `deskripsi` varchar(255) NOT NULL,
  UNIQUE KEY `unique_id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8;

-- Dumping data for table ortala.master_kompetensi_level: ~17 rows (approximately)
/*!40000 ALTER TABLE `master_kompetensi_level` DISABLE KEYS */;
INSERT INTO `master_kompetensi_level` (`id`, `kode_kompetensi`, `kode_def`, `level`, `deskripsi`) VALUES
	(16, 'KO-M7X', 'DEF-0P8PJ', 1, 'Mengetahui adanya perbedaan sudut pandang'),
	(17, 'KO-M7X', 'DEF-0P8PJ', 2, 'Mengenali sudut pandang orang lain yang berbeda'),
	(18, 'KO-M7X', 'DEF-0P8PJ', 3, 'Menyelaraskan sudut pandang pribadinya dengan sudut pandang orang lain'),
	(19, 'KO-M7X', 'DEF-0P8PJ', 4, 'mengakui kebenaran sudut pandang orang lain'),
	(20, 'KO-M7X', 'DEF-UUCR0', 1, 'menggunakan gagasan/pemikiran yang sudah ada'),
	(21, 'KO-M7X', 'DEF-UUCR0', 2, 'mengenali adanya gagasan baru'),
	(22, 'KO-M7X', 'DEF-UUCR0', 3, 'Mengidentifikasi alternatif ide/gagasan baru yang dapat diterapkan'),
	(23, 'KO-M7X', 'DEF-UUCR0', 4, 'menentukan alternatif yang mungkin dapat dikendalikan'),
	(24, 'KO-M7X', 'DEF-UUCR0', 5, 'mengadopsi ide/pemikiran yang cocok diterapkan dalam lingkungan kerja'),
	(25, 'KO-M7X', 'DEF-A7B2H', 1, 'mengetahui permasalahan dalam pekerjaan'),
	(26, 'KO-M7X', 'DEF-A7B2H', 2, 'menguraikan faktor-faktor penyebab dan dampak dari permasalahan terkait'),
	(27, 'KO-59S', 'DEF-7LQP4', 1, 'menganggap perubahan bukan suatu hal yang penting'),
	(28, 'KO-59S', 'DEF-7LQP4', 2, 'mengikuti perubahan sesuai dengan tuntutan kebijakan organisasi'),
	(29, 'KO-59S', 'DEF-7LQP4', 3, 'Menyesuaikan diri terhadap perubahan yang  terjadi atas kesadaran dan inisiatif sendiri'),
	(30, 'KO-59S', 'DEF-SM189', 1, 'Menyadari tentang pentingnya norma dan etika bagi organisasi'),
	(31, 'KO-59S', 'DEF-SM189', 2, 'Menerapkan norma dan etika organisasi sebatas memenuhi kewajiban'),
	(32, 'KO-59S', 'DEF-SM189', 3, 'mengingatkan orang lain untuk bertindak sesuai dengan nilai, norma dan etika organisasi dalam segala situasi dan kondisi');
/*!40000 ALTER TABLE `master_kompetensi_level` ENABLE KEYS */;

-- Dumping structure for table ortala.tb_evajab
CREATE TABLE IF NOT EXISTS `tb_evajab` (
  `id` bigint(255) NOT NULL AUTO_INCREMENT,
  `kode_evajab` varchar(50) DEFAULT NULL,
  `kode_instansi` varchar(50) NOT NULL DEFAULT '0',
  `tanggal` date NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `kode_evajab` (`kode_evajab`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table ortala.tb_evajab: ~0 rows (approximately)
/*!40000 ALTER TABLE `tb_evajab` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_evajab` ENABLE KEYS */;

-- Dumping structure for table ortala.tb_evajab_detail
CREATE TABLE IF NOT EXISTS `tb_evajab_detail` (
  `id` bigint(255) NOT NULL AUTO_INCREMENT,
  `kode_evajab` varchar(50) NOT NULL,
  `kode_faktor` varchar(50) NOT NULL,
  `level_faktor` int(11) NOT NULL,
  `nilai_faktor` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_tb_evajab_detail_tb_evajab` (`kode_evajab`),
  CONSTRAINT `FK_tb_evajab_detail_tb_evajab` FOREIGN KEY (`kode_evajab`) REFERENCES `tb_evajab` (`kode_evajab`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table ortala.tb_evajab_detail: ~0 rows (approximately)
/*!40000 ALTER TABLE `tb_evajab_detail` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_evajab_detail` ENABLE KEYS */;

-- Dumping structure for table ortala.tb_evajab_hasil_kerja
CREATE TABLE IF NOT EXISTS `tb_evajab_hasil_kerja` (
  `id` bigint(255) NOT NULL AUTO_INCREMENT,
  `kode_evajab` varchar(50) DEFAULT NULL,
  `kode_jabatan` varchar(50) DEFAULT NULL,
  `deskripsi` text,
  PRIMARY KEY (`id`),
  KEY `FK_tb_evajab_hasil_kerja_tb_evajab` (`kode_evajab`),
  CONSTRAINT `FK_tb_evajab_hasil_kerja_tb_evajab` FOREIGN KEY (`kode_evajab`) REFERENCES `tb_evajab` (`kode_evajab`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table ortala.tb_evajab_hasil_kerja: ~0 rows (approximately)
/*!40000 ALTER TABLE `tb_evajab_hasil_kerja` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_evajab_hasil_kerja` ENABLE KEYS */;

-- Dumping structure for table ortala.tb_evajab_tanggung_jawab
CREATE TABLE IF NOT EXISTS `tb_evajab_tanggung_jawab` (
  `id` bigint(255) NOT NULL AUTO_INCREMENT,
  `kode_evajab` varchar(50) DEFAULT NULL,
  `kode_jabatan` varchar(50) DEFAULT NULL,
  `deskripsi` text,
  PRIMARY KEY (`id`),
  KEY `FK_tb_evajab_tanggung_jawab_tb_evajab` (`kode_evajab`),
  CONSTRAINT `FK_tb_evajab_tanggung_jawab_tb_evajab` FOREIGN KEY (`kode_evajab`) REFERENCES `tb_evajab` (`kode_evajab`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Dumping data for table ortala.tb_evajab_tanggung_jawab: ~0 rows (approximately)
/*!40000 ALTER TABLE `tb_evajab_tanggung_jawab` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_evajab_tanggung_jawab` ENABLE KEYS */;

-- Dumping structure for table ortala.tb_jabatan
CREATE TABLE IF NOT EXISTS `tb_jabatan` (
  `id` bigint(255) NOT NULL AUTO_INCREMENT,
  `kode_jabatan` varchar(255) NOT NULL,
  `nama_jabatan` varchar(255) NOT NULL,
  `ikhtisar` varchar(255) NOT NULL,
  UNIQUE KEY `unique_id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- Dumping data for table ortala.tb_jabatan: ~2 rows (approximately)
/*!40000 ALTER TABLE `tb_jabatan` DISABLE KEYS */;
INSERT INTO `tb_jabatan` (`id`, `kode_jabatan`, `nama_jabatan`, `ikhtisar`) VALUES
	(2, 'JB-UD6LN', 'Kepala Subbagian Tata Usaha', 'Memimpin, melaksakan pembuatan rancangan penelitian, rancangan petunjuk teknis dan rancangan pedoman struktur kerja SKPD sesuai dengan peraturan dan ketentuan yang berlaku agar kegiatan di Sub Bagian Tata Usaha berjalan dengan lancar'),
	(3, 'JB-BMIZV', 'contoh', 'asdkjhwkcont');
/*!40000 ALTER TABLE `tb_jabatan` ENABLE KEYS */;

-- Dumping structure for table ortala.tb_unit_kerja
CREATE TABLE IF NOT EXISTS `tb_unit_kerja` (
  `id` bigint(255) NOT NULL AUTO_INCREMENT,
  `kode_jabatan` varchar(255) NOT NULL,
  `eselon_1` varchar(255) NOT NULL,
  `eselon_2` varchar(255) NOT NULL,
  `eselon_3` varchar(255) NOT NULL,
  `eselon_4` varchar(255) NOT NULL,
  UNIQUE KEY `unique_id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- Dumping data for table ortala.tb_unit_kerja: ~2 rows (approximately)
/*!40000 ALTER TABLE `tb_unit_kerja` DISABLE KEYS */;
INSERT INTO `tb_unit_kerja` (`id`, `kode_jabatan`, `eselon_1`, `eselon_2`, `eselon_3`, `eselon_4`) VALUES
	(2, 'JB-UD6LN', 'X', 'X', 'X', 'X'),
	(3, 'JB-BMIZV', 'contoh', 'contoh', 'contoh', 'contoh');
/*!40000 ALTER TABLE `tb_unit_kerja` ENABLE KEYS */;

-- Dumping structure for table ortala.tb_uraian_tugas
CREATE TABLE IF NOT EXISTS `tb_uraian_tugas` (
  `id` bigint(255) NOT NULL AUTO_INCREMENT,
  `kode_jabatan` varchar(255) NOT NULL,
  `kode_tugas` varchar(255) NOT NULL,
  `uraian_tugas` varchar(255) NOT NULL,
  `kata_kunci` varchar(255) NOT NULL,
  `kode_kompetensi` varchar(255) NOT NULL,
  `kode_def` varchar(255) NOT NULL,
  `level` tinyint(1) NOT NULL,
  UNIQUE KEY `unique_id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- Dumping data for table ortala.tb_uraian_tugas: ~3 rows (approximately)
/*!40000 ALTER TABLE `tb_uraian_tugas` DISABLE KEYS */;
INSERT INTO `tb_uraian_tugas` (`id`, `kode_jabatan`, `kode_tugas`, `uraian_tugas`, `kata_kunci`, `kode_kompetensi`, `kode_def`, `level`) VALUES
	(1, 'JB-UD6LN', 'TGS-LU8P9', 'Merencanakan kegiatan subbagian Tata Usaha sesuai dengan program kerja direktorat pengendalian Kebakaran Hutan dan ketentuan yang berlaku untuk pedoman pelaksanaan tugas', 'Kata Kunci--untuk nanti', 'KO-M7X', 'DEF-UUCR0', 3),
	(2, 'JB-UD6LN', 'TGS-4IO6K', 'Membagi tugas kepada bawahan di lingkungan subbagian tata usaha sesuai dengan beban kerja dan tanggung jawabnya masing-masing untuk kelancaran pelaksanaan tugas', 'Kata Kunci--untuk nanti', 'KO-59S', 'DEF-7LQP4', 2),
	(3, 'JB-BMIZV', 'TGS-0JPUN', '', 'Kata Kunci--untuk nanti', 'KO-M7X', 'DEF-0P8PJ', 2);
/*!40000 ALTER TABLE `tb_uraian_tugas` ENABLE KEYS */;

-- Dumping structure for table ortala.tb_uraian_tugas_detail
CREATE TABLE IF NOT EXISTS `tb_uraian_tugas_detail` (
  `id` bigint(255) NOT NULL AUTO_INCREMENT,
  `kode_tugas` varchar(255) NOT NULL,
  `point` tinyint(255) NOT NULL,
  `detail` varchar(255) NOT NULL,
  `kata_kunci` varchar(255) NOT NULL,
  UNIQUE KEY `unique_id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- Dumping data for table ortala.tb_uraian_tugas_detail: ~7 rows (approximately)
/*!40000 ALTER TABLE `tb_uraian_tugas_detail` DISABLE KEYS */;
INSERT INTO `tb_uraian_tugas_detail` (`id`, `kode_tugas`, `point`, `detail`, `kata_kunci`) VALUES
	(1, 'TGS-LU8P9', 1, 'Menelaah Program direktorat pengendalian Kebakaran Hutan', 'Kata Kunci--untuk nanti'),
	(2, 'TGS-LU8P9', 2, 'Menyusun konsep rencana kegiatan dengan pimpinan untuk mendapatkan pengarahan', 'Kata Kunci--untuk nanti'),
	(3, 'TGS-LU8P9', 3, 'menetapkan rencana kegiatan subbagian tata usaha', 'Kata Kunci--untuk nanti'),
	(4, 'TGS-4IO6K', 1, 'menjabarkan rencana tugas menjadi tugas-tugas kecil yang harus dilaksakan bawahan', 'Kata Kunci--untuk nanti'),
	(5, 'TGS-4IO6K', 2, 'memaparkan rencana kegiatan subbagian tata usaha', 'Kata Kunci--untuk nanti'),
	(6, 'TGS-4IO6K', 3, 'memberikan petunjuk pelaksaan', 'Kata Kunci--untuk nanti'),
	(7, 'TGS-0JPUN', 1, '', 'Kata Kunci--untuk nanti');
/*!40000 ALTER TABLE `tb_uraian_tugas_detail` ENABLE KEYS */;

-- Dumping structure for table ortala.tb_uraian_tugas_kategori
CREATE TABLE IF NOT EXISTS `tb_uraian_tugas_kategori` (
  `id` bigint(255) NOT NULL AUTO_INCREMENT,
  `kode_tugas` varchar(255) NOT NULL,
  `mutlak` tinyint(255) NOT NULL,
  `penting` tinyint(255) NOT NULL,
  `perlu` tinyint(255) NOT NULL,
  UNIQUE KEY `unique_id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table ortala.tb_uraian_tugas_kategori: ~0 rows (approximately)
/*!40000 ALTER TABLE `tb_uraian_tugas_kategori` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_uraian_tugas_kategori` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
